package nl.saxion.itech.protocoltests;

import java.io.*;
import java.net.Socket;
import java.util.Properties;

import org.junit.jupiter.api.*;

import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

public class PollTest {
    private static Properties props = new Properties();

    private Socket socketUser1, socketUser2, socketUser3;
    private BufferedReader inUser1, inUser2, inUser3;
    private PrintWriter outUser1, outUser2, outUser3;

    private final static int max_delta_allowed_ms = 100;

    @BeforeAll
    static void setupAll() throws IOException {
        InputStream in = MultipleUserTests.class.getResourceAsStream("testconfig.properties");
        props.load(in);
        in.close();
    }

    @BeforeEach
    void setup() throws IOException {
        socketUser1 = new Socket(props.getProperty("host"), Integer.parseInt(props.getProperty("port")));
        inUser1 = new BufferedReader(new InputStreamReader(socketUser1.getInputStream()));
        outUser1 = new PrintWriter(socketUser1.getOutputStream(), true);

        socketUser2 = new Socket(props.getProperty("host"), Integer.parseInt(props.getProperty("port")));
        inUser2 = new BufferedReader(new InputStreamReader(socketUser2.getInputStream()));
        outUser2 = new PrintWriter(socketUser2.getOutputStream(), true);

        socketUser3 = new Socket(props.getProperty("host"), Integer.parseInt(props.getProperty("port")));
        inUser3 = new BufferedReader(new InputStreamReader(socketUser3.getInputStream()));
        outUser3 = new PrintWriter(socketUser3.getOutputStream(), true);
    }

    @AfterEach
    void cleanup() throws IOException {
        socketUser1.close();
        socketUser2.close();
        socketUser3.close();
    }

    @Test
    void voteInPoll() throws IOException{
        receiveLineWithTimeout(inUser1); //INIT
        receiveLineWithTimeout(inUser2); //INIT
        receiveLineWithTimeout(inUser3); //INIT

        // Connect user1
        outUser1.println("IDENT user1");
        outUser1.flush();
        receiveLineWithTimeout(inUser1); //OK

        // Connect user2
        outUser2.println("IDENT user2");
        outUser2.flush();
        receiveLineWithTimeout(inUser2); //OK
        receiveLineWithTimeout(inUser1); //JOINED

        outUser3.println("IDENT user3");
        outUser3.flush();
        receiveLineWithTimeout(inUser3); //OK
        receiveLineWithTimeout(inUser1); //JOINED
        receiveLineWithTimeout(inUser2); //JOINED

        //send BCST from user 1
        outUser1.println("POLL START subject\t\t q1 \t yes \t no\t\t q2 \t yes \t no");
        outUser1.flush();
        receiveLineWithTimeout(inUser1);//ok poll start
        receiveLineWithTimeout(inUser1);//poll started
        receiveLineWithTimeout(inUser2);//poll started
        receiveLineWithTimeout(inUser3);//poll started
        outUser2.println("POLL VOTE subject \t\t0,1");
        outUser2.flush();
        receiveLineWithTimeout(inUser2);//ok poll vote
        outUser3.println("POLL VIEW subject");
        String fromUser3 = receiveLineWithTimeout(inUser3);
        assertEquals("OK POLL VIEW subject:1 \t\t  q1  \t  yes :1 \t  no:0 \t\t  q2  \t  yes :0 \t  no:1 ", fromUser3);
    }

    private String receiveLineWithTimeout(BufferedReader reader) {
        return assertTimeoutPreemptively(ofMillis(max_delta_allowed_ms), () -> reader.readLine());
    }
}
