package com.company;

import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.util.HashMap;
import java.util.Map;

public class Question {
    
    private String question;
    private String[] options;
    private HashMap<String, Integer> votes;

    public Question(String question, String[] options) {
        this.question = question;
        this.options = options;
        this.votes = new HashMap<>();
    }

    public String getQuestion() {
        return question;
    }

    public void vote(String voterName, Integer pollOption) {
        votes.put(voterName, pollOption);
    }

    public String[] optionsString() {
        String[] results = new String[options.length];
        for (int i = 0; i < options.length; i++) {
            results[i] = "[" + i + "] " + options[i] + "\t votes: " + getVotesPerOption(i);
        }
        return results;
    }

    public String toString() {
        String result = question + " ";
        for (int i = 0; i < options.length; i++) {
            result = result + "\t " + options[i] + " ";
        }
        return result;
    }

    public String toStringView() {
        String result = question + " ";
        for (int i = 0; i < options.length; i++) {
            result = result + "\t " + options[i] + ":" + getVotesPerOption(i) + " ";
        }
        return result;
    }

    public Integer getTotalVotes() {
        return votes.size();
    }

    private Integer getVotesPerOption(Integer index) {
        Integer total = 0;
        for (Map.Entry<String, Integer> entry : votes.entrySet()) {
            if (entry.getValue() == index) {
                total++;
            }
        }
        return total;
    }
}
