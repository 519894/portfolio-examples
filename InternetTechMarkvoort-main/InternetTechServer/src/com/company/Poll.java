package com.company;public class Poll {
    private String starterName;
    private Question[] questions;
    private String subject;

    public Poll(String starterName, String subject, Question[] questions){
        this.starterName = starterName;
        this.subject = subject;
        this.questions = questions;
    }

    public String getSubject() {
        return subject;
    }

    public String getStarterName(){return starterName;}

    public Question[] getQuestions() {
        return questions;
    }

    public Question getQuestion(int i) {
        return questions[i];
    }

    public String toString() {
        String result =  subject + " ";
        for (Question q : questions) {
            result = result + "\t\t " + q.toString();
        }
        return result;
    }

    public String toStringView() {
        String result =  subject + ":" + questions[0].getTotalVotes() + " ";
        for (Question q : questions) {
            result = result + "\t\t " + q.toStringView();
        }
        return result;
    }

    public void vote(String userName, String[] answers){
        if (answers.length != questions.length){
            System.out.println("not the right amount of answers.");
        }else{
            for (int i = 0; i < answers.length; i++) {
                questions[i].vote(userName, Integer.parseInt(answers[i]));
            }

        }
    }
}
