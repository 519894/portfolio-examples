package com.company;

import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    private static ServerSocket serverSocket;
    private static ServerSocket fileServerSocket;
    private final static int SERVER_PORT = 1337;
    private final static int SERVER_FILE_PORT = 1338;
    private static ArrayList<ClientConnection> clientConnections;
    private static ArrayList<Poll> polls = new ArrayList<>();
    private static HashMap<String, String> fileRequests = new HashMap<>();

    public static void main(String[] args) {
        clientConnections = new ArrayList<ClientConnection>();
        try {
            serverSocket = new ServerSocket(SERVER_PORT);
            fileServerSocket = new ServerSocket(SERVER_FILE_PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileSocketConnector FSConnector = new FileSocketConnector();
        FSConnector.start();

        while (true) {
            try {
                Socket socket = serverSocket.accept();
                ClientConnection clientConnection = new ClientConnection(socket);
                clientConnection.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static class FileSocketConnector extends Thread {
        public FileSocketConnector(){}

        public void run(){
            while (true) {
                FileTransfer fileTransfer = null;
                while(fileTransfer == null){
                    try {
                        Socket socket = fileServerSocket.accept();
                        fileTransfer = new FileTransfer(socket);
                        fileTransfer.start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private static class ClientConnection extends Thread {

        private Socket socket;
        private BufferedReader reader;
        private BufferedOutputStream outputStream;
        private PrintWriter pw;
        private volatile boolean running = true;
        private String username;
        private HeartBeat heartbeat;


        public ClientConnection(Socket socket) {
            try{
                this.socket = socket;
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                outputStream = new BufferedOutputStream(socket.getOutputStream());
                pw = new PrintWriter(outputStream);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {
            String welcome = "INIT Welcome to Chat Thing!";
            pw.println(welcome);
            pw.flush();
            System.out.println("--> " + welcome);

            // Wait until the user sends in a valid username, then break from the loop and continue
            while (running) {
                String userInput = "";
                try {
                    userInput = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("<-- " + userInput);
                if (userInput.startsWith("IDENT ")) {
                    username = userInput.substring(6);

                    boolean userLoggedIn = false;

                    for (ClientConnection c: clientConnections) {
                        if(c.username != null){
                            if(c.username.equals(username) && !c.equals(this)) {
                                userLoggedIn = true;
                                break;
                            }
                        }
                    }

                    if(userLoggedIn) {
                        pw.println("FAIL01 User already logged in");
                        pw.flush();
                        continue;
                    }

                    Pattern pattern = Pattern.compile("([^A-Za-z0-9\\_]+)");
                    Matcher matcher = pattern.matcher(username);
                    if(username.length() < 3 || username.length() > 14) {
                        String wrongLength = "FAIL02 Please enter a username between 3 and 14 characters.";
                        pw.println(wrongLength);
                        pw.flush();
                    } else {
                        if(matcher.find()){
                            String wrongLength = "FAIL02 Please enter a username using only number, letters and underscores.";
                            pw.println(wrongLength);
                            pw.flush();
                        } else {
                            String acknowledge = "OK IDENT " + username;
                            pw.println(acknowledge);
                            pw.flush();
                            System.out.println("--> [" + username + "] " + acknowledge);
                            break;
                        }
                    }
                } else if(userInput.startsWith("QUIT")){
                    String goodbye = "OK Goodbye";
                    pw.println(goodbye);
                    pw.flush();
                    System.out.println("--> [" + username + "] " + goodbye);
                    running = false;
                    this.interrupt();
                } else {
                    String deny = "FAIL00 Please login using IDENT";
                    pw.println(deny);
                    pw.flush();
                    System.out.println("--> [" + username + "] " + deny);
                }
            }

            clientConnections.add(this);
            System.out.println("Num clients: " + clientConnections.size());

			heartbeat = new HeartBeat(this);
			heartbeat.start();

            for (ClientConnection c: clientConnections) {
                if(c != this) {
                    String outgoing = "JOINED " + username;
                    c.pw.println(outgoing);
                    c.pw.flush();
                }
            }

            while (running) {
                try {
                    String message = reader.readLine();
                    if (message == null) {
                        continue;
                    }
                    System.out.println("<-- [" + username + "] " + message);
                    if (message.startsWith("BCST")) {
                        String acknowledge = "OK " + message;
                        pw.println(acknowledge);
                        pw.flush();
                        System.out.println("--> [" + username + "] " + acknowledge);
                        for (ClientConnection c: clientConnections) {
                            if(c != this) {
                                String outgoing = "BCST " + username + " " + message.substring(5);
                                c.pw.println(outgoing);
                                c.pw.flush();
                                System.out.println("--> [" + c.username + "] " + outgoing);
                            }
                        }
                    } else if(message.startsWith("DM ")){
                        String[] content = message.substring(3).split(" ", 2);
                        String name = content[0];
                        String directMessage = content[1];
                        boolean found = false;
                        for (ClientConnection c: clientConnections) {
                            if(c.username.equals(name)) {
                                String outgoing = "DM " + username + " " + directMessage;
                                c.pw.println(outgoing);
                                c.pw.flush();
                                System.out.println("--> [" + c.username + "] " + outgoing);
                                found = true;
                                break;
                            }
                        }
                        if(!found){
                            String error = "FAIL06 User " + name + " not found!";
                            System.out.println("--> [" + username + "] " +  error);
                            pw.println(error);
                            pw.flush();
                        }
                    } else if(message.startsWith("LIST")) {
                        String listMessage = "OK LIST ";
                        for(ClientConnection connection : clientConnections){
                            listMessage = listMessage + "[" + clientConnections.indexOf(connection) + "] " + connection.username + "\t";
                        }
                        pw.println(listMessage);
                        pw.flush();
                        System.out.println("--> [" + username + "] " +  listMessage);
                    } else if(message.startsWith("PUBKEY GET ")) {
                        String name = message.substring(11);
                        for(ClientConnection connection : clientConnections){
                            if(connection.username.equals(name)){
                                connection.pw.println("PUBKEY GET " + username);
                                connection.pw.flush();
                                System.out.println("--> [" + connection.username + "] " +  "PUBKEY GET " + username);
                            }
                        }
                    } else if(message.startsWith("PUBKEY SEND ")) {
                        String[] content = message.substring(12).split(" ", 2);
                        String name = content[0];
                        String pubKey = content[1];
                        for(ClientConnection connection : clientConnections){
                            if(connection.username.equals(name)){
                                connection.pw.println("PUBKEY SEND " + username + " " + pubKey);
                                connection.pw.flush();
                                System.out.println("--> [" + connection.username + "] " +  "PUBKEY SEND " + username + " " + pubKey);
                            }
                        }
                    } else if(message.startsWith("PASSWORD SEND ")) {
                        String[] content = message.substring(14).split(" ", 2);
                        String name = content[0];
                        String encodedPassword = content[1];
                        for(ClientConnection connection : clientConnections){
                            if(connection.username.equals(name)){
                                connection.pw.println("PASSWORD SEND " + username + " " + encodedPassword);
                                connection.pw.flush();
                                System.out.println("--> [" + connection.username + "] " +  "PASSWORD SEND " + username + " " + encodedPassword);
                            }
                        }
                    } else if(message.startsWith("PASSWORD OK ")) {
                        String name = message.substring(12);

                        for(ClientConnection connection : clientConnections){
                            if(connection.username.equals(name)){
                                connection.pw.println("PASSWORD OK " + username);
                                connection.pw.flush();
                                System.out.println("--> [" + connection.username + "] " +  "PASSWORD OK " + username);
                            }
                        }
                    } else if(message.startsWith("FILEREQUEST REQUEST ")) {
                        String content = "FILEREQUEST REQUEST " + username;
                        String receiverName = message.substring(20);

                        boolean found = false;
                        for (ClientConnection c: clientConnections) {
                            if(c.username.equals(receiverName)) {
                                c.pw.println(content);
                                c.pw.flush();
                                System.out.println("--> [" + c.username + "] " + content);
                                found = true;
                                fileRequests.put(username, receiverName);
                                break;
                            }
                        }
                        if(!found){
                            String error = "FAIL06 User " + receiverName + " not found!";
                            System.out.println("--> [" + username + "] " +  error);
                            pw.println(error);
                            pw.flush();
                        }
                    } else if(message.startsWith("FILEREQUEST ACCEPT ")) {
                        String senderName = message.substring(19);
                        String content = "FILEREQUEST ACCEPT " + username;

                        String receiverName = fileRequests.get(senderName);
                        if (receiverName == null) {
                            String error = "FAIL13 User " + senderName + " has not requested a filetransfer to you.";
                            System.out.println("--> [" + username + "] " + error);
                            pw.println(error);
                            pw.flush();
                        } else {
                            if (receiverName.equals(username)) {
                                boolean found = false;
                                for (ClientConnection c : clientConnections) {
                                    if (c.username.equals(senderName)) {
                                        c.pw.println(content);
                                        c.pw.flush();
                                        System.out.println("--> [" + c.username + "] " + content);
                                        found = true;
                                        fileRequests.remove(senderName);
                                        break;
                                    }
                                }
                                if (!found) {
                                    String error = "FAIL06 User " + senderName + " not found!";
                                    System.out.println("--> [" + username + "] " + error);
                                    pw.println(error);
                                    pw.flush();
                                    fileRequests.remove(senderName);
                                }
                            } else {
                                String error = "FAIL13 User " + senderName + " has not requested a filetransfer to you.";
                                System.out.println("--> [" + username + "] " + error);
                                pw.println(error);
                                pw.flush();
                            }
                        }
                    } else if(message.startsWith("FILEREQUEST DENY ")) {
                        String senderName = message.substring(17);
                        String content = "FILEREQUEST DENY " + username;

                        if (fileRequests.get(senderName).equals(username)){
                            boolean found = false;
                            for (ClientConnection c: clientConnections) {
                                if(c.username.equals(senderName)) {
                                    c.pw.println(content);
                                    c.pw.flush();
                                    System.out.println("--> [" + c.username + "] " + content);
                                    found = true;
                                    fileRequests.remove(senderName);
                                    break;
                                }
                            }
                            if(!found){
                                String error = "FAIL06 User " + senderName + " not found!";
                                System.out.println("--> [" + username + "] " +  error);
                                pw.println(error);
                                pw.flush();
                                fileRequests.remove(senderName);
                            }
                        } else {
                            String error = "FAIL13 User " + senderName + " has not requested a filetransfer to you.";
                            System.out.println("--> [" + username + "] " +  error);
                            pw.println(error);
                            pw.flush();
                        }
                    } else if(message.startsWith("POLL START ")) {
                        if (clientConnections.size() >= 3) {
                        
                            String[] content = message.substring(11).split("\t\t");
                            String subject = content[0];
                            Question[] questions = new Question[content.length-1];

                            for (int i = 1; i < content.length; i++) {
                                String[] questionContent = content[i].split("\t");
                                Question q = new Question(questionContent[0], Arrays.copyOfRange(questionContent, 1, questionContent.length));
                                questions[i-1] = q;
                            }

                            Poll poll = new Poll(username, subject, questions);
                            polls.add(poll);

                            for(ClientConnection connection : clientConnections){
                                connection.pw.println("BCST " + username + " has started a new poll called " + subject);
                                connection.pw.flush();
                            }

                            String okayMessage = "OK POLL START " + subject;
                            pw.println(okayMessage);
                            System.out.println("--> [" + username + "] " +  okayMessage);
                        } else {
                            String fail08 = "FAIL08 Not enough users online";
                            pw.println(fail08);
                            System.out.println("--> [" + username + "] " +  fail08);
                        }
                        pw.flush();
                    } else if(message.startsWith("POLL VIEW ")) {
                        String subject = message.substring(10);
                        Poll found = null;
                        for(Poll poll : polls){
                            if(poll.getSubject().equals(subject)){
                                found = poll;
                                break;
                            }
                        }
                        if(found == null){
                            String pollNotFound = "FAIL00";
                            pw.println(pollNotFound);
                            System.out.println("--> [" + username + "] " +  pollNotFound);
                        }
                        else {
                            String pollView = "OK POLL VIEW " + found.toStringView();
                            pw.println(pollView);
                            System.out.println("--> [" + username + "] " +  pollView);
                        }
                        pw.flush();
                    } else if(message.startsWith("POLL VOTE ")) {
                        String okVote = "OK POLL VOTE ";
                        pw.println(okVote);
                        System.out.println("--> [" + username + "] " +  okVote);
                        pw.flush();
                        String votes = message.substring(10);
                        String[] content = votes.split("\t\t");
                        String subject = content[0].trim();
                        String[] awnsers = content[1].split(",");
                        Poll foundpoll = null;
                        for(Poll poll : polls){
                            if (poll.getSubject().equals(subject)){
                                foundpoll = poll;
                                break;
                            }
                        }
                        if(foundpoll == null){
                            System.out.println("poll not found.");
                            break;
                        }
                        foundpoll.vote(username, awnsers);
                    } else if(message.startsWith("POLL VOTEVIEW ")) {
                        String subject = message.substring(14);
                        Poll found = null;
                        for(Poll poll : polls){
                            if(poll.getSubject().equals(subject)){
                                found = poll;
                                break;
                            }
                        }
                        if(found == null){
                            String pollNotFound = "FAIL00";
                            pw.println(pollNotFound);
                            System.out.println("--> [" + username + "] " +  pollNotFound);
                        } else {
                            String pollView = "OK POLL VOTEVIEW " + found.toString();
                            pw.println(pollView);
                            System.out.println("--> [" + username + "] " +  pollView);
                        }
                        pw.flush();
                    } else if(message.startsWith("POLL STOP ")) {
                        String subject = message.substring(10);
                        Poll foundPoll = null;
                        for(Poll poll : polls){
                            if (poll.getSubject().equals(subject)){
                                foundPoll = poll;
                                break;
                            }
                        }
                        if(foundPoll == null){
                            System.out.println("poll not found.");
                            break;
                        }
                        else{
                            if(username == foundPoll.getStarterName()) {
                                for (ClientConnection connection : clientConnections) {
                                    connection.pw.println("BCST " + username + " has ended a poll called " + subject);
                                    connection.pw.flush();
                                }
                                polls.remove(foundPoll);
                            }
                            else{
                                pw.println("FAIL33 can not end poll not started by you.");
                            }
                        }
                    } else if(message.startsWith("IDENT ")) {
                        pw.println("FAIL04 User cannot login twice");
                    } else if(message.startsWith("QUIT")) {
                        String goodbye = "OK Goodbye";
                        pw.println(goodbye);
                        pw.flush();
                        System.out.println("--> [" + username + "] " + goodbye);
                        clientConnections.remove(this);
                        running = false;
                        this.interrupt();
                    } else if(message.startsWith("PONG")) {
                        if(heartbeat.pong == true){
                            pw.println("FAIL05 Pong without ping");
                        }
                        heartbeat.pong = true;
                    } else {
                        System.out.println("ER00 Unkown command");
                    }
                } catch (IOException e) {
                    System.err.println("Something went wrong, disconnecting client " + username);
                    clientConnections.remove(this);
                    System.out.println("Num clients: " + clientConnections.size());
                    running = false;
                    this.interrupt();
                }
            }
            this.interrupt();
        }
    }
	
    private static class HeartBeat extends Thread {
        private boolean pong = false;
        private ClientConnection connection;

        public HeartBeat(ClientConnection connection) {
            this.connection = connection;            
        }
		
		public void run() {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(connection.running) {
                try{
                    String outgoing = "PING";
                    connection.pw.println(outgoing);
                    connection.pw.flush();
                    System.out.println("--> [" + connection.username + "] " + outgoing);
                    Thread.sleep(10000);
                    if (pong == false) {
                        String disconnect = "DSCN Pong timeout";
                        connection.pw.println(disconnect);
                        System.out.println("--> [" + connection.username + "] " + disconnect);
                        connection.running = false;
                    }
                    pong = false;	
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
		}
    }

    private static class FileTransfer extends Thread {
        private Socket fileReceiverSocket;
        private Socket fileSenderSocket;

        private DataOutputStream fileDataOutputStream;
        private DataInputStream dataInputStream;

        public FileTransfer(Socket givenSocket) {
            try {
                this.fileReceiverSocket = givenSocket;
                try {
                    fileSenderSocket = fileServerSocket.accept();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                dataInputStream = new DataInputStream(fileSenderSocket.getInputStream());
                fileDataOutputStream = new DataOutputStream(fileReceiverSocket.getOutputStream());
            }catch (Exception e){}
        }

        public void run() {
            try {
                while (true) {
                    int bytes = 0;

                    // checksum
                    long checksum = dataInputStream.readLong();     // read file size
                    fileDataOutputStream.writeLong(checksum);
                    fileDataOutputStream.flush();

                    // size
                    long size = dataInputStream.readLong();     // read file size
                    fileDataOutputStream.writeLong(size);
                    fileDataOutputStream.flush();

                    byte[] buffer = new byte[4 * 1024];
                    while (size > 0 && (bytes = dataInputStream.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
                        fileDataOutputStream.write(buffer, 0, bytes);
                        fileDataOutputStream.flush();
                        size -= bytes;      // read upto file size
                    }
                }
            }catch(Exception e){
                System.out.println(e);
            }
        }
    }
}
