package com.company;public class Poll {
    private String starterName;
    private Question[] questions;
    private String subject;

    public Poll(String starterName, String subject, Question[] questions){
        this.starterName = starterName;
        this.subject = subject;
        this.questions = questions;
    }

    public String getSubject() {
        return subject;
    }

    public Question[] getQuestions() {
        return questions;
    }

    public Question getQuestion(int i) {
        return questions[i];
    }

    public String toString() {
        String result =  subject + ":" + questions[0].getTotalVotes() + " ";
        for (Question q : questions) {
            result = result + "\t\t " + q.toString();
        }
        return result;
    }
}
