package com.company;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final String STATE_DEFAULT = "default";
    private static final String STATE_POLL_VOTING = "pollvoting";

    private static boolean running;
    private static Socket socket;
    private static Socket fileSocket;
    private static BufferedReader reader;
    private static PrintWriter writer;
    private static Scanner sc;
    private static Poll currentPoll;
    private static String state;
    private final static String SERVER_ADDRESS = "localhost";
    private final static int SERVER_PORT = 1337;
    private final static int SERVER_FILE_PORT = 1338;
    private static FileSender fileSender;
    private static FileReceiver fileReceiver;

    private static KeyPairGenerator generator;
    private static PrivateKey privKey;
    private static PublicKey pubKey;

    private static HashMap<String, String> encryptionPasswords;

    private static String algorithm;
    private static String encoding;
    private static Cipher cipher;

    private static String queuedDm;



    public static void main(String[] args) {
        try {
            generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(2048);
            KeyPair pair = generator.generateKeyPair();

            encryptionPasswords = new HashMap<>();

            privKey = pair.getPrivate();
            pubKey = pair.getPublic();

            algorithm = "AES";
            encoding = "UTF-8";
            cipher = Cipher.getInstance(algorithm);

            state = STATE_DEFAULT;
            running = true;
            socket = new Socket(SERVER_ADDRESS, SERVER_PORT);

            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            reader = new BufferedReader(new InputStreamReader(inputStream));
            writer = new PrintWriter(outputStream);
            sc = new Scanner(System.in);

            String line = reader.readLine();

            boolean loggedIn = false;

            while(!loggedIn){
                System.out.println("Please enter your username");
                String name = sc.nextLine();

                writer.println("IDENT " + name);
                writer.flush();

                line = reader.readLine();
                if(line.startsWith("OK IDENT")){
                    loggedIn = true;
                    System.out.println("Successfully logged in, welcome " + name + ".");
                } else {
                    if(line.startsWith("FAIL00")){
                        System.out.println("Unkown command");
                    }else if(line.startsWith("FAIL02")){
                        System.out.println("Username has an invalid format or length");
                    }else if(line.startsWith("FAIL03")){
                        System.out.println("Please log in first");
                    }else if(line.startsWith("FAIL05")){
                        System.out.println("Pong without ping");
                    }
                }
            }

            ServerListener sl = new ServerListener();
            sl.start();

            System.out.println("Welcome to the server. Type /HELP to view commands.");

            while(running && sl.serverRunning){
                switch(state) {
                    case STATE_DEFAULT:
                        String newLine = sc.nextLine();
                        if(newLine.startsWith("/QUIT")){
                            writer.println("QUIT");
                            writer.flush();
                            running = false;
                        } else if(newLine.startsWith("/DM ")){
                            String[] content = newLine.substring(4).split(" ", 2);
                            String toUser = content[0];
                            String messageToEncrypt = content[1];
                            String password = encryptionPasswords.get(toUser);
                            if (password != null) {
                                SecretKeySpec secretKey = new SecretKeySpec(password.getBytes(StandardCharsets.UTF_8), algorithm);
                                cipher.init(Cipher.ENCRYPT_MODE, secretKey);

                                String encryptedMessage = Base64.getEncoder().encodeToString(cipher.doFinal(messageToEncrypt.getBytes()));
                                String dmMessage = "DM " + toUser + " " + encryptedMessage;

                                writer.println(dmMessage);
                                writer.flush();
                            } else {
                                queuedDm = messageToEncrypt;
                                writer.println("PUBKEY GET " + toUser);
                                writer.flush();
                            }
                        } else if(newLine.startsWith("/HELP")){
                            System.out.println("List of commands:\n" +
                                            "/QUIT to exit the server.\n"+
                                            "/LIST to view all users in the server.\n"+
                                            "/FILE SEND [username] to send a file to a user.\n"+
                                            "/POLL START to start a poll.\n"+
                                            "/POLL VOTE [pollname] to vote in a poll.\n"+
                                            "/POLL VIEW [pollname] to view a poll.\n"+
                                            "/POLL STOP [pollname] to stop a poll you started.\n"+
                                            "/DM [username] to send a private message to a user.\n"
                                    );
                        } else if(newLine.startsWith("/LIST")){
                            String content = newLine.substring(1);
                            writer.println(content);
                            writer.flush();
                        } else if(newLine.startsWith("/FILE SEND ")){
                            String content = "FILEREQUEST REQUEST " + newLine.substring(11);
                            writer.println(content);
                            writer.flush();
                        } else if(newLine.startsWith("/FILE DENY ")){
                            String content = "FILEREQUEST DENY " + newLine.substring(11);
                            writer.println(content);
                            writer.flush();
                        } else if(newLine.startsWith("/FILE ACCEPT ")){
                            fileSocket = new Socket(SERVER_ADDRESS, SERVER_FILE_PORT);
                            fileReceiver = new FileReceiver(fileSocket);
                            fileReceiver.start();

                            String content = "FILEREQUEST ACCEPT " + newLine.substring(13);
                            writer.println(content);
                            writer.flush();
                        } else if(newLine.startsWith("/POLL START")){
                            String message = "POLL START ";
                            System.out.println("Starting a poll, please enter a subject.");
                            String subject = sc.nextLine();
                            message = message + subject;
                            int numQuestions = 0;
                            boolean pollDone = false;
                            while (!pollDone){
                                if (numQuestions >= 10) {
                                    System.out.println("Maximum amount of questions reached, continuing.");
                                    pollDone = true;
                                    continue;
                                }
                                System.out.println("Enter the description of an question, type /STOP to stop adding questions.");
                                String question = sc.nextLine();
                                if(question.startsWith("/STOP")){
                                    if(numQuestions >= 1) {
                                        pollDone = true;
                                        continue;
                                    } else {
                                        System.out.println("Not enough questions, enter more or type /STOP again to quit making a poll.");
                                        String newOption = sc.nextLine();
                                        if(newOption.startsWith("/STOP")){
                                            pollDone = true;
                                            continue;
                                        } else {                                 
                                            int numAnswers = 0;
                                            boolean questionDone = false;
                                            while(!questionDone) {                            
                                                System.out.println("Enter one of the possible answers, type /NEXT to finish this question and go to the next.");
                                                String answer = sc.nextLine();
                                                if(answer.startsWith("/NEXT")){
                                                    if(numAnswers >= 2) {
                                                        questionDone = true;
                                                        continue;
                                                    } else {
                                                        System.out.println("Not enough answers, enter more or type /NEXT again to end this question and start a new one.");
                                                        String newAnswer = sc.nextLine();
                                                        if(newAnswer.startsWith("/NEXT")){
                                                            questionDone = true;
                                                            continue;
                                                        } else {
                                                            question = question + " \t " + newAnswer;
                                                            numAnswers++;
                                                        }
                                                    }
                                                } else {
                                                    question = question + " \t " + answer;
                                                    numAnswers++;
                                                }
                                            }
                                            if(numAnswers >= 2){
                                                message = message + "\t\t " + question;
                                                numQuestions++;
                                            }
                                        }
                                    }
                                } else {
                                    int numAnswers = 0;
                                    boolean questionDone = false;
                                    while(!questionDone) {                            
                                        System.out.println("Enter one of the possible answers, type /NEXT to finish this question and go to the next.");
                                        String answer = sc.nextLine();
                                        if(answer.startsWith("/NEXT")){
                                            if(numAnswers >= 2) {
                                                questionDone = true;
                                                continue;
                                            } else {
                                                System.out.println("Not enough answers, enter more or type /NEXT again to end this question and start a new one.");
                                                String newAnswer = sc.nextLine();
                                                if(newAnswer.startsWith("/NEXT")){
                                                    questionDone = true;
                                                    continue;
                                                } else {
                                                    question = question + " \t " + newAnswer;
                                                    numAnswers++;
                                                }
                                            }
                                        } else {
                                            question = question + " \t " + answer;
                                            numAnswers++;
                                        }
                                    }
                                    if(numAnswers >= 2){
                                        message = message + "\t\t " + question;
                                        numQuestions++;
                                    }                        
                                }
                            }
                            if(numQuestions >= 1){
                                writer.println(message);
                                writer.flush();
                            }
                        } else if(newLine.startsWith("/POLL VIEW ")){
                            writer.println(newLine.substring(1));
                            writer.flush();
                        } else if(newLine.startsWith("/POLL VOTE ")){
                            writer.println("POLL VOTEVIEW " + newLine.substring(11));
                            writer.flush();
                            state = STATE_POLL_VOTING;
                        } else if(newLine.startsWith("/POLL STOP ")){
                            writer.println(newLine.substring(1));
                            writer.flush();
                        } else {
                            writer.println("BCST " + newLine);
                            writer.flush();
                        }
                        break;
                    case STATE_POLL_VOTING:
                        if (currentPoll != null) {
                            String answers = "";
                            System.out.println("Voting in poll " + currentPoll.getSubject());
                            for (int i = 0; i < currentPoll.getQuestions().length; i++) {
                                Question question = currentPoll.getQuestion(i);
                                String questionResult = question.getQuestion() + "\n";
                                for (int j = 0; j < question.getOptions().length; j++) {
                                    questionResult = questionResult + "[" + j + "] " + question.getOption(j) + "\n";
                                }
                                System.out.println(questionResult);
                                System.out.println("Please enter the number of your answer");
                                int answerNumber = -1;
                                while(answerNumber < 0 || answerNumber > currentPoll.getQuestion(i).getOptions().length -1){
                                    try {
                                        answerNumber = Integer.parseInt(sc.nextLine());
                                        if(answerNumber < 0 || answerNumber > currentPoll.getQuestion(i).getOptions().length -1){
                                            System.out.println("please enter a valid number.");
                                        }
                                    }
                                    catch (Exception e){
                                        System.out.println("please enter a number.");
                                    }
                                }
                                if(answers.equals("")){
                                    answers = answers + answerNumber;
                                } else {
                                    answers = answers + "," + answerNumber;
                                }
                            }


                            writer.println("POLL VOTE "+ currentPoll.getSubject() + "\t\t" + answers);
                            state = STATE_DEFAULT;
                            System.out.println("Votes sent, you can continue chatting.");
                            continue;
                        }
                        else{
                            Thread.sleep(1000);
                            if(currentPoll == null){
                                state = STATE_DEFAULT;
                                System.out.println("Failed to receive a poll. returning to chat.");
                            }
                        }
                }
            }

            sc.close();
            sl.interrupt();
        } catch (IOException | InterruptedException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            e.printStackTrace();
            running = false;
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
    }

    private static class ServerListener extends Thread {
        private volatile boolean serverRunning = true;

        public void run() {
            while(serverRunning){
                try {
                    String newLine = reader.readLine();

                    if(newLine.startsWith("BCST ")) {
                        String userAndMessage = newLine.substring(5);
                        String[] messageArray = userAndMessage.split(" ", 2);
                        System.out.println(messageArray[0] + ": " + messageArray[1]);
                    } else if(newLine.startsWith("JOINED ")) {
                        System.out.println(newLine.substring(7) + " has joined!");
                    } else if(newLine.startsWith("PUBKEY GET ")) {
                        String name = newLine.substring(11);
                        String resultMessage = "PUBKEY SEND " + name + " " + Base64.getEncoder().encodeToString(pubKey.getEncoded());
                        writer.println(resultMessage);
                        writer.flush();
                    } else if(newLine.startsWith("PUBKEY SEND ")) {
                        String[] content = newLine.substring(12).split(" ");
                        String name = content[0];
                        String pubKeyEncoded = content[1];
                        PublicKey pubKey;

                        try{
                            byte[] byteKey = Base64.getDecoder().decode(pubKeyEncoded.getBytes());
                            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
                            KeyFactory kf = KeyFactory.getInstance("RSA");

                            pubKey = kf.generatePublic(X509publicKey);

                            // Generate password for the other user and save it
                            int leftLimit = 97; // letter 'a'
                            int rightLimit = 122; // letter 'z'
                            int targetStringLength = 16;
                            Random random = new Random();

                            String generatedPassword = random.ints(leftLimit, rightLimit + 1)
                                    .limit(targetStringLength)
                                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                                    .toString();

                            encryptionPasswords.put(name, generatedPassword);

                            Cipher encryptCipher = Cipher.getInstance("RSA");
                            encryptCipher.init(Cipher.ENCRYPT_MODE, pubKey);

//                            System.out.println("GENERATED PASSWORD IS " + generatedPassword);

                            String encryptedPassword = Base64.getEncoder().encodeToString(encryptCipher.doFinal(generatedPassword.getBytes()));

                            String messageToSend = "PASSWORD SEND " + name + " " + encryptedPassword;

                            writer.println(messageToSend);
                            writer.flush();
                        }
                        catch(Exception e){
                            e.printStackTrace();
                        }

                    } else if(newLine.startsWith("PASSWORD SEND ")) {
                        String[] content = newLine.substring(14).split(" ");
                        String name = content[0];
                        String passwordEncoded = content[1];

                        Cipher encryptCipher = Cipher.getInstance("RSA");
                        encryptCipher.init(Cipher.DECRYPT_MODE, privKey);

                        byte[] decodedBytes = Base64.getDecoder().decode(passwordEncoded);
                        String password = new String(encryptCipher.doFinal(decodedBytes));

//                        System.out.println("RECEIVED PASSWORD IS " + password);

                        encryptionPasswords.put(name, password);

                        writer.println("PASSWORD OK " + name);
                        writer.flush();
                    } else if(newLine.startsWith("PASSWORD OK ")) {
                        String name = newLine.substring(12);

                        String messageToEncrypt = queuedDm;
                        queuedDm = null;

                        SecretKeySpec secretKey = new SecretKeySpec(encryptionPasswords.get(name).getBytes(StandardCharsets.UTF_8), algorithm);
                        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

                        String encryptedMessage = Base64.getEncoder().encodeToString(cipher.doFinal(messageToEncrypt.getBytes()));
                        String dmMessage = "DM " + name + " " + encryptedMessage;

                        writer.println(dmMessage);
                        writer.flush();
                    } else if(newLine.startsWith("DM ")) {
                        String[] content = newLine.substring(3).split(" ", 2);
                        String name = content[0];
                        String encryptedMessage = content[1];
                        String message;
                        String password = encryptionPasswords.get(name);

                        if (password != null) {
                            SecretKeySpec secretKey = new SecretKeySpec(password.getBytes(StandardCharsets.UTF_8), algorithm);
                            cipher.init(Cipher.DECRYPT_MODE, secretKey);
                            byte[] decodedBytes = Base64.getDecoder().decode(encryptedMessage);
                            message = new String(cipher.doFinal(decodedBytes));
                            message = message.trim();
                            System.out.println("DM from " + name + ": " + message);
                        } else {
                            System.err.println("Failed to decode message.");
                        }
                    } else if(newLine.startsWith("OK BCST ")) {
                    } else if(newLine.startsWith("FILEREQUEST DENY ")) {
                        String name = newLine.substring(17);
                        System.out.println("Sending file was denied.");
                    } else if(newLine.startsWith("FILEREQUEST REQUEST ")) {
                        String name = newLine.substring(20);
                        System.out.println("File send request from " + name + ". \n Please type /FILE DENY " + name + " or /FILE ACCEPT " + name);
                    } else if(newLine.startsWith("FILEREQUEST ACCEPT")) {
                        fileSocket = new Socket(SERVER_ADDRESS, SERVER_FILE_PORT);
                        fileSender = new FileSender(fileSocket);
                        fileSender.start();
                    } else if(newLine.startsWith("OK POLL VOTE ")) {
                    } else if(newLine.startsWith("OK POLL VOTEVIEW ")) {
                        String[] content = newLine.substring(17).split("\t\t");
                        String subject = content[0];

                        Question[] questions = new Question[content.length - 1];

                        for (int i = 1; i < content.length; i++) {
                            String[] questionContent = content[i].split("\t");
                            String question = questionContent[0] + "\n";

                            String[] answers = new String[questionContent.length - 1];

                            for (int j = 1; j < questionContent.length; j++) {
                                String[] answerVotes = questionContent[j].split(":");
                                answers[j-1] = answerVotes[0];
                            }
                            questions[i-1] = new Question(question, answers);
                        }
                        
                        currentPoll = new Poll ("noName", subject, questions);

                    } else if(newLine.startsWith("OK POLL VIEW ")) {
                        int numberOfAnswers = 0;
                        String[] content = newLine.substring(13).split("\t\t");
                        String subject = content[0];
                        String[] subjectContent = subject.split(":");

                        String result = "Poll " + subjectContent[0] + ". " + subjectContent[1] + " deelnemers. \n";

                        for (int i = 1; i < content.length; i++) {
                            String[] questionContent = content[i].split("\t");
                            String question = questionContent[0] + "\n";
                            for (int j = 1; j < questionContent.length; j++) {
                                String[] answerVotes = questionContent[j].split(":");
                                question = question + " - " + answerVotes[0] + ": " + answerVotes[1] + "\n";
                            }
                            result = result + " \n " + question;
                        }
                        System.out.println(result);
                    } else if(newLine.startsWith("OK LIST ")) {
                        System.out.println("The following people are connected to the chat server:");
                        System.out.println(newLine.substring(8));
                    } else if(newLine.startsWith("OK Goodbye")) {
                        System.out.println("Successfully disconnected.");
                        serverRunning = false;
                    } else if(newLine.startsWith("FAIL")) {
                        if(newLine.startsWith("FAIL00")){
                            System.err.println("Unkown command");
                        }else if(newLine.startsWith("FAIL01")){
                            System.err.println("User already logged in");
                        }else if(newLine.startsWith("FAIL04")){
                            System.err.println("User cannot login twice");
                        }else if(newLine.startsWith("FAIL05")){
                            System.err.println("Pong without ping");
                        }else if(newLine.startsWith("FAIL08")){
                            System.err.println("Need 3 users online to start a poll");
                        }else if(newLine.startsWith("FAIL33")){
                            System.err.println("can not stop a poll that you did not start");
                        }else {
                            System.err.println("Received unknown error: \n" + newLine);
                        }
                    } else if(newLine.startsWith("PING")) {
                        writer.println("PONG");
                        writer.flush();
                    }
                } catch (IOException | InvalidKeyException e) {
                    e.printStackTrace();
                    serverRunning = false;
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
            this.interrupt();
        }
    }

    private static class FileSender extends Thread{
        private Socket fileSenderSocket;

        private DataOutputStream dataOutputStream;

        public FileSender(Socket givenSocket){this.fileSenderSocket=givenSocket;}

        public void run(){
            try {
                dataOutputStream = new DataOutputStream(fileSenderSocket.getOutputStream());

                String path = "./AAA.pdf";
                int bytes = 0;
                File file = new File(path);
                FileInputStream fileInputStream = new FileInputStream(file);

                // send file checksum
                byte[] fileBytes = Files.readAllBytes(Paths.get(path));
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                byte[] md5Bytes = md5.digest(fileBytes);

                long md5Long = 0;
                for (byte b : md5Bytes) {
                    md5Long = (md5Long << 8) + (b & 0xFF);
                }

                dataOutputStream.writeLong(md5Long);
                dataOutputStream.flush();

                // send file size
                dataOutputStream.writeLong(file.length());
                // break file into chunks
                byte[] buffer = new byte[4 * 1024];

                while ((bytes = fileInputStream.read(buffer)) != -1) {
                    dataOutputStream.write(buffer, 0, bytes);
                    dataOutputStream.flush();
                }

                fileInputStream.close();

            }
            catch (Exception e){

            }
            this.interrupt();
        }
    }

    private static class FileReceiver extends Thread {
        private Socket fileReceiverSocket;

        private DataInputStream dataInputStream;

        public FileReceiver(Socket fileReceiverSocket) {
            try {
                this.fileReceiverSocket = fileReceiverSocket;

                dataInputStream = new DataInputStream(fileReceiverSocket.getInputStream());
            } catch (Exception e){
                System.out.println(e);
            }
        }

        public void run() {
            try {
                String path = "./BBB.pdf";
                FileOutputStream fileOutputStream = new FileOutputStream(path);
                int bytes = 0;

                long checksum = dataInputStream.readLong();

                long size = dataInputStream.readLong();     // read file size
;
                byte[] buffer = new byte[4 * 1024];

                while (size > 0 && (bytes = dataInputStream.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
                    fileOutputStream.write(buffer, 0, bytes);
                    size -= bytes;      // read upto file size
                }

                fileOutputStream.close();

                // check file checksum
                byte[] fileBytes = Files.readAllBytes(Paths.get(path));
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                byte[] md5Bytes = md5.digest(fileBytes);

                long md5Long = 0;
                for (byte b : md5Bytes) {
                    md5Long = (md5Long << 8) + (b & 0xFF);
                }


                if(md5Long == checksum){
                    System.out.println("System: Successfully received file.");
                } else {
                    System.out.println("System: Sent file was corrupted.");
                }
            }catch(Exception e){
                System.out.println(e);
            }
            this.interrupt();
        }
    }
}