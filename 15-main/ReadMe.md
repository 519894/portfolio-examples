Project: Client On The Board Carthago
=======================

# inhoudsopgave
- [Hoe je de database opzet](#hoe-je-een-database-opzet)
- [AVGBeveiliging](#avg-beveiliging)
- [Client communication log](#client-communication-log)
- [Onze code of conduct and definition of done](#code-of-conduct-and-definition-of-done)
- [Daily scrum log](#daily-scrum-log)
- [Database design](#database-design)
- [Endpoint documentatie](#endpoint-documentatie)
- [Functional design](#functional-design)
- [GitCanary en repo analyse](#gitcanary-en-repo-analyse)
- [Requirements](#requirements)
- [Sprint retrospectives](#sprint-retrospectives)
- [Technical design](#technical-design)
- [Website guide](#website-guide)


## Hoe je een database opzet 
We hebben voor ons project gebruik gemaakt van pgadmin 4 voor onze database. 
Voor het verbinden met de database hebben we een document gemaakt met informatie over hoe u de database moet opzetten 
om het te laten verbinden met de backend. Zie het document [readme](documentation/readme.md).

## AVG beveiliging
Voor onze applicatie hebben we beveiliging toegevoegd met betrekking tot de AVG wetgeving.
Hiervoor hebben we de beveiliging gedocumenteerd in een documment zodat we centraal hebben wat we gaan gebruiken qua gegevens.
Zie hiervoor het document [AVGBeveiliging](documentation/Project docs/AVGBeveiliging.md).

## Client communication log
Voor ons contact met de klant houden we ook een log bij met wanneer we contact hebben en hoe we contact op kunnen nemen.
Zie document [Client communication log](documentation/Project docs/Client communication log.md).

## Code of conduct and definition of done
We hadden voordat we begonnen aan het werken hebben we een code of conduct en definition of done geschreven.
Hierbij hebben we onder andere opgeschreven wat onze regelementen qua gedrag en code.
Zie het document [Code of conduct and definition of done](documentation/Project%20docs/Code%20of%20conduct%20and%20definition%20of%20done.md).

## Daily scrum log
We hebben onze scrum meetings bijgehouden, zodat we kunnen bijhouden wie aan welk onderwerp werkt.
Zie document [Daily scrum log](documentation/Project docs/Daily scrum log.md).

## Database Design
Ons design voor de database hebben we gedocumenteerd en in markdown gezet.
Zie document [Database Design](documentation/Project docs/Database Design.md).

## Endpoint documentatie
Om bij te houden welke http requests we hebben gemaakt hebben we dit gedocumenteerd.
Zie document [Endpoint documentatie](documentation/Project docs/Endpoint documentatie.md).

## Functional design
Ons functional design hebben we bijgehouden in markdown.
Zie document [Functional design](documentation/Project docs/Functional design.md).

## GitCanary en repo analyse
We hebben een document in markdown gemaakt om onze analyse van gicanary en de repository duidelijk te maken.
Zie document [GitCanary en repo analyse](documentation/Project docs/GitCanary en repo analyse.md).

## Requirements
We hebben om bij te houden wat de requirements zijn van zowel ons als de klant. Hebben we alles samengevoegd in een bestand.
Zie document [Requirements](documentation/Project docs/Requirements.md).

## Sprint retrospectives
Aan het einde val elke sprint houden we een retrospective en documenteren we dat.
Zie document [Sprint retrospectives](documentation/Project docs/Sprint retrospectives.md).

## Technical design
Het technische design van deze applicatie is ook hier te vinden.
Zie document [Technical design](documentation/Project docs/Technical design.md).

## Website guide
Om duidelijk te maken wat er allemaal in de website gedaan kan worden en hoe dit gedaan moet worden hebben wij een markdown document bijgehouden.
Zie document [Website guide](documentation/Project docs/Website guide.md).

