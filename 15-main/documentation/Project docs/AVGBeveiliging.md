# AVG beveiligingsregels

13-01-2023

Project Client On Board

Team FreeBird

## Inhoudsopgave

**[Inleiding](#_xhb9axdasxzh) 1**

**[Wat zijn AVG regels?](#_lp20uw60hmhw) 2**

**[Waarom zijn er AVG regels?](#_l3s3pib0s684) 2**

**[Om welke persoonsgegevens gaat het?](#_ilum8anm0nhx) 2**

[Algemene persoonsgegevens](#_mi5lp6rptdcp) 2

[Bijzondere persoonsgegevens](#_f2jdpqze01uz) 2

**[Wanneer moeten de gegevens worden verwijderd?](#_1ssnxtfmyvo6) 2**

**[Welke regels zijn van toepassing voor de website?](#_nn0913zes7su) 3**

**[Conclusie](#_e66a1se1u8kf) 3**

**[Bronnen:](#_lezq8r18o3w4) 4**

##


## Inleiding

Voor een project voor school moest er een website gemaakt worden voor een echte klant. Op deze website moest er een verkiezing gehouden kunnen worden en daarvoor moesten mensen inloggen met een e-mail van het bedrijf. Om een website te maken waar mensen kunnen inloggen moet je gebruikmaken van de AVG wet en aangezien er nog niet heel veel duidelijk over was, is er besloten om een document op te stellen waarin alle belangrijke informatie staat. Dat is dit document.

##


## Wat zijn AVG regels?

AVG regels zijn regels die gemaakt zijn voor de bescherming van persoonsgegevens. Deze wet is op 25 mei 2018 ingegaan in de hele Europese Unie. Dat houdt in dat er in de hele Europese Unie dezelfde regels zijn omtrent het gebruik van persoonsgegevens. In het Engels heet deze wet GDPR, ofwel de General Data Protection Regulation.

## Waarom zijn er AVG regels?

De AVG regels zijn gemaakt om de privacy van mensen beter te beschermen. Door de AVG moeten bedrijven en ondernemers zorgvuldiger omgaan met de persoonsgegevens van de klanten. Daarnaast moeten ze ook zorgvuldiger omgaan met de persoonsgegevens van het eigen personeel.

De achterliggende gedachte hierachter is dat burgers meer controle over hun eigen persoonsgegevens krijgen. De overheid is druk bezig om dit verder te ontwikkelen zodat iedereen eenvoudig zijn of haar informatie van het internet kan verwijderen.

## Om welke persoonsgegevens gaat het?

### Algemene persoonsgegevens

Persoonsgegevens die onder de AVG-wet vallen zijn iemands naam, adres, woonplaats, telefoonnummers en postcodes met huisnummers. Daarnaast vallen mailadressen ook onder de AVG-wet. Als een zakelijke mail een naam of een voornaam bevat valt deze ook onder de AVG-wet, deze info leidt terug tot een persoon.

### Bijzondere persoonsgegevens

Iemands ras, godsdienst of gezondheid valt onder bijzondere persoonsgegevens. Deze gegevens zijn door de wetgever extra beschermd. Deze gegevens zijn verboden te verwerken, behalve als er een wettelijke uitzondering is.

## Wanneer moeten de gegevens worden verwijderd?

In de AVG-wet staat geen concreet bewaartermijn voor persoonsgegevens. Organisaties en bedrijven mogen zelf kiezen hoelang ze de gegevens bewaren.

##


## Welke regels zijn van toepassing voor de website?

Er zijn bepaalde regels voor het opslaan van persoonsgegevens. Aangezien wij persoonsgegevens opslaan zullen we hier te maken mee hebben. Hieronder staan een aantal belangrijke die moeten worden toegepast op de website.

**Een privacyverklaring**

In de privacyverklaring moet aangegeven worden of de gegevens die worden gebruikt door de website met andere partijen worden gedeeld en hoe lang je die gegevens bewaard. Daarnaast moeten klanten op de hoogte worden gesteld dat ze een klacht mogen indienen bij de Autoriteit Persoonsgegevens.

**Onnodige accounts moeten verwijderd worden**

Voor de back-end en de front-end van de website is het verplicht om accounts die geen toegang dienen te hebben tot persoonsgegevens uit te schakelen.

**Cookies akkoord**

Naast de AVG-wet bestaat er ook een cookiewet. Hierin staan de regels omtrent het gebruik van cookies op websites. Er zijn drie soorten cookies: Functionele cookies, analytische cookies en marketing cookies. Functionele cookies zijn cookies die nodig zijn om de website te laten functioneren. Analytische cookies zijn cookies die nodig zijn om een analyse te kunnen maken van de statistieken van de website. Marketing cookies zijn cookies die bedoeld zijn om gerichte reclame te laten zien aan een gebruiker.

Voor functionele cookies hoeft geen toestemming gevraagd te worden. Analytische cookies mogen gebruikt worden zonder toestemming mits ze niet met andere partijen worden gedeeld. Voor marketing cookies moet wel toestemming gevraagd worden.

## Conclusie

in conclusie, De AVG wet is belangrijk want hierdoor worden persoonsgegevens beschermt. Om aan de AVG wet te voldoen is het belangrijk dat er een privacyverklaring is, daarnaast moeten onnodige accounts verwijderd worden en er moet een cookies akkoord aanwezig zijn. Dit zijn de belangrijkste AVG regels die toegepast moeten worden op een website.

## Bronnen:

- [https://www.hostnet.nl/academy/avg-proof-website#:~:text=De%20AVG%20stelt%20geen%20specifieke,en%20donateurs%20veilig%20te%20verwerken](https://www.hostnet.nl/academy/avg-proof-website#:~:text=De%20AVG%20stelt%20geen%20specifieke,en%20donateurs%20veilig%20te%20verwerken).
- [https://www.autoriteitpersoonsgegevens.nl/nl/over-privacy/persoonsgegevens/bewaren-van-persoonsgegevens#:~:text=Er%20is%20op%20grond%20van,zijn%20verzameld%20of%20worden%20gebruikt](https://www.autoriteitpersoonsgegevens.nl/nl/over-privacy/persoonsgegevens/bewaren-van-persoonsgegevens#:~:text=Er%20is%20op%20grond%20van,zijn%20verzameld%20of%20worden%20gebruikt).
- [https://www.rijksoverheid.nl/onderwerpen/privacy-en-persoonsgegevens/privacyregels-beschermen-persoonsgegevens](https://www.rijksoverheid.nl/onderwerpen/privacy-en-persoonsgegevens/privacyregels-beschermen-persoonsgegevens)
- [https://autoriteitpersoonsgegevens.nl/nl/onderwerpen/algemene-informatie-avg/algemene-informatie-avg](https://autoriteitpersoonsgegevens.nl/nl/onderwerpen/algemene-informatie-avg/algemene-informatie-avg)