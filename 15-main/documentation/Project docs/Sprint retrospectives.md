# FreeBird, Sprint retrospectives

Project Client on board, Saxion Hogeschool

Carthago ICT - Verkiezingssysteem

Sprint 1, Sprint retrospectives - 18/12/2022

Sprint 2, Sprint retrospectives - 15/01/2023

# Inhoud

[Korte uitleg](#_Toc122260235)

[Sprint 1](#_Toc122260236)

[Goede punten](#_Toc122260237)

[Verbeterpunten](#_Toc122260238)

[Sprint 2](#_Toc122260239)

[Goede punten](#_Toc122260240)

[Verbeterpunten](#_Toc122260241)

[Sprint 3](#_Toc122260242)

[Goede punten](#_Toc122260243)

[Verbeterpunten](#_Toc122260244)

[Hoe te verbeteren](#_Toc122260245)

# Korte uitleg

Voor dit project hebben we na iedere sprint een sprint retrospective gehouden. In de sprint retrospectives is terug te vinden wat er volgens het groepje allemaal goed ging tijdens het project, wat er allemaal wat minder goed ging tijdens het project en hoe we de minder goede punten zouden kunnen verbeteren.

De notulen van de sprint retrospectives werden steeds op de laatste les van de sprint gemaakt. Hierbij heeft ieder teamlid notulen gemaakt voor wat deze persoon goed vond gaan in het project, minder goed vond gaat in het project en hoe deze persoon dacht dat dit verbeterd kon worden. Zodra ieder teamlid klaar was met de notulen maken werden al deze notulen verzameld en samengevoegd. Uit het resultaat van de vorige stap zijn vervolgens voor iedere sprint de meest voorkomende of belangrijke resultaten genomen en in dit verslag gezet.

# Sprint 1

## Goede punten

In de eerste sprint waren al veel dingen goed gegaan volgens onze groep. Zo was een van de goede punten dat mensen de samenwerking en communicatie tussen iedereen heel goed vond gaan. Een ander goed punt dat uit de notulen kwam was dat mensen over het algemeen tevreden waren met een goede taakverdeling waardoor alles op tijd af kwam. Ook waren de mensen in onze groep tevreden met het eindresultaat van de sprint die we aan de klant hebben laten zien.

## Verbeterpunten

Naast goede dingen waren er ook een aantal gezamenlijke punten waarvan we vonden dat het iets minder goed ging en waar dus verbetering in zou kunnen plaatsvinden. Zo hadden soms mensen een taak op zich gepakt die eigenlijk bij een andere taak hoorde terwijl er al iemand met de andere taak bezig was. Onze oplossing om dit te verbeteren voor de volgende sprint was om issues aan elkaar te linken op gitlab zodra ze werden gemaakt. Door gebruik te maken van deze feature kan een taak niet meer opgepakt worden als de andere taak nog niet af is gemaakt. Dit zou er ook meteen weer voor moeten zorgen dat iemand niet per ongeluk een taak pakt die bij een andere taak hoort waar iemand anders al mee bezig is.

Een ander verbeterpunt wat uit de notulen kwam was dat de benaming van de branches niet altijd consistent was zoals vermeld stond in de regelgeving. De oplossing die hierop werd bedacht was dat de mensen die in het vervolg een merge request na gingen kijken ook zullen gaan kijken of de branch naam klopt volgens de door ons bepaalde naamgeving. Als dit niet het geval zou zijn dan zou de persoon die de merge request nakijkt de persoon die de branch heeft aangemaakt moeten benaderen en dit vertellen. Zo kan de persoon die de branch heeft aangemaakt in het vervolg er beter op letten.

Het laatste punt wat meer naar boven kwam binnen de groep was dat het niet altijd duidelijk was wat er exact naar de klant werd toegestuurd. Iedereen wist wel wat er gestuurd zou worden maar niet hoe het geformuleerd was in de mails. Ook voor de mails die we weer terug ontvingen wist een deel van de groep niet exact wat er in de mail stond omdat ze dit via een ander te horen kregen. De oplossing die we hiervoor hadden bedacht was dat we in het vervolg iedereen als cc aan de mails toevoegen. Ook hadden we afgesproken dat we in het vervolg voordat we een mail sturen het eerst doorsturen in discord zodat er feedback op gegeven kan worden. Hierdoor zou de communicatie die tussen de klant en ons plaats vindt en stuk duidelijker moeten worden.

# Sprint 2

## Goede punten

Na sprint 1 zijn er verbeterpunten naar voren gekomen over het gebruik van branches en branch namen, over communicatie van welke issues worden opgepakt en dat we het duidelijk hebben met elkaar wanneer we elkaar ontmoeten buiten de les. Dit zijn dingen die tijdens sprint 1 beter hadden gekund, die wij in sprint 2 beter hebben gedaan. Het is goed dat het zichtbaar is dat deze problemen niet terugkomen nadat het is genoemd als probleem. Tijdens sprint 2 is de groep doorgegaan met elkaar goed en professioneel behandelen. Dit heeft ervoor gezorgt dat wij aan het einde van sprint 2 en goed product hebben kunnen leveren.

## Verbeterpunten

Met de verbeteringen die zijn gemaakt tijdens sprint 2 lijkt het er op dat wij als groep minder fouten maken. Daarnaast zijn er toch onduidelijkheden ontstaan in de planning en het werk tijdens de vakantie, dit begon dat voor de vakantie niet duidelijk was wanneer iedereen van plan was hun eerste issue van de sprint af te maken, wat het moeilijker maakte om hier omheen te plannen. In de tweede week van de vakantie werden de eerste issues afgemaakt en meer opgepakt, helaas duurde het langer dan verwacht dat de merge requests nagekeken waren, wat de planning in de weg zat. In de volgende sprint moet de groep duidelijker zijn over hun planning en proactiever merge requests nakijken.

Aangezien de volgende sprint de polishing fase is voor het project, wil de groep beter opletten wanneer er warnings zijn binnen de code, en wanneer er inconsistencies zijn in de front-end.

# Sprint 3

## goede punten

Erik
- tijdens sprint 3 zijn we tegen moeilijke issues voor mensen aangekomen en teamleden hebben elkaar geholpen daarmee.
- Wij hebben geleerd van onze fouten in sprint 2 en ze niet herhaald.
- we waren niet last minute klaar met sprint 3 en voorkwamen stress

Fabian
- De applicatie werkt goed
- Elk team lid heeft goed zijn issues voltooid indien mogelijk

Jordy
- We hebben de meeste issues kunnen afmaken in de polish fase.
- We hebben de meeste polishing punten die de klant heeft opgenoemd aan het einde van sprint 2 kunnen verwerken.
- De klant was tevreden met onze resultaten.

Maurits H
- De applicatie werkt goed en bevat bijna alles wat we wouden
- De klant was blij met het resultaat
- We hebben ervaring gekregen in onderdelen waar we eerst niet veel over wisten

Maurits TD
- we hebben alle taken af gekregen en een mooi resultaat neergezet
- de klant wat blij met het product

Simon
- We hebben in de polish fase de meeste punten kunnen verbeteren.
- De presentatie ging erg goed.

Thom
- De meeste issues zijn afgekomen
- De klant was tevreden met het resultaat

## verbeterpunten

Erik

- Wij hebben geen Powerpoint gemaakt als intro voor de klant presentatie.
- er leken laat in de sprint nog veel kleine fouten in styling die opgelost moesten worden.

Fabian
- Aan het einde van de sprint moesten er nog ineens allemaal (kleine) veranderingen plaatsvinden
- Er was niet goed begrepen dat een OR verkiezing meerdere winnaars kan hebben

Jordy
- We hebben de meeste issues kunnen afmaken in de polish fase.
- We hebben de meeste polishing punten die de klant heeft opgenoemd aan het einde van sprint 2 kunnen verwerken.
- De klant was tevreden met onze resultaten.

Maurits H
- Net voor het einde waren er toch nog wel wat bugs die snel opgelost moesten worden
- We hebben de sprint net niet helemaal afgekregen

Maurits TD
- de dag van tevoren moest er nog best wel veel gebeuren
- op het eind (toen er een beetje nood aan de man was) duurde het lang voordat de merge requests werden nagekeken

Simon
- We hadden als enige groepje geen powerpoint bij de presentatie.
- Er waren een aantal onverwachte bugs gevonden en gefixed worden een dag van te voren en op de dag van de presentatie.
- Het is jammer dat het niet gelukt was om https of encryptie voor wachtwoorden toe te voegen aan het project.

Thom
- Wij hadden niet goed begrepen dat er uit een verkiezing voor een OR meerdere winnaars moeten komen


## hoe te verbeteren
Erik
- beter in gedacht houden hoe de klant het product ziet en niet hoe wij het zien.
- zorgvuldiger kijken naar wat er mis is met het project voordat we in de laatste fase waren

Fabian
- De deadline voor het eindproduct een dag eerder leggen, zodat er nog speling is voor onverwachte problemen.
- De requirements moeten specifieker zijn en aannames moeten nagekeken worden met de klant.

Jordy
- voor een presentatie een powerpoint maken.

Maurits H
- Goed blijven bugtesten bij merge requests
- Beter onderzoek doen naar issues en of we dit mogelijk kunnen halen

Maurits TD
- volgende keer duidelijk hebben wat er nog moet gebeuren en deze taken verdelen
- de laatste week moet het programma een keer heel goed worden gereviewed, dan kunnen onderliggende bugs gevonden worden en hoeft dit niet een dag van tevoren te worden gefixt.

Simon
- Powerpoint voorbereiden bij de presentatie
- In de eerste sprint al een beetje onderzoek doen naar https
- Een week voor de deadline geen features meer toevoegen zodat iedereen in de laatste week grondig de applicatie kan testen op bugs.

Thom
- Door betere afspraken te maken met de klant