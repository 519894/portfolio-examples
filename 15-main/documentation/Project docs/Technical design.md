# FreeBird, Technisch design

Project Client on board, Saxion Hogeschool

Carthago ICT - Verkiezingssysteem

Sprint 1, Technisch Ontwerp

## **Inleiding**
* [Introduction](#Introduction)
* [Design considerations](#Design_considerations)
* [System architecture](#System_architecture)
* [Logical view (functional components)](#Logical_view_(functional components))
* [Hardware architecture (deploy)](#Hardware_architecture_(deploy))
* [Information architecture](#Information_architecture)
* [Security architecture](#Security_architecture)
* [Performance](#Performance)
* [Database design](#Database_design)
* [User interface design](#User_interface_design)
* [Hardware design](#Hardware_design)
* [Software design](#Software_design)
* [Opbouw Svelte](#Opbouw_Svelte)
* [Opbouw Spring Boot](#Opbouw_Spring_Boot)
* [Entiteiten](#Entiteiten)
* [User](#User)
* [Election](#Election)
* [ElectableUser](#ElectableUser)
* [Voter](#Voter)
* [Token](#Token)
* [Controllers](#Controllers)
* [UserController](#UserController)
* [ElectionController](#ElectionController)
* [ElectableUserController](#ElectableUserController)
* [LoginController](#LoginController)
* [Services](#Services)
* [UserService](#UserService)
* [ElectionService](#ElectionService)
* [ElectableUserService](#ElectableUserService)
* [VoterService](#VoterService)
* [TokenService](#TokenService)
* [EmailService](#EmailService)
* [Repositories](#Repositories)
* [Samenwerking geheel](#Samenwerking_geheel)
* [Test plan, code reviews en quality control](#Test_plan,_code_reviews_en_quality_control)
* [Security design](#Security_design)





# Introduction

Voor het vak Client on board werken studenten aan een opdracht waar een bedrijf als klant werkt, Carthago ICT geeft de opdracht en beoordeelt of het resultaat naar verwachting is.

De opdracht is een verkiezingssoftware maken voor het OR van Carthago.

In dit document leggen wij het technisch design uit voor deze opdracht.

We gaan onze aanpak uitleggen, onze design keuzes, het systeem architectuur en het systeem design. General overview and approach

We gaan een OR systeem maken voor Carthago. Hiervoor wordt een fullstack applicatie gemaakt met een front-end gemaakt met Svelte en een back-end gemaakt met Java Spring Boot. De back-end communiceert met de front-end en met een SQL database.


# Design considerations

Bij het maken van een verkiezingssysteem is beveiliging zeer belangrijk. Hier houden wij dan ook veel rekening mee, zodat het systeem zo veilig mogelijk is. Ook mag niemand in het systeem zien wie op wie heeft gestemd. Hier moeten wij dan ook veel rekening mee houden.


# System architecture

De frontend zal geschreven worden met het express framework. Dit runt op javascript. De frontend zal ook gebruik maken van html en css. De backend zal geschreven worden in java. Hier wordt de java springBoot framework gebruikt. Er is ook een database server, die op postgresql runt.


# Logical view (functional components)

De back-end kan een kiezer laten inloggen, verkiesbaar stellen en kiezen. Verder kan de back-end keuzes opslaan in een database en ophalen. De back-end kan ook aan het einde van een verkiezing vaststellen welke verkiesbare hoeveel stemmen heeft gekregen. Tot slot kan de back-end een verkiezing starten en eindigen.


# Hardware architecture (deploy)

Het programma gaat uiteindelijk runnen in een cloud, zoals Azure devops. Voor development en testen runnen we het programma via desktop of laptop.

Voor dit programma zijn er meerdere servers die met elkaar werken. De client zal in een browser de website bezoeken. Deze request zal bij de svelte frontend server aankomen. Hierna worden in de browser de gegevens geladen vanaf de Java Springboot bankend server. De backend haalt deze data op van de Postgresql server op en stuurt de juiste response naar de browser. De backend server zal ook emails sturen naar de mailserver voor invites en reminders.

![alt_text](images/TechnicalDesign/UML Deployment Diagram.vpd.png "image_tooltip")

# Information architecture

In de database zullen 3 objecten zijn. Verkiezingen, gebruikers en verkiezings-profielen voor een verkiezing. Het schema zal er als volgend uit gaan zien.

**Gebruiker**



* id
* email
* wachtwoord hash
* rol (kiezer/admin)
* verkiezing id(s)

**verkiezingen**



* id
* beschrijving
* stemmen per verkiesbare
* optioneel: regels

**verkiezings-profielen**



* user id
* verkiezings id
* foto
* biografie


# Security architecture

Voor de beveiliging van het verkiezingssysteem is het belangrijk dat gebruikers niet bij de informatie kunnen waar ze niet bij horen. Het belangrijkste probleem rond beveiliging is het resultaat na de verkiezing en tijdens de verkiezing. Ondanks dat admins het uiteindelijke resultaat krijgen, mogen zij niet kunnen zien wat de tussentijdse resultaten zijn om te voorkomen dat individuele stemmen kunnen achterhaald worden.


# Performance

Er wordt verwacht dat er maximaal honderd users zijn. Deze gaan in de loop van een dag stemmen op de mensen die zich verkiesbaar hebben gesteld. Het verkiezingssysteem moet in staat zijn om dit aantal users af te handelen. Verder is er geen andere vereiste op het gebied van performance.


# Database design




![alt_text](images/TechnicalDesign/image1.png "image_tooltip")
\
database design figuur 1.

De database bestaat uit een admin, user, electable, voter en election. De admin en user zijn met elkaar verbonden op basis van een id. De user is met de electable verbonden op basis van het id. Dit heeft ook een 1 op veel relatie. Aangezien er maar een election is er een 1 op veel relatie tussen de election en electable en de voter. Ook his hieronder onze tijdelijke code te zien van dbdiagram.io


# User interface design

De  interface van het verkiezingssysteem wordt ontworpen als een website gemaakt met HTML-CSS en maakt gebruik van svelte. Svelte zorgt ervoor dat de interface snel en responsive werkt. Het ontwerp van de interface is te vinden in de wireframes.


# Hardware design

De website voor het verkiezingssysteem hoort te draaien op desktop, en mogelijk ook mobiele apparaten, via standaard browsers waarvan verwacht wordt dat er mogelijk gebruik van gemaakt wordt.

Het systeem wordt geïmplementeerd door het projectteam met InteliJ op desktop. Het gebruik van de database voor het verkiezingssysteem wordt mogelijk gemaakt door de Azure cloud van Carthago-ICT

De svelte frontend server, springboot backend server, database server en email server moeten allemaal in de azure cloud gaan runnen.

# Software design

Voor de wireframes hebben we gebruik gemaakt van Invision. We programmeren met Svelte voor de front-end en het Spring boot framework voor de backend in IntelliJ Ultimate Edititon. De database maken we met behulp van PgAdmin en PostgreSQL.

## Opbouw Svelte

Voor elke pagina die te zien is in de mockups van het functional design zullen aparte componenten gemaakt worden. Bij het maken van de pagina’s wordt er gekeken naar onderdelen die hergebruikt kunnen worden. Van deze onderdelen worden componenten gemaakt zodat het makkelijk te hergebruiken is. Dit zorgt ervoor dat toekomstige wijzigingen makkelijker zijn en de applicatie een consistente stijl behoudt.

## Opbouw Spring Boot

In dit onderdeel wordt de opbouw van de backend uitgelegd en hoe deze systemen met elkaar communiceren. Hierdoor zou het duidelijk moeten worden hoe de applicatie in elkaar zit.

### Entiteiten

De entiteiten in het project zullen verantwoordelijk zijn voor het structureren van data in een manier waarop het makkelijk gebruikt kan worden voor het maken van de website.

#### User

De user entiteit bevat alle informatie over een gebruiker zoals een email, een wachtwoord en een rol. Ook bevat de gebruiker informatie over in welke verkiezingen ze een stemmer zijn en in welke verkiezingen ze een kandidaat zijn.

#### Election

De election entiteit bevat alle informatie over een verkiezing zoals de instellingen, de stemmers en de kandidaten van de verkiezing.

#### ElectableUser

De electable user entiteit bevat alle informatie over een gebruiker die zichzelf verkiesbaar heeft gesteld in een verkiezing. Een aantal voorbeelden hiervan zijn: de naam, beschrijving, slogan en standpunten van de gebruiker.

#### Voter

De voter entiteit bevat de informatie of een gebruiker al heeft gestemd in een verkiezing waar deze gebruiker voor was uitgenodigd.

#### Token

De token entiteit zal gebruikt worden om een nieuw wachtwoord voor een gebruiker in te kunnen stellen. Deze entiteit zal zorgen voor de authenticatie van de gebruiker.

### Controllers

De controllers in het project zullen ervoor zorgen dat er allemaal endpoints beschikbaar zijn waardoor de front-end met de backend zal kunnen communiceren.

#### UserController

Deze controller zal alle endpoints bevatten voor het ophalen, maken, verwijderen en bewerken van gebruikers.

#### ElectionController

Deze controller zal endpoints bevatten voor alle gewenste acties in de applicatie die te maken hebben met een verkiezing. Een voorbeeld hiervan is het ophalen, maken, verwijderen en bewerken van verkiezingen. Een ander voorbeeld is het toevoegen van een kandidaat of het doorgeven van stemmen.

#### ElectableUserController

Deze controller zal alle endpoints bevatten voor het ophalen, maken, verwijderen en bewerken van verkiesbare gebruikers.

#### LoginController

Deze controller bevat de endpoint om in te kunnen loggen als een gebruiker.

### Services

De services in het project zijn ervoor om een tussenlaag te bieden tussen de controllers en de repositories. Door gebruik te maken van services is het makkelijker om in de toekomst de structuur van de applicatie te veranderen.

#### UserService

De user service zorgt ervoor dat een of meerdere gebruikers opgehaald kunnen worden uit de database. Ook heeft deze service de functionaliteit om een gebruiker te maken, bewerken of verwijderen.

#### ElectionService

De election service zorgt ervoor dat de gegevens van een of meerdere verkiezingen opgehaald kunnen worden uit de database. Ook heeft deze service de functionaliteit om een verkiezing te maken, bewerken of verwijderen.

#### ElectableUserService

De electable user service zorgt ervoor dat de data van een verkiesbare kandidaat opgehaald kan worden. Ook heeft deze service de functionaliteit om een kandidaat te maken, bewerken of verwijderen.

#### VoterService

De electable user service zorgt ervoor dat de data van een stemmer opgehaald kan worden. Ook heeft deze service de functionaliteit om een stemmer te maken, bewerken of verwijderen.

#### TokenService

De token service zorgt ervoor dat de tokens van een gebruiker opgehaald kunnen worden uit de database. Ook bevat deze service de functionaliteit om een token aan te maken voor een gebruiker en de functionaliteit om een token te verwijderen.

#### EmailService

De email service bevat de functionaliteit om een mail te kunnen sturen naar gebruikers.

### Repositories

De repositories in het project zorgen voor de communicatie tussen de spring boot applicatie en de database.

Elke repository bevat de queries naar de database die nodig zijn om hun bijbehorende service goed te kunnen laten werken.

## Samenwerking geheel
In het onderstaande diagram valt te zien hoe de structuur van het programma in elkaar zit. Hierdoor kan je zien hoe alle componenten met elkaar samenwerken.

![Diagram communicatie tussen frondend en backend](images/TechnicalDesign/CommunicationDiagramFrontendBackend.png "image_tooltip")

# Test plan, code reviews en quality control
Om de regels betreft het testen, het nakijken van code en de kwaliteitsstandaarden te weten kan er gekeken worden naar het Code of Conduct bestand.


# Security design

Voor beveiliging hebben wij als groep nog niet genoeg over geleerd om een belangrijk systeem zoals verkiezingen aan te pakken, daarom gaan we onderzoek doen over de juiste beveiliging voor dit systeem. De prioriteit is dat in het uiteindelijke product de wet binnen Nederland wordt aangehouden.

Changelog

Versie 0.1, 02-12-22, initieel ontwerp
Versie 1.1, 18/12/2022, sprint 1, feedback van sprint 0
Versie 2.1 31/01/2022, sprint 2, feedback van sprint 1

