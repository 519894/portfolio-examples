# FreeBird, Client communication

Project Client on board, Saxion Hogeschool

Carthago ICT - Verkiezingssysteem

Sprint 1, Client communication

## De klant

Onze klant is Carthago ICT, gevestigd in Hengelo, John Maynard Keynesstraat 359. De contactpersoon van onze groep is Jordy Nieuwenhuis. Zijn contactgegevens zijn:

- email: [jordy.nieuwenhuis@carthago-ict.nl](mailto:jordy.nieuwenhuis@carthago-ict.nl)
- tel: 0541 627062

## Contactpersoon

De contactpersoon uit onze groep is Thom Kamphuis. Hij is degene die contact legt met Jordy via e-mail. Hij is ook de enige die dit doet, zodat de klant geen mails met dezelfde vragen krijgt van meerdere leden uit onze groep. Ook is het zo duidelijk voor Jordy met wie hij contact moet leggen als hij dat wil.

## Contactmomenten met de klant

- Vrijdag 25 november: Op vrijdagochtend 25 november zijn wij met de hele groep naar het pand geweest van Carthago. Hier hebben wij Jordy Nieuwenhuis ontmoet en onze eerste vragen gesteld. Tijdens de eerste meeting hadden wij ook meteen een vervolgafspraak ingepland voor de volgende week.
- Woensdag 30 november: Op deze dag kwam Jordy Nieuwenhuis naar ons toe op het Saxion. Wij hebben hem onze eerste wireframes laten zien, en ons functioneel ontwerp laten horen. Na de meeting hebben wij alles nog een keer naar hem toegestuurd. Ook hadden wij een aantal vragen gesteld. Daarnaast is er een vervolgafspraak gemaakt. Deze staat gepland op donderdag 15 december om 12 uur, bij Carthago ICT.
- Donderdag 15 december: Onze eerstvolgende afspraak om 12 uur.
- Woensdag 11 januari: Onze eerstvolgende afspraak is om half 12.
- Vrijdag 27 januari: Onze eerstvolgende afspraak is om half 10.

## notities

## Sprint 0

**Maurits Ten Dam**

Aantekeningen vragen

Hardware: ergens in de cloud, azure subscription, toegang krijgen als groep.

Helemaal losse applicatie, nergens op gebouwd

Eerst binnen dit bedrijf, daarna misschien buiten het bedrijf

Project is niet eerder uitgevoerd

Rechten: admins -\> kan alleen verkiezing starten en eindigen

Via uitnodiging kan iemand account aanmaken en stemmen -\> kan zichzelf verkiesbaar stellen voor bepaalde datum

Admin hoeft niet te weten of iemand verkiesbaar wordt gesteld

Alleen de admin, zou de uitslagen moeten kunnen zien en mededelen

Geen automatische resultaten mailtjes

Admins kunnen zichzelf aanwijzen als kiezer -\> beetje krom maar doen ze nu ook al

Resultaten worden binnen de app opgeslagen

Admin kan alleen regels veranderen voordat het stemmen is begonnen. Niet tijdens het stemmen veranderen

kiesgroepen: 1 iemand kiest namens de groep, alle waarden van elke groep zijn gelijk

individuen

geen stijl nog bedacht -\> als challenge een aanpasbaar thema

stem tijd is configureerbaar

tijd van tevoren ook configureerbaar

tijd over -\> template

prioriteit bij desktop app (chrome of safari)

darkmode moet een mogelijkheid zijn

3 maanden in dienst voor account aan mogen maken

Iedereen heeft 5 stemmen

Dubbelstemmen -\> komt hij op terug

Doel verkeizing -\> 5 leden eruit krijgen die winnen (algemeen)

Door configuratie gebruik voor meerdere dingen

Reminder via email is prima

Regels per verkiezing instelbaar

Configureerbaar via web browser zou mooi zijn, maar heeft geen prioriteit

Volgende week donderdag 12 uur aanwezig

12 januari 12 uur

26 januari 12 uur

Via teams ook mogelijk

**Jordy Grooten**

• 8 dagen na verkiezing mag data verwijderd worden

• Hardware: cloud (azure) gebruik op desktop/laptop

• App zit helemaal los van de andere systems in het bedrijf

• In eerste instantie gebruik binnen bedrijf

• Project is niet eerder gedaan

• Admin mag verkiezing starten/eindigen

• Kiezers krijgen uitnodiging om te kiezen

• Kiezers mogen zelf verkiesbaar stellen

• Admin krijgt uitslagen binnen

• Admin mag kiezer worden

• Uitslag opslaan in app

• Kiezer A mag meerdere keren stemmen op kiezer B (komen ze op terug)

• Kiesgroep: niet iedereen die stemt maar 1 representative

• Verkiezing is voor individuen

• \*optional\* thema is aanpasbaar

• Reminder is instelbaar (hoelang tot deadline wanneer reminder wordt gestuurd)

• Format reminder mag simpel

• Werkt op chrome en safari

• \*\*\*DARK MODE\*\*\*

• Regels voor verkiesbaar stellen wordt zelf geregeld (persoon mag pas kiezer worden na

3 maand daar werken).

• Iedereen 5 stemmen

• Uitslag sowieso 5 verkiesbare uitkomen

• Email reminder is genoeg (push hoeft niet)

• Regels te veranderen buiten de code (mogelijk, maar hoeft niet!)

**Simon Vermaat**

Carthago aantekeningen

De applicatie draaid in de cloud (azure).

De applicatie wordt binnen het bedrijf gebruikt.

Het project is niet eerder uitgevoerd.

Admins kunnen verkiezing starten en eindigen.

Applicatie is via uitnodiging. Dit wordt bepaald door de admin.

Verkiesbare kunnen zichzelf verkiesbaar stellen.

Resultaten wordt door de admin ingezien.

Admins kunnen zichzelf als kiezer en verkiesbare stellen.

Uitslagen worden binnen de app opgeslagen. De data wordt voor 8 dagen opgeslagen.

Als de verkiezingen niet zijn gestart mogen de regels aangepast worden.

Kiesgroep is dat je 1 iemand namens een groep laat stemmen.

Tijd om te stemmen en reminder is configureerbaar. Format voor reminder is simpel.

Houd in eerste instantie rekening met een desktop applicatie te maken. Daarna mobiel.

Er moet een dark mode zijn.

Werknemer moet minimaal 3 maanden in dienst zijn om verkiesbaar te stellen.

Iedereen heeft 5 stemmen.

Email is voldoende. Pushberichten zijn niet nodig.

Regels kunnen veranderd worden zonder code aan te passen. Dit is niet noodzakelijk.

## Sprint 1

**Jordy Grooten**

2nd meeting

• Functionaliteit. We zouden nog wat vragen sturen

• Mag op jezelf stemmen

• Kiezer a mag maar 1 keer stemmen op kiezer b

• Stemmen mag maar in 1 sessie

• Wachtwoord vergeten: bericht naar admin (kiezer moet in 1ste instantie zelf wachtwoord

aanmaken voor account)

• Verkiesbare moet verhaaltje bij zichzelf kunnen maken voor verkiezing

• Verkiesbare moet per verkiezing aangeven om verkiesbaar te stellen

• Focus op maar 1 verkiezing per keer (is in eerste instantie ook alleen voor OR)

• Verwijderen verkiezing het liefste handmatig

• Admin kan niet zien wie heeft gestemd op wie

• Na het verwijderen van de verkiezing kunnen alle accounts (non-admin) verwijderd

worden (elk verkiezing een schone lei)

• Tot nu toe geen intentie om uit te breiden met meerdere verkiezingen

Next meeting: donderdag 15 december

**Maurits Ten Dam**

Notities 30-11-2022 Project client on board

Wireframes

- Eerste instantie ziet t er strak uit

- Functionaliteit: Er zouden nog wat vragen gestuurd moeten worden

- Op jezelf stemmen moet kunnen, 1 keer stemmen maximaal

- Er moet in 1 keer gestemd worden, er mag niet gelinkt worden wie er op wie stemt

- Wireframe mist? Account inloggen

- Eerst berichtje naar admin bij wachtwoord vergeten. Zelfde mailtje als wachtwoord en account aanmaken dat van de admin komt

- Iets uitgebreider verhaaltje kunnen maken voor de verkiesbare (standpunten etc)

Op- of aanmerkingen

- Elke verkiezing

- Niet meerdere verkiezingen tegelijk

Requirements en user stories

functioneel ontwerp en beveiliging

- Als je kunt instellen wat maximaal stemmen is is prima

Verder nog bericht over het bestand

- Verkiezing moet kunnen worden verwijderd, ook als hij al beeindigd is

- Verwijderen verkiezing van na 8 dagen naar 3 maanden

- 15 december eerstvolgende afspraak, 12 uur aanwezig bij cathargo (halfuurtje de tijd)

- Volgende week mailtje over beveiliging en eventuele vragen

- Bij stemmen dus niet de naam van op wie er gestemd is er bij opslaan

- Accounts mogen ook na 3 maanden worden verwijderd

- Bij verkiezing verwijderen worden ook de users die aan de verkiezing gekoppeld verwijderd

- Mensen worden uitgenodigd voor een specifieke verkiezing

- Biografie is gekoppeld aan de verkiezing, niet aan de persoon

- Niet relevant om meerdere verkiezingen tegelijk te laten lopen

- Mailtjes beveiliging -\> saxion mailtjes

- Geen regelement dat er niet op externe linkjes in mailtjes mag worden geklikt

**Jordy Grooten**

notes meeting 3 (15-12-2022):

knop om handmatig een reminder te sturen zou handig zijn

kleuren bij verkiezingspagina veranderen (meer gevoel bij kleur)

datum notatie bij verkiezingspagina veranderen

planning sturen

verder tevreden met product tot nu toe

volgende afspraak: 11-01-2023 11.30 op school

**Maurits Ten Dam**

Uitleg systeem

Nog geen knop voor reminder -\> een knop is wel fijn om te hebben

Kleuren van de verkiezing pagina -\> gevoel bij kleur

Datum notatie niet goed

Planning sturen

Volgende afspraak: 11 januari in de les, hij komt langs op school

## Sprint 2

**Thom Kamphuis**

Notities 11-1-2023

Knoppen zijn niet gelijk in verschillende paginas.

Invoerformulieren komen niet overeen.

Profiel aanmaken textboxes horen niet naast elkaar, maar onder elkaar.

Plaatje van het internet is niet veilig.

Payload bevat gebruikersnaam en wachtwoord

Datepicker kan in de input, ipv het kleine icoontje

Het text veld om mensen uit te nodigen moet een lijst worden.

Meeste componenten staan te dicht op elkaar.

Log uit knop aanpassen.

Volgende meeting:

27-1 om 9:30.

**Erik Markvoort**

Na gebruiker aanmaken meteen ingelogd zijn

Confirmation popup voor aanpassen gebruikers

Consistency aan stijl, knoppen en invulvelden

Stijl aanpassing voor paginas om velden onder elkaar te hebben niet naast elkaar

Uploaden fotos, geen foto url, dit is een stijl en security issue.

Bigints in database

Svelte logs

Unhashed password in request body

Datepicker voor elections zodra je de dates velden aanklikt

In stemmers aanmaken voor elections, een lijst maken, niet een grote tekstblok

Padding en margin kan beter, voor een betere UX

Vrijdag half 10 in de centrale lobby van carthago ict is de volgende afspraak.

Bij levering graag de middelen en de kennis om het systeem aan de praat te krijgen.

## Sprint 3

**Thom kamphuis**

Notities 11-1-2023

Knoppen zijn niet gelijk in verschillende paginas.

Invoerformulieren komen niet overeen.

Profiel aanmaken textboxes horen niet naast elkaar, maar onder elkaar.

Plaatje van het internet is niet veilig.

Payload bevat gebruikersnaam en wachtwoord

Datepicker kan in de input, ipv het kleine icoontje

Het text veld om mensen uit te nodigen moet een lijst worden.

Meeste componenten staan te dicht op elkaar.

Log uit knop aanpassen.


Volgende meeting:
27-1 om 9:30.

**Erik Markvoort**

Na gebruiker aanmaken meteen ingelogd zijn

Confirmation popup voor aanpassen gebruikers

Consistency aan stijl, knoppen en invulvelden

Stijl aanpassing voor paginas om velden onder elkaar te hebben niet naast elkaar

Uploaden fotos, geen foto url, dit is een stijl en security issue.

Bigints in database

Svelte logs

Unhashed password in request body

Datepicker voor elections zodra je de dates velden aanklikt

In stemmers aanmaken voor elections, een lijst maken, niet een grote tekstblok

Padding en margin kan beter, voor een betere UX

Vrijdag half 10 in de centrale lobby van carthago ict is de volgende afspraak.

Bij levering graag de middelen en de kennis om het systeem aan de praat te krijgen.