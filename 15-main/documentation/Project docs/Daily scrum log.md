# FreeBird, Scrum Log

Project Client on board, Saxion Hogeschool

Carthago ICT - Verkiezingssysteem

Sprint retrospectives - 18/12/2022

## Inhoudsopgave

**[Inhoudsopgave](#_ucjke187mmhi) 1**

[Inleiding](#_s7ns88qdl4bn) 1

[Sprint 1 meeting 1](#_p9dq1nerngcf) 2

[Sprint 1 meeting 2](#_dppaf3wfeyks) 3

[Sprint 1 meeting 3](#_ge71s4css4h) 4

[Sprint 1 meeting 4](#_o7qahql4jqry) 5

[Sprint 2 meeting 1](#_ncrfuppi663s) 6

[Sprint 2 meeting 2](#_rqhpgr71g51i) 7

[Sprint 2 meeting 3](#_gju48vr1mez6) 8

[Sprint 2 meeting 4](#_hu16y6wvabsv) 9

## Inleiding

In dit document zijn alle standups genoteerd van elke sprint met de punten die gedaan zijn, wat er wordt gedaan en de eventuele problemen.

##


## Sprint 1 meeting 1

**Wat heb je gedaan?**

Maurits TD: Nog niet bezig geweest

Maurits Hee: Wireframes maken en maken code of conduct

Simon: Footer gemaakt

Thom: Nog niet bezig geweest

Fabian: Merge requests nagekeken

Erik: Merge requests nagekeken

Jordy: Nog niet bezig geweest

**Wat ga je doen?**

Maurits TD: Bezig met de AVG documentatie en backend documentatie

Maurits Hee: Stel verkiesbaar scherm maken

Simon: Header maken

Thom: Log in scherm maken

Fabian: Bezig met documentatie van de endpoints in de database

Erik: Verkiezing scherm maken en kijken naar de regelgeving over beveiliging

Jordy: Spring boot opzetten

**Zijn er nog problemen?**

Iedereen heeft deze vraag beantwoord met nee.

## Sprint 1 meeting 2

**Wat heb je gedaan?**

Maurits TD: Documentatie AVG en documentatie database gemaakt

Maurits Hee: Stel verkiesbaar scherm gemaakt en merge requests nagekeken

Simon: Header die responsive is gemaakt en ik heb feedback toegepast op de footer.

Thom: Log in scherm gemaakt

Fabian: Documentatie endpoints gemaakt

Erik: Verkiezingen scherm gemaakt en gekeken naar beveiliging

Jordy: Springboot project opgezet

**Wat ga je doen?**

Maurits TD: Documentatie omzetten naar markdown en een connectie opzetten naar de database

Maurits Hee: Stemscherm verkiezing en resultaten scherm verkiezing maken

Simon: Verder bezig met verbeteren headers en een pop up scherm maken

Thom: Profiel aanmaak en bewerk scherm maken

Fabian: Gebruiker beheer scherm en election aanmaak scherm maken

Erik: Gebruiker beheer scherm en election aanmaak scherm maken

Jordy: Nieuwe gebruiker pagina en bewerk gebruiker pagina maken

**Zijn er nog problemen?**

Iedereen heeft deze vraag beantwoord met nee.

## Sprint 1 meeting 3

**Wat heb je gedaan?**

Maurits TD: Documentatie omgezet naar markdown en de database connectie gemaakt

Maurits Hee: Stemscherm verkiezing, resultaten scherm verkiezing en kandidaat informatie scherm afgemaakt

Simon: Pop up scherm gemaakt, footer bug gefixed en header verbeterd

Thom: Profiel aanmaak en bewerk scherm gemaakt

Fabian: Gebruiker beheer scherm en election aanmaak scherm gemaakt

Erik: Bewerken verkiezing scherm en aanmaken verkiezing scherm gemaakt

Jordy: Nieuwe gebruiker pagina en bewerk gebruiker pagina gemaakt

**Wat ga je doen?**

Maurits TD: How to git omzetten naar markdown, model database implementeren en voorbereiden op de meeting met de klant

Maurits Hee: Navigatie toevoegen tussen schermen en voorbereiden op de meeting met de klant

Simon: Voorbereiden op de meeting met de klant

Thom: Voorbereiden op de meeting met de klant en componenten maken voor het profiel aanmaak en bewerk scherm

Fabian: Voorbereiden op de meeting met de klant

Erik: Verbeteren van de hoofdpagina en voorbereiden op de meeting met de klant

Jordy: Voorbereiden op de meeting met de klant

**Zijn er nog problemen?**

Iedereen heeft deze vraag beantwoord met nee.

##


## Sprint 1 meeting 4

**Wat heb je gedaan?**

Maurits TD: Database model geïmplementeerd en meeting met de klant gehad

Maurits Hee: Navigatie tussen schermen toegevoegd en meeting met de klant gehad

Simon: Meeting met de klant gehad

Thom: Componenten gemaakt voor het profiel aanmaak scherm en meeting met de klant gehad

Fabian: Meeting met de klant gehad

Erik: Hoofdpagina verbeterd en meeting met de klant gehad

Jordy: Meeting met de klant gehad

**Wat ga je doen?**

Iedereen heeft geantwoord met: Sprint planning en sprint retrospective maken.

**Zijn er nog problemen?**

Iedereen heeft deze vraag beantwoord met nee.

## Sprint 2 meeting 1

**Wat heb je gedaan?**

Maurits TD: Documentatie bijgewerkt, database repositories gemaakt en geholpen met databases opzetten

Maurits Hee: Sprint retrospectives gemaakt, database controllers gemaakt en documentatie bijgewerkt

Simon: Documentatie bijgewerkt

Thom: Wireframes aangepast en documentatie bijgewerkt

Fabian: Documentatie bijgewerkt

Erik: Documentatie bijgewerkt en ingeleverd

Jordy: Backend services gemaakt en documentatie bijgewerkt

**Wat ga je doen?**

Maurits TD: Verder helpen met databases opzetten en database fixen

Maurits Hee: Databases opzetten en dummy data maken voor de database

Simon: Issue uitzoeken

Thom: Taak uitzoeken

Fabian: Issue uitzoeken

Erik: Database opzetten en get maken voor de elections

Jordy: Issue uitzoeken

**Zijn er nog problemen?**

bij Maurits H en Simon

## Sprint 2 meeting 2

**Wat heb je gedaan?**

Maurits TD: Bezig geweest met database opzetten en bugfixen

Maurits Hee: Dummy data gemaakt voor de database en database structuur verbeterd

Simon: Get request gemaakt voor het ophalen van alle gebruikers en verkiesbare gebruikers

Thom: Bezig geweest met het aanmaken van een verkiesbare gebruiker

Fabian: Mailservice gemaakt voor het sturen van invites en reminders

Erik: Get request gemaakt voor de verkiezingen

Jordy: Bezig geweest met het inloggen maken

**Wat ga je doen?**

Maurits TD: De fouten in de database oplossen

Maurits Hee: De dummydata aanpassen en een nieuwe issue uitzoeken

Simon: Verder bezig met het ophalen van verkiesbare gebruikers en feedback van een merge request toepassen

Thom: Bezig gaan met het oplossen van errors tijdens het aanmaken van een verkiesbare gebruiker

Fabian: Een nieuwe issue uitzoeken

Erik: Een nieuwe issue uitzoeken

Jordy: Verder bezig met het maken van het inlogsysteem

**Zijn er nog problemen?**

Bij Simon en Thom

## Sprint 2 meeting 3

**Wat heb je gedaan?**

Hele groep: alle overgebleven issues afmaken

**Wat ga je doen?**

Hele groep: Voorbereiden op meeting met de klant en nieuwe backlog maken

**Zijn er nog problemen?**

geen problemen in de groep

## Sprint 2 meeting 4

**Wat heb je gedaan?**

Hele groep: Client meeting gehad

**Wat ga je doen?**

Hele groep: documentatie aanpassen op basis van de feedback die is gegeven in sprint 1.

**Zijn er nog problemen?**

geen problemen in de groep

## Sprint 3 meeting 1

**Wat heb je gedaan?**

Maurits TD: Margins en padding fixen, Documentatie aanpassen en individuele reflectie gemaakt

Maurits Hee: Automatische login gefixed, documentatie aangepast en individuele reflectie gemaakt

Simon: Missende popups toevoegen

Thom: Invulvenden dezelfde stijl maken, loginpagina email label fixen

Fabian: Constraints toevoegen aan database

Erik: Invulvelden onder elkaar zetten

Jordy: Code comments toevoegen

**Wat ga je doen?**

Maurits TD: Issue uitzoeken

Maurits Hee: Ongebruikte json informatie weghalen uit returns van de backend

Simon: Popups afmaken en nieuwe issue uitzoeken

Thom: Invulvelden zelfde stijl maken en een nieuwe issue uitzoeken

Fabian: Nieuwe issue uitzoeken

Erik: Nieuwe issue uitzoeken

Jordy: Nieuwe issue uitzoeken

**Zijn er nog problemen?**

Simon

## Sprint 3 meeting 2

**Wat heb je gedaan?**

Maurits TD: Margins en padding fixen, Documentatie aanpassen en individuele reflectie gemaakt

Maurits Hee: Ongebruikte json informatie weggehaald en onderzoek gedaan naar missende image in build

Simon: Missende popups toevoegen

Thom: Invulvenden dezelfde stijl maken, loginpagina email label fixen

Fabian: Constraints toevoegen aan database

Erik: Invulvelden onder elkaar zetten

Jordy: Code comments toevoegen

**Wat ga je doen?**

Maurits TD: Nieuwe issue uitzoeken

Maurits Hee: Missende images in build oplossen en een bug bij het bewerken van een verkiezing oplossen

Simon: Popups afmaken en nieuwe issue uitzoeken

Thom: Invulvelden zelfde stijl maken en een nieuwe issue uitzoeken

Fabian: Nieuwe issue uitzoeken

Erik: Nieuwe issue uitzoeken

Jordy: Nieuwe issue uitzoeken

**Zijn er nog problemen?**

Simon

## Sprint 3 meeting 3

**Wat heb je gedaan?**

Maurits TD: Tests toegevoegd, backend autorization geupdate

Maurits Hee: Missende images gefixed en bug opgelost waar een verkiezing updaten fout ging

Simon: Https proberen toe te passen, encryptie proberen toe te passen, ppk toepassen

Thom: Uploaden foto's afgemaakt in de backend

Fabian: Backend data validatie toegevoegd

Erik: Website handleiding gemaakt, engelstalige tekst omgezet naar nederlands, een bug opgelost in de dropdown menu

Jordy: Foutafhandeling checken frontend

**Wat ga je doen?**

Maurits TD: Kijken naar een nieuwe taak

Maurits Hee: Anderen helpen met bugfixes en meeting voorbereiden met klant

Simon: Verder werken aan de ppk

Thom: Fotos inladen in de frontend

Fabian: Kijken naar een nieuwe taak

Erik: Kijken naar een nieuwe taak

Jordy: Kijken naar een nieuwe taak

**Zijn er nog problemen?**

Simon