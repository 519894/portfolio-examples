# **Requirements**


## **Business requirements**


<table>
  <tr>
   <td><strong>naam</strong>
   </td>
   <td><strong>beschrijving</strong>
   </td>
   <td><strong>F/NF</strong>
   </td>
   <td><strong>priority</strong>
   </td>
   <td><strong>bron</strong>
   </td>
  </tr>
  <tr>
   <td>BR-001
   </td>
   <td>De applicatie voorziet het bedrijf van een verkiezingssysteem voor binnen het bedrijf.
   </td>
   <td>NF
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>BR-002
   </td>
   <td>Werknemers die 3 maanden binnen het bedrijf werken kunnen gebruik maken van de applicatie
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>BR-003
   </td>
   <td>Het systeem moet op een Azure Cloud kunnen draaien
   </td>
   <td>NF
   </td>
   <td>SHOULD
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>BR-004
   </td>
   <td>De applicatie is door het team zelf ontwikkeld.
   </td>
   <td>NF
   </td>
   <td>MUST
   </td>
   <td>Notities
<p>

   </td>
  </tr>
  <tr>
   <td>BR-005
   </td>
   <td>De applicatie wordt niet op een andere voorgebouwde applicatie gebouwd
   </td>
   <td>NF
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
</table>





## **User requirements**


<table>
  <tr>
   <td><strong>naam</strong>
   </td>
   <td><strong>beschrijving</strong>
   </td>
   <td><strong>F/NF</strong>
   </td>
   <td><strong>priority</strong>
   </td>
   <td><strong>bron</strong>
   </td>
  </tr>
  <tr>
   <td>UR-001
   </td>
   <td>Als Gebruiker wil ik kunnen inloggen
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>UR-002
   </td>
   <td>Als kiezer wil ik mij kunnen inschrijven als verkiesbaar persoon tijdens de inschrijfperiode.
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
<p>
<strong> </strong>
   </td>
  </tr>
  <tr>
   <td>UR-003
   </td>
   <td>Als verkiesbare wil ik mij kunnen uitschrijven uit de kieslijst
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>UR-004
<p>
<strong> </strong>
   </td>
   <td>Als gebruiker wil ik een mail krijgen om mee te kunnen doen aan de verkiezing
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
<p>
<strong> </strong>
   </td>
  </tr>
  <tr>
   <td>UR-005
<p>

   </td>
   <td>Als kiezer wil ik kunnen stemmen op een verkiesbare tijdens de stemperiode
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
<p>

   </td>
  </tr>
  <tr>
   <td>UR-006
<p>

   </td>
   <td>Als admin wil ik mensen kunnen uitnodigen voor een verkiezing
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>UR-007
<p>

   </td>
   <td>Als admin wil ik verkiezingen kunnen aanmaken
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>UR-008
<p>

   </td>
   <td>Als admin wil ik de regels van een verkiezing kunnen aanpassen buiten de code.
   </td>
   <td>F
   </td>
   <td>COULD
   </td>
   <td>Notities
<p>

   </td>
  </tr>
  <tr>
   <td>UR-009
<p>

   </td>
   <td>Als admin wil ik de standaard verkiezingsregels kunnen configureren
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
<p>

   </td>
  </tr>
  <tr>
   <td>UR-010
<p>

   </td>
   <td>Als admin wil alleen ik de resultaten van de verkiezing in kunnen zien
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>UR-011
   </td>
   <td>Als KiesGroep wil ik kunnen stemmen op een verkiesbare
   </td>
   <td>F
   </td>
   <td>COULD
   </td>
   <td>Notities
   </td>
  </tr>
</table>





## **Systeem requirements**


<table>
  <tr>
   <td><strong>naam</strong>
   </td>
   <td><strong>beschrijving</strong>
   </td>
   <td><strong>F/NF</strong>
   </td>
   <td><strong>priority</strong>
   </td>
   <td><strong>bron</strong>
   </td>
  </tr>
  <tr>
   <td>SR-001
   </td>
   <td>De regels kunnen niet meer aangepast worden wanneer de stemming is begonnen
   </td>
   <td>NF
   </td>
   <td>MUST
   </td>
   <td>Notities
<p>

   </td>
  </tr>
  <tr>
   <td>SR-002
<p>

   </td>
   <td>Acht dagen na de verkiezing moet de verkiezingsdata verwijderd zijn.
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
<p>

   </td>
  </tr>
  <tr>
   <td>SR-003
<p>

   </td>
   <td>De applicatie moet werken op Chrome en Safari.
   </td>
   <td>NF
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>SR-004
<p>

   </td>
   <td>Resultaten worden binnen de applicatie opgeslagen
   </td>
   <td>NF
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>SR-005
   </td>
   <td>De frontend is gemaakt in darkmode
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>SR-006
   </td>
   <td>De frontend bevat een mogelijkheid om light mode aan te zetten
   </td>
   <td>F
   </td>
   <td>COULD
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>SR-007
   </td>
   <td>De email reminder voor stemmen is instelbaar
   </td>
   <td>F
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>SR-008
   </td>
   <td>De applicatie maakt gebruik van een database om informatie op te slaan
   </td>
   <td>NF
   </td>
   <td>MUST
   </td>
   <td>Notities
   </td>
  </tr>
  <tr>
   <td>SR-009
   </td>
   <td>De applicatie kan beide op desktop en mobiel browsers draaien.
   </td>
   <td>NF
   </td>
   <td>COULD
   </td>
   <td>Notities
   </td>
  </tr>
</table>

