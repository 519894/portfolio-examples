# Freebird verkiezingsomgeving handleiding.

Project client on board, Saxion Hogeschool

Carthago ICT – verkiezingssysteem

handleiding

## Inhoud

##

[Freebird verkiezingsomgeving handleiding.](#_heading=h.u367qsnty38h)

[Inhoud](#_heading=h.ubipxu4zpkp2)

[Gebruiker functionaliteiten](#_heading=h.bxfrrayf5y3k)

[Aanmelden als nieuwe gebruiker](#_heading=h.ss7pbxar2zb1)

[Aanmelden als bestaande gebruiker](#_heading=h.lz77rsaf1xeu)

[Een verkiezing bekijken](#_heading=h.v3ac2dq21ye4)

[Verkiesbaar stellen in een verkiezing](#_heading=h.31v3u4hmskp6)

[Verkiezingsprofiel aanpassen](#_heading=h.xt41s2zpbob)

[Terug trekken als verkiesbare](#_heading=h.fz647omxv3j8)

[Stemmen in een verkiezing](#_heading=h.6m6m3rpcaj5o)

[Log uit](#_heading=h.489qfwp1736t)

[Admin functionaliteiten](#_heading=h.cs5fxnbt61c0)

[Verkiezing bewerken](#_heading=h.8uf0024sft31)

[Verkiezing aanmaken](#_heading=h.3mjyec10jyi)

[Verkiezing verwijderen](#_heading=h.za4zwx5sn2mq)

[Reminder sturen](#Reminder_sturen)

[Uitslag verkiezing bekijken](#_heading=h.45npv5bkf5st)

[Gebruiker verwijderen](#_heading=h.qzkoyxrmv9qr)

[Gebruiker bewerken](#_heading=h.ewdtje1m8jd6)

##

## standaard log in gegevens
De applicatie komt standaard met een admin account om de applicatie te kunnen testen. De email van dit account is: admin@gmail.com en het wachtwoord is: admin.

Voor standaard gebruikers kunnen de volgende gegevens gebruikt worden:

Email: test@gmail.com 

Email:password@gmail.com

Email: user@gmail.com

Email: token@gmail.com

De wachtwoorden voor deze accounts zijn hetzelfde als de eerste delen van de email. Het wachtwoord van test@gmail is dus test.

## Gebruiker functionaliteiten

### Aanmelden als nieuwe gebruiker

Wanneer een administrator van het verkiezingssysteem je heeft uitgenodigd voor een verkiezing, wordt vanuit het verkiezingssysteem een email naar je verzonden met de volgende tekst:

Maak een account aan via de volgende link: [link]


Bij het aanklikken van de link die verstuurd wordt, word je gebracht naar de verkiezingswebsite waar je je kan aanmelden door een wachtwoord tweemaal in te vullen in de volgende pagina.

![alt_text](images/WebsiteGuide/newUser.png "image")

Na het tweemaal invullen van een identiek wachtwoord, klik op bevestig, daarna word je naar de hoofdpagina gebracht.

### Aanmelden als bestaande gebruiker

Als je je hebt aangemeld als nieuwe gebruiker, kan je ook opnieuw inloggen, gebruik makend van het email adres wat je gebruikt en je opgegeven wachtwoord in de volgende pagina.

![alt_text](images/WebsiteGuide/login.png "image")

Na het invullen van je e-mailadres en wachtwoord, klik op login. Als de informatie die je hebt opgegeven correct is, word je gebracht naar de hoofdpagina, waar je verkiezingen kan bekijken.

### Een verkiezing bekijken

Wanneer je ingelogd bent, word je als eerste gebracht naar de hoofdpagina van het verkiezingssysteem, waar je alle verkiezingen kan zien waar je bent ingeschreven als stemmer.

![alt_text](images/WebsiteGuide/electionsUser.png "image")

Vanuit de hoofdpagina kan je bij een verkiezing op 'bekijk' klikken, wat je naar de pagina van die specifieke verkiezing brengt. Deze pagina's kunnen verschillen op basis van of de verkiezing aanstaande is of begonnen.

###

### Verkiesbaar stellen in een verkiezing

Wanneer je een kiezer bent in een verkiezing, en de verkiezing kan zien in je hoofd pagina, is het mogelijk om jezelf verkiesbaar te stellen. Als je jezelf verkiesbaar wil stellen klik je bij een verkiezing op 'stel verkiesbaar', wat je naar de 'profiel aanmaken' scherm brengt. Hier zie je de optie om je verkiezing profiel samen te stellen door je profielfoto, naam, beschrijving, slogan en standpunten in te vullen.

![alt_text](images/WebsiteGuide/editProfile1.png "image")

Na het invullen van alle velden, en mogelijk een profielfoto, klik onderaan in de pagina op de knop 'bevestig' om je profiel aan te maken en verkiesbaar te worden.

![alt_text](images/WebsiteGuide/editProfile2.png "image")

### Verkiezingsprofiel aanpassen

Wanneer je al verkiesbaar bent in een verkiezing, zie je als je die verkiezing bekijkt de optie om je terug te trekken en je verkiezing profiel aan te passen. Wanneer je kiest om je profiel aan te passen, ga je naar een soortgelijke pagina als profiel aanmaken, met je informatie al ingevuld. Pas hier aan wat je wilt aanpassen en klik op 'bevestig' om je verkiezing profiel aan te passen.

### Terug trekken als verkiesbare

Wanneer je de pagina bekijkt van een aanstaande verkiezing waar je verkiesbaar bent, is er naast de optie om je profiel te bewerken, de optie om je terug te trekken. Om je terug te trekken, klik in de volgende pagina op 'Trek terug'. Deze optie is ook zichtbaar vanaf de hoofdpagina.

![alt_text](images/WebsiteGuide/upcomingElection.png "image")

Voordat je je terugtrekt uit een verkiezing, wordt nog een keer bevestigd of je het zeker weet door middel van de volgende pop-up.

![alt_text](images/WebsiteGuide/popupRetreat.png "image")

### Stemmen in een verkiezing

Wanneer je de pagina opent van een verkiezing die gestart is, kan je op de pagina ook stemmen, zoals te zien op de volgende pagina, zijn alle verkiesbare kandidaten zichtbaar, om de beschrijving van de kandidaten te zien kan je ook op de kandidaat foto klikken.

![alt_text](images/WebsiteGuide/vote1.png "image")
Als gebruiker kan je op kandidaten stemmen door het vakje onder hun naam aan te vinken. Zolang je nog stemmen hebt kan je vakjes blijven aanvinken. Vervolgens kan de gebruiker op de bevestig knop klikken om de stemmen door te geven.


![alt_text](images/WebsiteGuide/vote2.png "image")

Wanneer je op bevestig klikt, krijg je een pop-up om te bevestigen of je zeker bent van je stem(men). ![](RackMultipart20230126-1-jnttnz_html_635a8a0377099eb2.png)

![alt_text](images/WebsiteGuide/popupVote.png "image")

### Log uit

Om uit te loggen, klik simpelweg op 'logout' in de balk of het uitklapmenu boven in het scherm.

![alt_text](images/WebsiteGuide/headerUser.png "image")

## Admin functionaliteiten

Als ingelogde admin, heb je meer functionaliteiten beschikbaar, wat ook zichtbaar is vanaf de hoofdpagina zoals hieronder is te zien. Een admin kan de regels, startdatum en einddatum van een verkiezing aanpassen in 'instellingen', een verkiezing starten of verwijderen, gebruikers beheren en de uitslagen van verkiezingen zien.

![alt_text](images/WebsiteGuide/electionsAdmin.png "image")

### Verkiezing bewerken

Als admin kan je vanaf de hoofdpagina bij een van de aanstaande verkiezingen op 'instellingen' klikken, wat je brengt naar het verkiezing bewerken scherm zoals hieronder is te zien. Op dit scherm is de bestaande informatie van de verkiezing al ingevuld, en kan je deze aanpassen, onder deze informatie is te vinden de startdatum, einddatum, naam, stemmen per persoon, de mogelijkheid of je op jezelf mag stemmen, en welke kiezers aan de verkiezing meedoen.

![alt_text](images/WebsiteGuide/editElection1.png "image")

Om aan te passen wie er aan een verkiezing mee mag doen, kan je onderaan de pagina gebruikers uit de lijst verwijderen door de 'verwijder' knop aan te klikken naast de gebruiker die je wilt verwijderen van de verkiezing. Om nieuwe gebruikers toe te voegen, typ een email adres in de invulbalk onder de lijst en klik op voeg toe.

![alt_text](images/WebsiteGuide/editElection2.png "image")

Wanneer je klaar bent met de verkiezing aanpassen, klik op bevestig en de verkiezing wordt aangepast. Mocht je nieuwe gebruikers toegevoegd hebben die niet onderdeel zijn van het verkiezingssysteem, wordt aan deze persoon een mail verzonden met een uitnodiging.

### Verkiezing aanmaken

Wanneer in de balk boven in het scherm, of in het uitklapmenu, op 'nieuwe verkiezing' wordt geklikt, word je gebracht naar een soortgelijke pagina waar je in plaats van een verkiezing aan past, een nieuwe verkiezing aanmaakt, het verschil is dat je niet al informatie ingevuld zal vinden en er nog geen gebruikers zullen zijn die deel uitmaken van de verkiezing. Ook hier zullen nieuwe gebruikers buiten het systeem een uitnodigingsmail krijgen.

### Verkiezing verwijderen

Een admin kan op de hoofdpagina de optie om een verkiezing te verwijderen zien bij verkiezingen die nog niet zijn gestart en verkiezingen die zijn verlopen. Om een verkiezing te verwijderen, klik op de knop 'verwijder', waarna je een pop-up krijgt om te bevestigen dat je zeker bent dat je de verkiezing wilt verwijderen.

![alt_text](images/WebsiteGuide/popupDeleteElection.png "image")

### Reminder sturen

Een admin kan op de pagina van een begonnen verkiezing een reminder sturen naar alle stemmers die nog niet gestemd hebben. Deze gebruikers zullen een mail ontvangen met een reminder om te stemmen.

### Uitslag verkiezing bekijken

Om de uitslag van een verkiezing te zien, moet je simpelweg klikken op 'bekijk' bij een afgelopen verkiezing, dit brengt je naar de volgende pagina. Hierop is te zien hoeveel stemmen iedere kandidaat heeft, en wie de winnaar is.

![alt_text](images/WebsiteGuide/endedElection.png "image")

### Gebruiker verwijderen

Om een gebruiker te verwijderen, begin door naar de 'gebruikers beheren' te gaan, te vinden in de balk boven in het scherm of het uitklapmenu. Daarna bevind je je op het volgende scherm. Hier kan een admin een gebruiker verwijderen door te klikken op 'verwijder' bij de gebruiker die je wilt verwijderen.

### ![alt_text](images/WebsiteGuide/manageUsers.png "image")

Wanneer je op 'verwijder' klikt, krijg je een pop-up om te bevestigen dat je zeker weet dat je de gebruiker wilt verwijderen.

![alt_text](images/WebsiteGuide/popupDeleteUser.png "image")

### Gebruiker bewerken

Wanneer je in gebruikers beheren klikt op 'bewerken', kom je terecht in het volgende scherm. Hier kun je het email adres van een gebruiker bewerken. Vul hier het oude e-mailadres in ter bevestiging, en daarna in het volgende veld het nieuwe email adres. Klik daarna op '' om de gebruiker te bewerken.

![alt_text](images/WebsiteGuide/editUser.png "image")

Nadat je op 'bevestig' klikt, krijg je een pop-up om te bevestigen dat je zeker weet dat je de gebruiker wilt verwijderen.

![alt_text](images/WebsiteGuide/popupEditUser.png "image")