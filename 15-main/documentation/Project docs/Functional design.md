
# FreeBird, Functional design

Project Client on board, Saxion Hogeschool

Carthago ICT - Verkiezingssysteem

Sprint 1, Functioneel Ontwerp


# Inhoud


[TOC]



# Introductie

Voor het vak Client on board werken studenten aan een opdracht waar een bedrijf als klant werkt, Carthago ICT geeft de opdracht en beoordeelt of het resultaat naar verwachting is.

De opdracht van Carthago ICT luidt als volgt:

Maken van verkiezingssoftware (OR). Ze zijn bezig om de OR op te zetten binnen Carthago. Dit willen doen door middel van een digitaal systeem. De eerste iteratie wordt nog uitbesteed aan een externe partij. Aangezien deze verkiezingen elke 3 jaar worden gedaan is er voor de volgende keer behoefte aan een eigen applicatie.

over de span van 8 weken, verdeeld in 4 sprints, wordt de opdracht ontworpen, geïmplementeerd en mogelijk uitgebreid door de projectgroep.


## Visie product

Het doel van het product is om een veilige verkiezingssoftware te maken. Bij een verkiezing is het uitermate belangrijk dat er geen misbruik gemaakt kan worden door mensen en dat de data veilig wordt opgeslagen en behandeld. Daarom speelt bij het ontwerp van deze verkiezingssoftware beveiliging een grote rol. Dit is belangrijk voor onze visie, want als het bedrijf groeit, zullen de hoeveelheid gebruikers ook groeien. Als het programma goed werkt met 10 gebruikers, is de kans dat het met 100 of 200 gebruikers goed werkt vrij groot. Het is dus belangrijk om op kleine schaal een goed werkend programma te hebben zodat er later nog eventuele uitbreidingen gedaan kunnen worden.


## Stakeholders


#### HBO-ICT project groep.

De projectgroep werkt gezamenlijk aan de opdracht, zij maken het ontwerp en implementeren het, aan de hand van de hulp van de docent en de feedback van de klant krijgt de groep de juiste informatie om het project te vervullen.


#### Docent: Eelco Jannink

De docent voor het vak begeleid het project, licht de opdracht en deliverables toe en geeft feedback op werk, de docent heeft er belang bij dat het geleverde product en het werkproces goed en representatief zijn voor saxion studenten.


#### Contactpersoon/Klant: Jordy Nieuwenhuis, Carthago ICT

Jordy overziet voor de klant het project, dit houdt in dat hij het contact houdt met de projectgroep en communiceert wat de klant verwacht en nodig heeft. De verantwoordelijkheid van dit klantencontact ligt bij de projectgroep. Jordy heeft er baat bij dat het eindproduct goed genoeg werkt voor Carthago ICT om ook gebruik te maken hiervan.


## Planning

#### Startup

In de eerste twee weken zetten wij de opdracht op. We spreken af met de klant, we maken een functioneel en een technisch ontwerp en we maken een scrumboard. Ook maken we een individueel learning plan.

#### Sprint 1

Aan het einde van sprint 1 hebben we de basis functionaliteit van het systeem. Hiermee kan de visie van de klant worden bevestigd en kan de haalbaarheid worden bewezen.

#### Sprint 2

##### Waarom deze sprint?

Tijdens afgelopen sprint hebben we voornamelijk het front-end van het product gemaakt. Hierbij hebben we de webpagina's gemaakt en een begin gemaakt aan de backend door een Spring Boot Applicatie te maken en een database te verbinden daarmee.

##### Wat gaan we doen in deze sprint?

Tijdens deze sprint gaan we streven om de backend af te krijgen en deze te verbinden met het front-end. Daarnaast is het ons doel om aan het einde van deze sprint een compleet product te leveren zodat we tijdens de laatste sprint alleen het product hoeven te opschonen en de code netter maken waar nodig.

##### Hoe gaan we dit voltooien?

We spreken af tijdens onze lessen. Ook spreken we af en toe af op maandag. Tijdens deze afgesproken momenten stellen we per persoon een doel vast om een bepaalde issue af te krijgen. Dan gaan we bij de volgende meeting tijdens een stand-up meeting bespreken of iedereen zijn opgenomen issue heeft kunnen voltooien. Elke keer als iemand gaat werken aan een issue, maakt de persoon een feature branch aan vanaf de Development branch. De naamgeving van de feature branch staat aangegeven in de code of conduct. Wanneer iemand zijn issue af heeft, dan vraagt de persoon een merge-request aan en merged de feature branch terug in de Development branch. Het volledige stappenplan hiervoor staat in het document How to Git.

#### Sprint 3

Tijdens sprint 3 gaan wij de laatste kleine beetjes aanpassen en oplossen. We leveren aan het einde een volledig compleet werkend product in, waar de klant blij mee is.

## Backlog

hieronder staat de backlog voor sprint 1. Hier zal de komende 2 weken aan gewerkt worden. Zoals u kunt zien wordt er in de eerste sprint veel gewerkt aan de Front-End van het project. Hier is voor gekozen zodat er aan het eind van sprint 1 een werkend resultaat kan worden laten zien aan de klant.

Er zal nog niet zo veel worden gewerkt aan de Backend van het project. Deze functionaliteit zal in de tweede sprint worden toegevoegd. Uiteraard is het niet de bedoeling dat er niks wordt gedaan aan de backend. Er zal een design worden opgesteld en er zal een project worden aangemaakt zodat er in de tweede sprint meteen aan verder gewerkt kan worden.

![alt_text](images/FunctionalDesign/image13.png "image_tooltip")

![alt_text](images/FunctionalDesign/image3.png "image_tooltip")


Hierboven zijn een aantal issues uit onze backlog te zien. In het linker diagram ziet u vooral de taken die moeten worden uitgevoerd in de backend. Het zijn vooral taken die te maken hebben met het aanmaken van een database en een design bedenken en documenteren.

In de rechter diagram ziet u een aantal taken die moeten worden uitgevoerd in de frontend. De taken zijn onderverdeeld in schermen. Elk scherm is een aparte taak. Als er onderdelen zijn die bij veel schermen van toepassing zijn, dan wordt daar ook een aparte taak van gemaakt. Een voorbeeld hiervan is een pop-up scherm.

![alt_text](images/FunctionalDesign/image2.png "image_tooltip")

![alt_text](images/FunctionalDesign/image8.png "image_tooltip")

In de twee bovenstaande diagrammen ziet u taken die te maken hebben met de front-end. In deze diagrammen staan vooral schermen die gemaakt moeten worden. Ook zijn er een aantal componenten die gemaakt moeten worden.

Als al deze taken zijn uitgevoerd zou er een werkend front-end systeem moeten zijn. Dit systeem zal aan het einde van de eerste sprint getoond worden aan de klant.

voor sprint 2 worden de volgende gelijste taken uitgevoerd. De samenhang tussen deze issues zijn dat voor iedere interactie met de back-end ook een implementatie in de front-end worden gemaakt, deze zijn verdeelt in aparte issues die individueel kunnen worden uitgevoerd maar zullen van elkaar afhangen.

![alt_text](images/FunctionalDesign/image20.png "image_tooltip")

## Requirements

Tijdens sprint 1 heeft de groep ontmoet met Carthago ICT, vooraf heeft de groep de casus gegeven door het bedrijf bekeken en aan de hand daarvan vragen gesteld om te verduidelijken wat de verwachtingen zijn van het bedrijf. Aan de hand hiervan, en de volgende meetings met de klant kunnen er ook user stories worden gemaakt.

## Business requirements

| **naam** | **beschrijving** | **F/NF** | **priority** | **bron** |
| --- | --- | --- | --- | --- |
| BR-001 | De applicatie voorziet het bedrijf van een verkiezingssysteem voor binnen het bedrijf. | NF | MUST | Notities |
| BR-002 | Werknemers die 3 maanden binnen het bedrijf werken kunnen gebruik maken van de applicatie | F | MUST | Notities |
| BR-003 | Het systeem moet op een Azure Cloud kunnen draaien | NF | SHOULD | Notities |
| BR-004 | De applicatie is door het team zelf ontwikkeld. | NF | MUST | Notities |
| BR-005 | De applicatie wordt niet op een andere voorgebouwde applicatie gebouwd | NF | MUST | Notities |

## User requirements

| **naam** | **beschrijving** | **F/NF** | **priority** | **bron** |
| --- | --- | --- | --- | --- |
| UR-001 | Als gebruiker wil ik kunnen inloggen | F | MUST | Notities |
| UR-002 | Als kiezer wil ik mij kunnen inschrijven als verkiesbaar persoon tijdens de inschrijfperiode. | F | MUST | Notities |
| UR-003 | Als verkiesbare wil ik mij kunnen uitschrijven uit de kieslijst | F | MUST | Notities |
| UR-004 | Als gebruiker wil ik een mail krijgen om mee te kunnen doen aan de verkiezing | F | MUST | Notities |
| UR-005 | Als kiezer wil ik kunnen stemmen op een of meer verkiesbare tijdens de stemperiode | F | MUST | Notities |
| UR-006 | Als admin wil ik mensen kunnen uitnodigen voor een verkiezing | F | MUST | Notities |
| UR-007 | Als admin wil ik verkiezingen kunnen aanmaken | F | MUST | Notities |
| UR-008 | Als admin wil ik de regels van een verkiezing kunnen aanpassen buiten de code. | F | COULD | Notities |
| UR-009 | Als admin wil ik de standaard verkiezingsregels kunnen configureren | F | MUST | Notities |
| UR-010 | Als admin wil ik als enige de resultaten van de verkiezing in kunnen zien | F | MUST | Notities |
| UR-011 | Als admin wil ik alle kiezers die niet hebben gekozen in een verkiezing een reminder kunnen sturen. | F | MUST | Klantgesprek week 4 |
| UR-012 | Binnen 3 maanden na het einde van de verkiezing wil ik als admin de verkiezing handmatig kunnen beëindigen. | F | MUST | gesprek week 2 |
| UR-013 | Als KiesGroep wil ik kunnen stemmen op een verkiesbare | F | COULD | Notities |

Systeem requirements

| **naam** | **beschrijving** | **F/NF** | **priority** | **bron** |
| --- | --- | --- | --- | --- |
| SR-001 | De regels kunnen niet meer aangepast worden wanneer de stemming is begonnen | NF | MUST | Notities |
| SR-002 | De applicatie moet werken op Chrome en Safari. | NF | MUST | Notities |
| SR-003 | Resultaten worden binnen de applicatie opgeslagen | NF | MUST | Notities |
| SR-004 | De frontend is gemaakt in dark mode | F | MUST | Notities |
| SR-005 | De frontend bevat een mogelijkheid om light mode aan te zetten | F | COULD | Notities |
| SR-006 | De email reminder voor stemmen is instelbaar | F | MUST | Notities |
| SR-007 | De applicatie maakt gebruik van een database om informatie op te slaan | NF | MUST | Notities |
| SR-008 | De applicatie kan beide op desktop en mobiel browsers draaien. | NF | COULD | Notities |

##


##



## User stories

aan de hand van de requirements zijn user stories en een user interaction diagram gemaakt, dit verduidelijkt hoe een gebruiker gebruik kan maken van de interface van het verkiezingssysteem.

Aan de hand van de volgende twee user interaction diagrams is te zien hoe een gebruiker en admin zich navigeren door functies en paginas in de web applicatie, verdere uitleg is te vinden in de user stories.

### User interaction diagram: User

![alt_text](images/FunctionalDesign/image19.png "image_tooltip")


### Admin

![alt_text](images/FunctionalDesign/image18.png "image_tooltip")


**Meld aan**

**Actor:** gebruiker

**Normale flow:**

Voordat er gebruik gemaakt wordt van de verkiezings-applicatie, krijgt een werknemer binnen Carthago ICT een uitnodiging om deel te nemen aan een verkiezing binnen het bedrijf. Werknemers met deze uitnodiging kunnen een account aanmaken op de startpagina van de applicatie, gebruik makend van hun bedrijf e-mailadres en een nieuw wachtwoord.

De gebruiker vult deze twee in de gegeven textboxes en klikt op 'meld aan', waarna de gebruiker wordt aangemeld als kiezer en toegang krijgt tot de applicatie.

**alternatieven:**

De gebruiker vult niet het juiste e-mailadres in en klikt op 'meld aan', de gebruiker blijft op de startpagina en krijgt de foutmelding dat niet de juiste informatie is gegeven.

**Log in**

**Actor:** gebruiker

**Normale flow:**

Werknemers met een account kunnen op de startpagina van de applicatie, gebruik makend van hun bedrijf e-mailadres en hun gekozen wachtwoord, inloggen.

De gebruiker vult deze twee in de gegeven textboxes en klikt op 'log in', waarna de gebruiker wordt aangemeld als kiezer en toegang krijgt tot de applicatie.

**alternatieven:**

De gebruiker vult niet het juiste e-mailadres of wachtwoord in en klikt op 'log in', de gebruiker blijft op de startpagina en krijgt de foutmelding dat niet de juiste informatie is gegeven.

**Kiezen binnen een verkiezing**

**Actor:** kiezer, verkiesbare, admin

**Normale flow:**

In het hoofdpagina staat een lijst van verkiesbare profielen. in deze profielen staat een foto, slogan en een tickbox die aangevinkt wordt om aan te geven dat je wil stemmen op deze persoon. In de standaardsituatie kunnen tot 5 verschillende profielen worden aangevinkt op deze manier, waarna de kiezer de 'kies' knop kan aanklikken om de stemmen te bevestigen. Nadat er gekozen is, blijft de kiezer op de pagina van de verkiezing maar kan niet meer stemmen uitbrengen.

**alternatieven:**

Als de kiezer een verkiesbare heeft aangevinkt en daarna de pagina van de verkiezing verlaat, wordt de keuze niet opgeslagen en worden geen stemmen uitgebracht.

Als er gekozen wordt met minder dan de maximaal aantal stemmen, kan de rest van de overgebleven stemmen niet langer gebruikt worden.

**Stijl applicatie aanpassen(optioneel)**

**Actor:** kiezer, verkiesbare, admin

**Normale flow:**

Wanneer de ingelogde gebruiker binnen de applicatie is, kan het stijl menu worden geopend en krijgt de gebruiker de optie om een dropdown menu te openen met een aantal stijlen die gekozen worden. De keuzes kunnen bestaan uit een dark en light mode, of mogelijk nog meer.

**Aanmelden als verkiesbare**

**Actor:** kiezer

**Normale flow:**

Wanneer een kiezer de hoofdpagina met de aanstaande verkiezing bekijkt, staat hier het aantal verkiesbare, maar kunnen nog geen stemmen worden uitgebracht, in plaats daarvan is er de optie om aan te melden als verkiesbare. Om dit te doen klikt de kiezer op 'meld aan als verkiesbare', en krijgen deze de vraag zij het zeker weten. Als de kiezer dan op ja klikt gaan ze naar het verkiezings-profiel scherm maar kan daarna nog steeds worden teruggetrokken.

**alternatieven:**

De gebruiker kan in de fase dat wordt gevraagd of zij zeker van hun zaak zijn, de pagina verlaten of nee antwoorden, in dit geval wordt de kiezer niet aangemeld als verkiesbare.

**Verkiezings-profiel opstellen**

**Actor:** verkiesbare

**Normale flow:**

Verkiesbaren kunnen in de pagina van de verkiezing waar ze verkiesbaar zijn een extra optie zien, namelijk kunnen zij de 'profiel' knop zien, waarmee ze naar de profielpagina kunnen gaan. Op de profielpagina kan een foto en een korte biografie worden gegeven en opgeslagen. Dit is alleen mogelijk voordat de verkiezing is gestart, maar kan in deze tijd meerdere keren aangepast worden.

**Terugtrekken uit een verkiezing**

**Actor:** verkiesbare

**Normale flow:**

Wanneer een verkiesbare de pagina van de verkiezing bekijkt waar zij zelf zijn aangemeld als verkiesbare, heeft de verkiesbare in de pagina van de verkiezing, naast de optie om hun profiel aan te passen, de optie om terug te trekken in plaats van de optie om zich aan te melden. bij het gebruik hiervan wordt ook bevestigd of de gebruiker het zeker weet.

**alternatieven:**

De gebruiker kan in de fase dat wordt gevraagd of zij zeker van hun zaak zijn, de pagina verlaten of nee antwoorden, in dit geval wordt de kiezer niet afgemeld als verkiesbare.

**Een verkiezing starten**

**Actor:** admin

**Normale flow:**

Als iemand is aangemeld als admin, is er als deel van de header de optie om een nieuwe verkiezing te starten, de admin klikt op de knop 'nieuwe verkiezing' en gaat naar de pagina voor een nieuwe verkiezing, waar de admin informatie invult over de verkiezing, zoals de naam, de startdatum en de einddatum. Als admins ook regels van de verkiezing in de applicatie kunnen aanpassen, kan dit hier ook gedaan worden.

**Een verkiezing annuleren**

**Actor:** admin

**Normale flow:**

In de hoofdpagina van de verkiezing, waar andere gebruikers zich kunnen aanmelden en afmelden als verkiesbare, hebben admins ook de optie om een bestaande verkiezing te annuleren tijdens de fase dat de verkiezing nog niet open is. De admin klikt op de 'annuleer verkiezing' knop en bevestigt daarna dat zij het zeker weten.

**alternatieven:**

De gebruiker kan in de fase dat wordt gevraagd of zij zeker van hun zaak zijn, de pagina verlaten of nee antwoorden, in dit geval wordt de verkiezing niet geannuleerd.

**Verkiezingsregels aanpassen(optioneel)**

**Actor:** admin

**Normale flow:**

Naast de optie om een verkiezing te annuleren, kan een admin mogelijk ook de regels van een verkiezing aanpassen zolang het nog in de fase is dat de verkiezing nog niet is geopend. De admin klikt op de knop 'regels' en wordt gebracht naar de regels pagina waar alle regels, start en einddatum, en naam van de verkiezingen worden weergegeven en een aantal tot allen aanpasbaar zijn. de admin bevestigd daarna hun keuze en wordt teruggebracht naar de hoofdpagina.

**alternatieven:**

De gebruiker kan in de fase dat wordt gevraagd of zij zeker van hun zaak zijn, de pagina verlaten of nee antwoorden, in dit geval worden de regels niet aangepast.

**Een verkiezing beëindigen**

**Actor:** admin

**normale flow:**

Nadat een verkiezing is geëindigd, kan een admin van het systeem de uitslag van de verkiezing zien op de pagina van de geëindigde verkiezing. Op deze pagina heeft de admin de optie om de verkiezing te verwijderen, waarbij de verkiezing en de gebruikers verwijdert worden, wat het systeem terugbrengt naar het nulpunt voor dat de verkiezing is gestart. De keuze wordt eerst bevestigd of de admin het zeker weet.

**alternatieven:**

De gebruiker kan in de fase dat wordt gevraagd of zij zeker van hun zaak zijn, de pagina verlaten of nee antwoorden, in dit geval wordt de verkiezing niet beëindigd.


# 


# Functioneel ontwerp

## Front-end

De front end van het verkiezingssysteem wordt gebouwd op basis van de wireframes en requirements in svelte. De front end praat tegen de gebouwde back-end aan om data te kunnen weergeven. De front end maakt gebruik van Svelte stores om informatie zoals tokens op te slaan.

De front end van de verkiezingswebsite die is ontwikkeld met svelte laadt components op basis van welke pagina is geopend. Hierin zullen sommige componenten blijven staan, zoals de header en footer, en andere componenten worden worden weggezet en naar voren gezet terwijl de gebruiker in de pagina navigeert, zoals de invulvelden van inloggen of een verkiezing aanmaken.

Terwijl de gebruiker in de website navigeert, en van pagina verandert, wordt gebruik gemaakt van een router, die de url van de huidige pagina aanpast wanneer de router wordt aangeroepen hiervoor. Ook zal de router page componenten laden op basis van welke url op dat moment actief is. Een page component is de pagina en bestaat uit meerdere sub-componenten, zoals blokken met informatie en knoppen met functionaliteiten. De functionaliteiten van componenten is geïmplementeerd in het component zelf.
## Uitgangspunten

Hieronder worden uitgangspunten beschreven. Deze worden verder uitgelegd bij de wireframes.

De uitgangspunten van de wireframes zijn:

1. Een admin interface.
2. Een gebruikers interface.


### Admin interface
De admin gebruiker kan een aantal handelingen doen binnen de applicatie.

De admin kan:

- Inloggen en uitloggen.
- Verkiezingen aanmaken en verwijderen.
- Gebruikers beheren(toevoegen aan verkiezing en verwijderen).
- De regels aanpassen voordat de verkiezing begint.
- De uitslag inzien.

### gebruikersinterface

De gebruiker kan een aantal handelingen doen in de applicatie.

De gebruiker kan:

- Inloggen en uitloggen.
- Stemmen in uitgenodigde verkiezingen op mensen die zich verkiesbaar hebben gesteld.
- Zichzelf verkiesbaar stellen.








![alt_text](images/FunctionalDesign/image17.png "image_tooltip")
Wanneer de gebruiker nog niet is ingelogd en de website opent zal deze een inlogpagina getoond worden. Hier kan de gebruiker inloggen nadat hij een account heeft aangemaakt via een uitnodiging.






![alt_text](images/FunctionalDesign/image15.png "image_tooltip")


Wanneer de gebruiker probeert in te loggen met onjuiste gegevens zal er een error message getoond worden op de inlog pagina.






![alt_text](images/FunctionalDesign/image12.png "image_tooltip")


Nadat een gebruiker is ingelogd zal deze naar de huidige verkiezingen pagina gestuurd worden. Hier kan de gebruiker alle verkiezingen zien die nog niet geëindigd zijn. Door op een van deze verkiezingen te klikken kan een gebruiker naar de pagina van deze verkiezing.

Boven elke pagina staat ook een navigatie bar. Deze is hetzelfde voor elke pagina. Hier kan een gebruiker navigeren naar de huidige verkiezingen en aankomende verkiezingen. Ook is er een knop waarmee gebruikers kunnen uitloggen. Een admin zal ook de optie hebben om naar de afgelopen verkiezingen, verkiezingen aanmaken en gebruikers beheren pagina navigeren.






![alt_text](images/FunctionalDesign/image10.png "image_tooltip")


Wanneer een gebruiker op een verkiezing, die actief is, klikt zal deze naar het stem scherm gestuurd worden. Hier kan de gebruiker in een keer al zijn stemmen uitbrengen als de gebruiker dat nog niet heeft gedaan. Ook kan de gebruiker op een kandidaat klikken om deze verder te inspecteren.






![alt_text](images/FunctionalDesign/image11.png "image_tooltip")


Bij het inspecteren van een kandidaat wordt een beschrijving getoond en zijn/haar standpunten. Ook kan de gebruiker weer terug naar de verkiezing door op terug te drukken.






![alt_text](images/FunctionalDesign/image10.png "image_tooltip")


Op deze pagina kan een gebruiker alle aankomende verkiezingen zien. Door op een verkiezing te klikken kan een gebruiker zich verkiesbaar stellen voor deze verkiezing. Een admin kan op deze pagina ook nog verkiezingen wijzigen of verwijderen.






![alt_text](images/FunctionalDesign/image1.png "image_tooltip")


Wanneer een admin hier een verkiezing wilt verwijderen, verschijnt er een popup om te voorkomen dat een verkiezing per ongeluk verwijdert wordt.






![alt_text](images/FunctionalDesign/image14.png "image_tooltip")


Wanneer er op een aankomende verkiezing geklikt wordt kan de gebruiker alle kandidaten zien en zichzelf verkiesbaar stellen.






![alt_text](images/FunctionalDesign/image6.png "image_tooltip")


Wanneer je klikt op ‘Stel verkiesbaar’ wordt er eerst een popup getoond zodat je niet per ongeluk jezelf verkiesbaar stelt.






![alt_text](images/FunctionalDesign/image7.png "image_tooltip")


Wanneer een gebruiker zichzelf verkiesbaar wil stellen voor een verkiezingen, kan de gebruiker dat doen door middels van dit scherm. Hier kan de gebruiker een foto instellen en een beschrijven, slogan en standpunten invullen.






![alt_text](images/FunctionalDesign/image10.png "image_tooltip")


Na het bevestigen van het verkiesbaar stellen wordt de user weer teruggestuurd naar het verkiezings scherm. Hier kan de gebruiker zichzelf nu ook terugtrekken of zijn/haar profiel aanpassen.






![alt_text](images/FunctionalDesign/image4.png "image_tooltip")


Wanneer er op ‘Trek verkiesbaarheid terug’ geklikt wordt, wordt er een popup getoond zodat er niet per ongeluk teruggetrokken kan worden.






![alt_text](images/FunctionalDesign/image9.png "image_tooltip")


Een admin kan een verkiezing aanmaken met deze pagina. De admin kan hier een naam en beschrijving invoeren. Ook moeten de start- en einddatum ingesteld worden. Net zoals de hoeveelheid stemmen per persoon en of verkiesbare op zichzelf mogen stemmen. Wanneer de verkiezing gemaakt is zal deze in de aankomende verkiezingen scherm tevoorschijn komen.

Wanneer een admin een verkiezing wilt wijzigen zal deze pagina ook geopend worden, maar dan is alle informatie al ingevuld met de huidige informatie.


## Back-end

De back-end van het verkiezingssysteem wordt gebouwd via Java Spring Boot. Er zal een systeem worden gebouwd waar het front-end verbinding mee kan maken om data in te laden. De back-end zal met behulp van een database de data opslaan. 

## ChangeLog

Hier wordt getoond in welke week informatie is toegevoegd.


**30 november**

voorblad toegevoegd.

begin aan de inhoudsopgave toegevoegd.

introductie toegevoegd.

stakeholders toegevoegd.

planning toegevoegd.

backlog toegevoegd.

business requirements toegevoegd.

user requirements toegevoegd.

user stories toegevoegd.

back-end toegevoegd.

data opslag toegevoegd.

beveiliging toegevoegd.

**4 december**

visie product toegevoegd.

datum aangepast.

functioneel ontwerp met wireframes toegevoegd.


**16 december**

inhoudsopgave aangepast toegevoegd.

**17 december**

uitgangspunten toegevoegd.

wireframes aangepast.

**18 december**

inhoudsopgave aangepast.

backlog aangepast.

user requirements aangepast.

UserStories aangepast.

wireframes aangepast.

planning aangepast.

**13 Januari**

Changelog toegevoegd.

**25 Januari**

Uitleg front end is uitgebreid.

##