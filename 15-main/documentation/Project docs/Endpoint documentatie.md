## **Get Requests**


<table>
  <tr>
   <td><strong>GET</strong>
   </td>
   <td colspan="4" ><strong>/users/{id}</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" ><strong>
none
</strong>
   </td>
  </tr>
  <tr>
   <td colspan="5" >Querried de user met id {id}
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>Id*
   </td>
   <td>path
   </td>
   <td>De id van de user die gereturned moet worden
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>De token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="5" colspan="2" ><strong> </strong>
   </td>
   <td>200
   </td>
   <td colspan="2" >Locaties is gevonden en verstuurt. Bijvoorbeeld: <strong><em><span style="text-decoration:underline;">TODO</span></em></strong>
   </td>
  </tr>
  <tr>
   <td>404
   </td>
   <td colspan="2" >User met id {id} niet gevonden
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >{id} is geen nummer
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >User is geen admin
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
{<br>
id:{int}<br>
email:{string}<br>
saltedPasswordHash:{string}<br>
salt:{string}<br>
}</td>
  </tr>
</table>





<table>
  <tr>
   <td><strong>GET</strong>
   </td>
   <td colspan="4" ><strong>/users</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" ><strong>
none
</strong>
   </td>
  </tr>
  <tr>
   <td colspan="5" >Querried alle users
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>De token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="3" colspan="2" ><strong> </strong>
   </td>
   <td>200
   </td>
   <td colspan="2" >User is gevonden en verstuurt. Bijvoorbeeld: <strong><em><span style="text-decoration:underline;">TODO</span></em></strong>
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >User is geen admin
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
[ <br>
{<br>
id:{int}<br>
email:{string}<br>
saltedPasswordHash:{string}<br>
salt:{string}<br>
}<br>
]</td>
  </tr>
</table>



<table>
  <tr>
   <td><strong>GET</strong>
   </td>
   <td colspan="4" ><strong>/elections/{id}</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" ><strong>
none
</strong>
   </td>
  </tr>
  <tr>
   <td colspan="5" >Querried de election met id {id}
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>Id*
   </td>
   <td>path
   </td>
   <td>De id van de user die gereturned moet worden
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>De token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="5" colspan="2" ><strong> </strong>
   </td>
   <td>200
   </td>
   <td colspan="2" >Election is gevonden en verstuurt. Bijvoorbeeld: <strong><em><span style="text-decoration:underline;">TODO</span></em></strong>
   </td>
  </tr>
  <tr>
   <td>404
   </td>
   <td colspan="2" >User met id {id} niet gevonden
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >{id} is geen nummer
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >User is geen admin of is geen kiezer bij deze election
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
{<br>
id:{int}<br>
noVotes:{int}<br>
startDate:{date}<br>
endDate:{date}<br>
canVoteOnSelf:{bool}<br>
description:{string}<br>
}</td>
  </tr>
</table>



<table>
  <tr>
   <td><strong>GET</strong>
   </td>
   <td colspan="4" ><strong>/elections</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" ><strong>
none
</strong>
   </td>
  </tr>
  <tr>
   <td colspan="5" >Querried alle elections
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>De token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="3" colspan="2" ><strong> </strong>
   </td>
   <td>200
   </td>
   <td colspan="2" >Election is gevonden en verstuurt. Bijvoorbeeld: <strong><em><span style="text-decoration:underline;">TODO</span></em></strong>
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >User staat niet in systeem
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
[<br>
{<br>
id:{int}<br>
noVotes:{int}<br>
startDate:{date}<br>
endDate:{date}<br>
canVoteOnSelf:{bool}<br>
description:{string}<br>
}<br>]</td>
  </tr>
</table>



<table>
  <tr>
   <td><strong>GET</strong>
   </td>
   <td colspan="4" ><strong>/elections/{id}/electables</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" ><strong>
none
</strong>
   </td>
  </tr>
  <tr>
   <td colspan="5" >Querried alle electables voor de election met id {id}
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>De token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>id*
   </td>
   <td>path
   </td>
   <td>id van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="3" colspan="2" ><strong> </strong>
   </td>
   <td>200
   </td>
   <td colspan="2" >Electable is gevonden en verstuurt. Bijvoorbeeld: <strong><em><span style="text-decoration:underline;">TODO</span></em></strong>
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >User heeft geen toegang tot de election
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
[<br>{<br>
id:{int}<br>
name:{string}<br>
photoUrl:{string}<br>
slogan:{string}<br>
description:{string}<br>
standPoints:{string}<br>
userId:{int}<br>
electionId:{int}<br>
}<br>]</td>
  </tr>
</table>



<table>
  <tr>
   <td><strong>GET</strong>
   </td>
   <td colspan="4" ><strong>/elections/{id}/electables/{userid}</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" ><strong>
none
</strong>
   </td>
  </tr>
  <tr>
   <td colspan="5" >Querried de electable met userid {userid} voor de election met id {id}
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>De token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>id*
   </td>
   <td>path
   </td>
   <td>Id van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>userid*
   </td>
   <td>path
   </td>
   <td>id van de electable
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="3" colspan="2" ><strong> </strong>
   </td>
   <td>200
   </td>
   <td colspan="2" >Electable is gevonden en verstuurt. Bijvoorbeeld: <strong><em><span style="text-decoration:underline;">TODO</span></em></strong>
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >User staat niet in systeem
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
[<br>{<br>
id:{int}<br>
name:{string}<br>
photoUrl:{string}<br>
slogan:{string}<br>
description:{string}<br>
standPoints:{string}<br>
userId:{int}<br>
electionId:{int}<br>
}<br>]</td>
  </tr>
</table>



## **Post Requests**


<table>
  <tr>
   <td><strong>POST</strong>
   </td>
   <td colspan="4" ><strong>/users</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" >
{<br>
email:{string}<br>
saltedPasswordHash:{string}<br>
salt:{string}<br>
}</td>
  </tr>
  <tr>
   <td colspan="5" >Een user wordt aangemaakt
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>email*
   </td>
   <td>body
   </td>
   <td>De email van de nieuwe user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>The token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>201
   </td>
   <td colspan="2" >User is aangemaakt
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >Ingelogde user is geen admin
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
status:CREATED
  </tr>
</table>



<table>
  <tr>
   <td><strong>POST</strong>
   </td>
   <td colspan="4" ><strong>/users</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" >
{<br>
email:{string}<br>
saltedPasswordHash:{string}<br>
salt:{string}<br>
}</td>
  </tr>
  <tr>
   <td colspan="5" >Een user logt in
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>email*
   </td>
   <td>body
   </td>
   <td>De email van de nieuwe user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>password*
   </td>
   <td>obdy
   </td>
   <td>Wachtwoord van de user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>200
   </td>
   <td colspan="2" >User login is successvol. User token is teruggestuurd
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >Inlog gegevens incorrect
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
token:{token}
  </tr>
</table>





<table>
  <tr>
   <td><strong>POST</strong>
   </td>
   <td colspan="4" ><strong>/elections</strong>
   </td>
  </tr>
<tr>
   <td><strong>body</strong>
   </td>
   <td colspan="4" >
{<br>
noVotes:{int}<br>
startDate:{date}<br>
endDate:{date}<br>
canVoteOnSelf:{bool}<br>
description:{string}<br>
}</td>
  </tr>
  <tr>
   <td colspan="5" >Een election wordt aangemaakt
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>The token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>startdate*
   </td>
   <td>body
   </td>
   <td>De start datum van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>enddate*
   </td>
   <td>body
   </td>
   <td>De eind datum van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>numberofvotes*
   </td>
   <td>body
   </td>
   <td>De hoeveelheid votes per user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>description*
   </td>
   <td>body
   </td>
   <td>Een beschijving van de verkiezing
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>201
   </td>
   <td colspan="2" >Election is aangemaakt
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >Ingelogde user is geen admin
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
status:CREATED
  </tr>
</table>



<table>
  <tr>
   <td><strong>POST</strong>
   </td>
   <td colspan="4" ><strong>/elections/{electionid}/electables</strong>
   </td>
  </tr>
<tr>
   <td><strong>body</strong>
   </td>
   <td colspan="4" >
{<br>
name:{string}<br>
photoUrl:{string}<br>
slogan:{string}<br>
description:{string}<br>
standPoints:{string}<br>
userId:{int}<br>
electionId:{int}<br>
}</td>
  </tr>
  <tr>
   <td colspan="5" >Een election wordt aangemaakt
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>The token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>electionid*
   </td>
   <td>path
   </td>
   <td>de id van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>description*
   </td>
   <td>body
   </td>
   <td>De description van de electable
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>photourl*
   </td>
   <td>body
   </td>
   <td>De url voor de profiel foto
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>201
   </td>
   <td colspan="2" >Electable is aangemaakt
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >Ingelogde user is geen kiezer voor de election
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
status:CREATED
  </tr>
</table>



<table>
  <tr>
   <td><strong>POST</strong>
   </td>
   <td colspan="4" ><strong>/elections/{electionid}/voters</strong>
   </td>
  </tr>
<tr>
   <td><strong>body</strong>
   </td>
   <td colspan="4" >
{<br>
[voterId{int}]<br>
}</td>
  </tr>
  <tr>
   <td colspan="5" >Een voter wordt aangemaakt
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>The token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>electionid*
   </td>
   <td>path
   </td>
   <td>de id van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>userid*
   </td>
   <td>body
   </td>
   <td>Id van de user die mag voten
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>201
   </td>
   <td colspan="2" >voter is aangemaakt
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >Ingelogde user is geen admin
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
status:CREATED
  </tr>
</table>







## **Put Requests**


<table>
  <tr>
   <td><strong>PUT</strong>
   </td>
   <td colspan="4" ><strong>/users</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" >
{<br>
email:{string}<br>
saltedPasswordHash:{string}<br>
salt:{string}<br>
}</td>
  </tr>
  <tr>
   <td colspan="5" >Het wachtwoord van de user die gelinkt is aan de invitetoken wordt aangepast
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>invitetoken*
   </td>
   <td>body
   </td>
   <td>De token voor de user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>password*
   </td>
   <td>body
   </td>
   <td>Het wachtwoord van de user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="3" colspan="2" ><strong> </strong>
   </td>
   <td>201
   </td>
   <td colspan="2" >User wachtwood is aangepast
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
none
  </tr>
</table>





<table>
  <tr>
   <td><strong>PUT</strong>
   </td>
   <td colspan="4" ><strong>/users</strong>
   </td>
  </tr>
<tr>
   <td><strong>Body</strong>
   </td>
   <td colspan="4" >
{<br>
email:{string}<br>
saltedPasswordHash:{string}<br>
salt:{string}<br>
}</td>
  </tr>
  <tr>
   <td colspan="5" >Het wachtwoord van de user die gelinkt is aan de invitetoken wordt aangepast
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>userToken*
   </td>
   <td>header
   </td>
   <td>De token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>email*
   </td>
   <td>body
   </td>
   <td>Het wachtwoord van de user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>userid*
   </td>
   <td>body
   </td>
   <td>id van de user die aangepast moet worden
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="3" colspan="2" ><strong> </strong>
   </td>
   <td>201
   </td>
   <td colspan="2" >User wachtwoord is aangepast
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
none
  </tr>
</table>



<table>
  <tr>
   <td><strong>PUT</strong>
   </td>
   <td colspan="4" ><strong>/elections</strong>
   </td>
  </tr>

  <tr>
   <td colspan="5" >De election wordt aangepast
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>The token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>startdate*
   </td>
   <td>body
   </td>
   <td>De start datum van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>enddate*
   </td>
   <td>body
   </td>
   <td>De eind datum van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>numberofvotes*
   </td>
   <td>body
   </td>
   <td>De hoeveelheid votes per user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>description*
   </td>
   <td>body
   </td>
   <td>Een beschijving van de verkiezing
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong> </strong>
   </td>
   <td>201
   </td>
   <td colspan="2" >Election is aangepast
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
none
  </tr>
</table>



<table>
  <tr>
   <td><strong>PUT</strong>
   </td>
   <td colspan="4" ><strong>/elections/{id}</strong>
   </td>
  </tr>
<tr>
   <td><strong>body</strong>
   </td>
   <td colspan="4" >
{<br>
noVotes:{int}<br>
startDate:{date}<br>
endDate:{date}<br>
canVoteOnSelf:{bool}<br>
description:{string}<br>
}</td>
  </tr>
  <tr>
   <td colspan="5" >Verander de vote van een user in een election
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>hasVoted*
   </td>
   <td>body
   </td>
   <td>Of een user gestemd heeft
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>De token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>201
   </td>
   <td colspan="2" >electionUserLink is aangepast
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>404
   </td>
   <td colspan="2" >User mag niet stemmen op de election
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
none
  </tr>
</table>





<table>
  <tr>
   <td><strong>PUT</strong>
   </td>
   <td colspan="4" ><strong>/elections/{id}/electables</strong>
   </td>
  </tr>
<tr>
   <td><strong>body</strong>
   </td>
   <td colspan="4" >
{<br>
name:{string}<br>
photoUrl:{string}<br>
slogan:{string}<br>
description:{string}<br>
standPoints:{string}<br>
userId:{int}<br>
electionId:{int}<br>
}</td>
  </tr>
  <tr>
   <td colspan="5" >Verander het profiel van een electable in een election
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>The token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>electionid*
   </td>
   <td>path
   </td>
   <td>de id van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>description*
   </td>
   <td>body
   </td>
   <td>De description van de electable
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>photourl*
   </td>
   <td>body
   </td>
   <td>De url voor de profiel foto
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>201
   </td>
   <td colspan="2" >Electable is aangepast
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >Ingelogde user is geen kiezer voor de election
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
none
  </tr>
</table>



## **Delete Requests**


<table>
  <tr>
   <td><strong>DELETE</strong>
   </td>
   <td colspan="4" ><strong>/user/{id}</strong>
   </td>
  </tr>
<tr>
   <td><strong>body</strong>
   </td>
   <td colspan="4" >
none</td>
  </tr>
  <tr>
   <td colspan="5" >Er wordt een user verwijderd met id {id}
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" ><em> </em>
   </td>
   <td>Id*
   </td>
   <td>path
   </td>
   <td>Het id van de item die verwijderd moet worden
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>Token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>204
   </td>
   <td colspan="2" >De user is verwijderd
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Id is geen nummer
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >Ingelogde user is geen admin
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
none</td>
  </tr>
</table>



<table>
  <tr>
   <td><strong>DELETE</strong>
   </td>
   <td colspan="4" ><strong>/elections/{id}</strong>
   </td>
  </tr>
<tr>
   <td><strong>body</strong>
   </td>
   <td colspan="4" >
none</td>
  </tr>
  <tr>
   <td colspan="5" >Er wordt een election verwijderd met id {id}
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" ><em> </em>
   </td>
   <td>Id*
   </td>
   <td>path
   </td>
   <td>Het id van de election die verwijderd moet worden
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>Token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>204
   </td>
   <td colspan="2" >De electionis verwijderd
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Id is geen nummer
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >Ingelogde user is geen admin
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
none</td>
  </tr>
</table>



<table>
  <tr>
   <td><strong>DELETE</strong>
   </td>
   <td colspan="4" ><strong>/elections/{id}/electables/{userid}</strong>
   </td>
  </tr>
<tr>
   <td><strong>body</strong>
   </td>
   <td colspan="4" >
none</td>
  </tr>
  <tr>
   <td colspan="5" >Delete de electable met userid {userid} voor de election met id {id}
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Parameters:</strong>
   </td>
   <td><strong>Name</strong>
   </td>
   <td><strong>Type</strong>
   </td>
   <td><strong>Description</strong>
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>usertoken*
   </td>
   <td>header
   </td>
   <td>Token van de ingelogde user
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>id*
   </td>
   <td>path
   </td>
   <td>Id van de election
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>userid*
   </td>
   <td>path
   </td>
   <td>id van de electable user
   </td>
  </tr>
  <tr>
   <td colspan="2" ><strong>Responses:</strong>
   </td>
   <td><strong>Code</strong>
   </td>
   <td colspan="2" ><strong>Description / example if successful</strong>
   </td>
  </tr>
  <tr>
   <td rowspan="4" colspan="2" ><strong> </strong>
   </td>
   <td>204
   </td>
   <td colspan="2" >De electable is verwijderd
   </td>
  </tr>
  <tr>
   <td>400
   </td>
   <td colspan="2" >Missende of ongeldige parameters
   </td>
  </tr>
  <tr>
   <td>401
   </td>
   <td colspan="2" >Ingelogde user is geen admin of niet de te verwijderen electable user
   </td>
  </tr>
  <tr>
   <td>500
   </td>
   <td colspan="2" >Server heeft een error
   </td>
  </tr>
<tr>
   <td><strong>output</strong>
   </td>
   <td colspan="4" >
none</td>
  </tr>
</table>

