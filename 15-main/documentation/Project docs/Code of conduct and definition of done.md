# FreeBird, Code of conduct & definition of done

Project Client on board, Saxion Hogeschool

Carthago ICT - Verkiezingssysteem

Sprint 1, Code of conduct & definition of done

Reglementen:

Strikes:



* Bij 1 strike wordt het alleen duidelijk gemaakt aan de leraar
* Bij 2 strikes wordt er een waarschuwing uitgegeven en wordt dit duidelijk gemaakt aan de leraar.
* Bij 3 strikes wordt de desbetreffende persoon uit de groep gezet met hulp van de leraar. Voordat dit gebeurd wordt dit eerst duidelijk gemaakt aan de leraar.

Te laat komen/Afwezig zijn:



* Bij 2 keer te laat komen of 1 keer afwezig zijn zonder goede reden wordt er een strike uitgedeeld en wordt dit duidelijk gemaakt aan de leraar.
* goede reden is alleen noodgevallen (OV vertraging, ziekenhuis etc.)
* slechte reden is bijvoorbeeld: verslapen, geen zin, vergeten.

Tijdsbesteding:



* Als er niet ongeveer een gelijke hoeveelheid uren zijn gewerkt onder het team wordt er gekeken waarom dit zo is. Als dit geen geldige reden heeft kan er een waarschuwing worden gegeven of zelfs een strike gebaseerd op de hoeveelheid tijd en de reden.
* Als er een taak niet op tijd afgemaakt is wordt er gekeken wat de reden hierachter is. Is het niet een goede reden (bijvoorbeeld weinig tijd besteed aan het project) dan kan er een waarschuwing of strike gegeven worden door de groep.

  Als het wel een goede reden heeft kan de taak overgedragen worden aan een ander of kan de taak verplaatst worden naar een later tijdstip.


programmeertaal-keuze



* De backend wordt geschreven in Java met behulp van Spring boot
* De frontend wordt geschreven in Svelte

Merge requests



* Voordat een merge request wordt aangemaakt moet de development branch in de feature branch gemerged worden en moet de code even opnieuw nagekeken worden.
* Iedereen gaat bezig met merge requests zodat de hoeveelheid werk gelijk is verdeeld over het team.
* Een merge request wordt door 2 mensen bekeken. Als deze twee mensen een akkoord hebben gegeven word de branch gemerged naar de target branch.
* Als de merge request niet is geaccepteerd door een bug wordt er een bug issue aangemaakt met het volgende formaat: &lt;issue nummer> &lt;bug nummer> &lt;korte beschrijvende naam>
* Als de merge request niet is geaccepteerd omdat de code nog niet als klaar is wordt er verwacht dat de developer die de code heeft gemaakt de code weer aanpast gebaseerd op de feedback. Vervolgens moet deze developer weer een nieuwe merge request aanmaken.

code controle voor merge met main (features en bugs)



* Er zijn geen errors als de code wordt uitgevoerd
* De geschreven code werkt met de andere code
* De geschreven code wordt door 2 mensen gecontroleerd.
* is de code goedgekeurd? dan klikt de persoon die de controle heeft uitgevoerd op de like knop en zet een comment in de beschrijving met zijn naam (bv: “Checked by: Jordy”)
* is de code niet goedgekeurd? dan klikt de persoon die controle heeft uitgevoerd op de dislike knop en zet een comment in de beschrijving van de issue met een beschrijving van het probleem en hoe je het probleem kan recreëren (bv: “The banana.png does not stretch correctly. ”)
* Zijn er 2 likes op een issue? dan mag de issue branch gemerged worden met de main branch

Feature branching



* Branche namen zijn geschreven in de engelse taal.
* Voor elke issue wordt een branch aangemaakt in de vorm “feature/&lt;issue nummer> &lt;korte beschrijvende naam>”
* Bijvoorbeeld: feature/10 voting screen

Commits



* commits worden gemaakt in de vorm van “&lt;issue nummer> &lt;beschrijving van de commit>“
* bijvoorbeeld “10 banana.png is fixed”
* alle tekst wordt in het Engels neergezet

Code quality:



* Alle code bevat waar nodig duidelijke comments
* Alle methodes geschreven in java bevatten een javadoc beschrijving
* Alle methodes geschreven in javascript bevatten een jsdoc beschrijving
* voorbeeld van javaDoc beschrijving:
* @author &lt;auteur>
* @description &lt;description>
* Er wordt goed gebruik gemaakt van de aangeleerde regels op school (encapsulatie, inheritance, polymorphisme, interfaces en abstractie).
* Er wordt rekening gehouden met de programmeerprincipes K.I.S.S. (Keep It Super Simple) en D.R.Y. (Don’t Repeat Yourself).
* Semantic programming wordt toegepast voor alle HTML tags in het project.
* De backend volgt de principes zoals het is aangeleerd in de lessen van eerdere vakken.



Teamwork:



* Als iemand niet begrijpt wat hij/zij moet doen word er verwacht dat deze persoon aan de andere groepsgenoten vragen gaat stellen tot het wel duidelijk is.
* Als iemand niet weet hoe hij/zij iets moet programmeren word er verwacht dat deze persoon het eerst zelf probeert op te lossen. Als dit echter te veel tijd kost dan word er verwacht dat deze persoon het met de andere groepsgenoten communiceert zodat het opgelost kan worden.
* Tijdens elke fysieke les wordt er een stand-up meeting gehouden. Het is hier de bedoeling dat er kort wordt uitgelegd wat je hebt gedaan, wat je gaat doen en of je nog tegen problemen bent aangelopen. Het zou door deze meeting duidelijk moeten worden hoe iedereen in het project staat.

Definition of done



* Code wordt altijd volgens de reglementen uitgevoerd.
* Een issue die door een persoon wordt opgepakt wordt compleet afgehandeld door die persoon
* Code wordt altijd gereviewed door op zijn minst 2 andere mensen
* Code wordt altijd getest door de programmeur
* De code in de backend wordt getest door middel van automatische tests, deze zullen ook door dezelfde programmeur moeten worden geschreven
* De gemaakte code bevat alle functionaliteiten die voor de desbetreffende issue relevant zijn.