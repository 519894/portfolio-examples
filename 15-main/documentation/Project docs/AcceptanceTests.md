##


# Acceptatie testen

### UR-001 Als gebruiker wil ik kunnen inloggen

Prerequisites:

- Er is geen gebruiker ingelogd
- De inlog pagina is geopend
- Er is een gebruiker in het systeem, die al een wachtwoord heeft

Stappen:

1. Vul het email in op de login pagina

2. Vul het wachtwoord in op de login pagina

3. Klik op de login knop

PostCondities:

- De homepagina is geopend
- De gebruiker is ingelogd

### UR-002 Als kiezer wil ik mij kunnen inschrijven als verkiesbaar persoon tijdens de inschrijfperiode.

Prerequisites:

- Er is een gebruiker ingelogd, die als kiezer staat ingesteld voor een verkiezing die nog niet is begonnen
- De gebruiker staat nog niet als verkiesbare ingeschreven bij de betreffende verkiezing
- De homepagina is geopend

Stappen:

1. Klik op verkiesbaar stellen bij de niet begonnen verkiezing

2. Klik op ja op de confirmation popup

3. Vul een naam in die nog niet bestaat bij deze verkiezing

4. Vul iets in bij de beschrijving

5. Vul iets in bij de slogan

6. Vul iets in bij de standpunten

7. Klik op bevestigen

8. Klik op ja bij de confirmation popup

PostCondities:

- De gebruiker is ingesteld als verkiesbare bij de betreffende verkiezing
- Er staat nu een trek terug ipv stel verkiesbaar knop bij de verkiezing

### UR-003 Als verkiesbare wil ik mij kunnen uitschrijven uit de kieslijst

Prerequisites:

- Er is een gebruiker ingelogd die als verkiesbaar staat ingesteld op een verkiezing die nog niet is begonnen
- De home pagina is open

Stappen:

1. Klik op trek terug bij de betreffende verkiezing

2. Klik op ja bij de confirmation popup

PostCondities:

- De trek terug knop is veranderd naar stel verkiesbaar
- De gebruiker is geen verkiesbare meer bij de verkiezing

### UR-004 Als gebruiker wil ik een mail krijgen om mee te kunnen doen aan de verkiezing

Prerequisites:

- Er is een admin ingelogd
- Je hebt toegang tot een email waarin de uitnodiging ontvangen zal worden
- De creeer verkiezing pagina is open

Stappen:

1. Vul iets in als naam van de verkiezing

2. Vul iets in als startdatum na de datum van vandaag van de verkiezing

3. Vul een einddatum in die na de startdatum is

4. Vul een positief getal in bij de stemmen per persoon

5. Vul het email waar je de uitnodiging wilt ontvangen in bij nodig mensen uit.

6. Klik op voeg toe

7. Klik op bevestig

8. Klik op ja bij de confirmation popup

PostCondities:

- De verkiezing is aangemaakt
- Het emailadres heeft een uitnodiging ontvangen

### UR-005 Als kiezer wil ik kunnen stemmen op een of meer verkiesbare tijdens de stemperiode

Prerequisites:

- Er is gebruiker ingelogd die ingeschreven staat als kiezer bij een begonnen verkiezingen en die nog niet gestemd heeft
- De verkiezing pagina is geopend

Stappen:

1. Klik op de checkbox onder een kandidaat

2. Klik op bevestig onder op de pagina

3. Klik op ja bij de confirmation popup

PostCondities:

- Je stemmen zijn geregistreerd bij het programma
- Je kan niet nogmaals stemmen

### UR-006 Als admin wil ik mensen kunnen uitnodigen voor een verkiezing

Prerequisites:

- Er is een admin ingelogd
- Je hebt toegang tot een email waarin de uitnodiging ontvangen zal worden
- De creeer verkiezing pagina is open

Stappen:

1. Vul iets in als naam van de verkiezing

2. Vul iets in als startdatum na de datum van vandaag van de verkiezing

3. Vul een einddatum in die na de startdatum is

4. Vul een positief getal in bij de stemmen per persoon

5. Vul het email waar je de uitnodiging wilt ontvangen in bij nodig mensen uit.

6. Klik op voeg toe

7. Klik op bevestig

8. Klik op ja bij de confirmation popup

PostCondities:

- De verkiezing is aangemaakt
- Het emailadres heeft een uitnodiging ontvangen

### UR-007 Als admin wil ik verkiezingen kunnen aanmaken

Prerequisites:

- Er is een admin ingelogd
- De creeer verkiezing pagina is open

Stappen:

1. Vul iets in als naam van de verkiezing

2. Vul iets in als startdatum na de datum van vandaag van de verkiezing

3. Vul een einddatum in die na de startdatum is

4. Vul een positief getal in bij de stemmen per persoon

6. Klik op voeg toe

7. Klik op bevestig

8. Klik op ja bij de confirmation popup

PostCondities:

- De verkiezing is aangemaakt
- De verkiezing is te zien op de home pagina

### UR-008 Als admin wil ik de regels van een verkiezing kunnen aanpassen buiten de code.

Prerequisites:

- Er is een admin ingelogd
- De home pagina is open
- Er is een verkiezing die nog niet begonnen is

Stappen:

1. Klik op bewerken bij de verkiezing

2. Verander iets van de verkiezing

3. Klik op bevestigen

4. Klik op ja bij de confirmation popup

PostCondities:

- De verkiezing is aangepast
- De aanpassing is te zien op de home pagina

### UR-009 Als admin wil ik de standaard verkiezingsregels kunnen configureren

Niet geïmplementeerd

### UR-010 Als admin wil ik als enige de resultaten van de verkiezing in kunnen zien

Prerequisites:

- Er is een admin ingelogd
- De home pagina is open
- Er is een verkiezing die voltooid is

Stappen:

1. Klik op bekijk bij de verkiezing

PostCondities:

- De resultaten zijn te zien

Prerequisites:

- Er is een gebruiker ingelogd, die geen admin is en als kiezer staat ingeschreven bij een voltooide verkiezing
- De home pagina is open
- Er is een verkiezing die voltooid is

PostCondities:

- De voltooide verkiezing is niet te zien

### UR-011 Als admin wil ik alle kiezers die niet hebben gekozen in een verkiezing een reminder kunnen sturen.

Prerequisites:

- Er is een admin ingelogd
- De home pagina is open
- Er is een verkiezing die gestart is
- Bij de verkiezing is een kiezer waarvan je de email kan bekijken, die nog niet gestemd heeft

Stappen:

1. Klik op bekijk bij de verkiezing

2. Klik op stuur herinnering

PostCondities:

- Op de pagina staat herinnering verstuurd
- De kiezer heeft een herinneringsmail ontvangen

### UR-012 Binnen 3 maanden na het einde van de verkiezing wil ik als admin de verkiezing handmatig kunnen verwijderen.

Prerequisites:

- Er is een admin ingelogd
- De home pagina is open
- Er is een verkiezing die gestopt is

Stappen:

1. Klik op verwijderen bij de verkiezing

2. Klik op ja bij de confirmation popup

PostCondities:

- De verkiezing is verwijderd
- De verkiezing is niet meer te zien op de home pagina

### UR-013 Als KiesGroep wil ik kunnen stemmen op een verkiesbare

Niet geïmplementeerd