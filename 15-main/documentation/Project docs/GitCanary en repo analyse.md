# GitCanary en repo analyse

## GitCanary

### Sprint 1:

Als je kijkt naar sprint 1 in GitCanary valt het meteen op dat er twee mensen zijn die veel code hebben toegevoegd en de rest niks of nauwelijks. Dit komt doordat GitCanary HTML en CSS niet als code beschouwd. Wij hebben ons in de eerste sprint vooral op de frontend gericht en dat is de reden waarom de meesten weinig code hebben toegevoegd in de sprint. Maurits ten Dam heeft wel veel code toegevoegd, want hij is bezig geweest met het opzetten van de database. Ook heeft Erik veel code toegevoegd, want hij had al wat code in JavaScript neergezet.

Maurits ten Dam heeft alle documentatie die wij hebben gemaakt omgezet in markdown. Het is dus niet zo dat hij als enige de documentatie heeft gemaakt, maar hij heeft wel alle documentatie in de repo gezet.

### Sprint 2: 

Bij de tweede sprint valt het meteen op dat iedereen wel code toegevoegd, aangepast en verwijderd heeft. Ook zie je al snel dat Maurits Heetkamp er dik boven uit steekt. Dit komt doordat hij zelf iets te druk is geweest met het project en bezig is geweest met veel taken. De rest van de groep heeft allemaal een redelijk gelijk aantal aan code.

## Repo:

Het aantal commits van elk teamgenoot is redelijk gelijk. Simon en vooral Maurits Heetkamp schieten er wel boven uit. Het hoge aantal commits van Simon komt waarschijnlijk omdat hij wat vaker commit dan de anderen, want zijn uren aantal is wel gelijk met de rest van de groep. Het hoge aantal van Maurits komt doordat hij erg veel heeft gedaan in het project, zoals eerder aangegeven. Het aantal commits van Maurits ten Dam is iets lager dan het gemiddelde van de groep. Dit komt doordat hij niet tussen door commit. Hij doet dit pas als hij een issue af heeft.