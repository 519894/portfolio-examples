# Read Me

### Opzetten van het project

Om het project op te zetten moeten de volgende stappen worden uitegevoerd:
1. Download het .zip bestand van het project
2. Pak het .zip bestand uit
3. Open het project in Intellij. Selecteer hierbij de folder repo waar de readme in zit.
4. Markeer de project folders zoals verwacht.  Hiervoor kan het kopje "Markeren van folders" gebruikt worden.
5. Er moet eerst een database worden aangemaakt, hiervoor kunnen de instructies in het kopje "Database Opzetten" gevolgd worden
6. Installeer npm door de instructies in het kopje "Npm installeren" te volgen
7. Voor het project moet Gradle worden ingesteld. Binnen Intellij, ga naar File -> settings -> Build, Execution, Development -> Build tools -> Gradle. Hier moet de Gradle worden ingesteld als degene in het project (Gradle.properties) en de jvm moet worden ingesteld op versie 17.
8. Vervolgens moet de java versie worden ingesteld voor het project. Dit kan via File -> Project Structure -> project. Zet hier de JDK op versie 17.
9. Start eerst de backend database door in het bestand "DemoApplication" (in server/src/java/com.example.demo) links op het groene driehoekje te klikken.
10. Start de client door de terminal in intellij te openen, typ "cd client" en daarna "npm run start"
11. Na "npm run start" in te voeren zal er een url naar de website staan, de url bevat "localhost", klik deze om de website te openen

### Markeren van folders
Om het project goed te laten werken is het belangrijk dat de juiste folders gemarkeerd zijn. Volg hiervoor de onderstaande stappen.

1. markeer de folder src als sources root. Deze folder is te vinden in client/src.
2. markeer de folder java als sources root. Deze folder is te vinden in server/src/main/java.
3. Markeer de folder resources als resources root. Deze folder is te vinden in server/src/main/resources.

### Database Opzetten
Voor de database wordt gebruik gemaakt van PGadmin, deze kan gedownload worden via deze link: https://www.pgadmin.org/download/pgadmin-4-windows/ 

1. Maak in PGadmin een nieuwe user met als username "mainuser" en als password 'safepassword'.
2. Maak in PGadmin een nieuwe database met als naam: "database"
3. Zet als "Owner" de net nieuw aangemaakte "mainuser"

De database zou nu moeten zijn opgezet.

### Npm installeren
Npm staat voor Node Package Manager. Dit programma is nodig om de frontend te laten werken. 

Via deze link kan npm worden gedownload: https://nodejs.org/en/download/

Volg de download instructies in het gedownloade programma en npm zou daarna geinstalleerd moeten zijn.

### Uitleg Documentatie
in het mapje 'Project docs' staan alle documenten voor het project. In deze documenten staat het 
design van het project. Ook staat alle communicatie met client, de uren verdeling en de scrum logs erin.

Hieronder staat een lijst met documenten en wat het document inhoudt.

- Acceptance tests
  - Hierin staan de acceptance tests van het project
- Time_registration.xlsx
  - Hierin staat de tijdsregistratie
- AVGBeveiliging
  - Hierin staat een korte samenvatting van AVGBeveiliging
- Client communication log 
  - Hierin staat alle communicatie die met de klant is gevoerd
- Code of conduct and definition of done
  - Hierin staan de regels waar we ons aan houden in het project
- Daily scrum log
  - Hierin staat waar iedereen elke week mee bezig is gegaan
- Database design 
  - Hierin staat het design van de database
- Endpoint documentatie
  - Hierin staan de endpoints van de backend gedocumenteerd
- Functional design
  - Hierin staat het functionele design van het programma
- Gitcanary en repo analyse 
  - Hierin staat de analyse van het werk in de gitlab repository
- Requirements 
  - Hierin staan alle opgestelde requirements van het project
- Sprint retrospectives 
  - Hierin staan de retrospectives van elke sprint
- Technical design
  - Hierin staat het technical design
- Website guide
  - Hierin staat hoe je de website kan gebruiken
 
# Tests uitvoeren
Voor het uitvoeren van de tests moet er eerst een token worden gehaald uit
de backend. Hiervoor moeten eerste de token tests gedraaid worden. Als dit
niet gebeurd zullen alle andere tests falen.

Draai de tests in deze volgorde:
1. Zet de run environment op "env"
2. start tokens.http 
3. start getters.http

### HTTPS beveiliging
HTTPS beveiliging kan worden toegepast op het project. Dit zal dan wel door uzelf geconfigureerd moeten worden.
U kunt https configureren in het application.properties. Dit bestand staat in de main van de server map. U zult wel een geldig SSL certificaat hiervoor nodig hebben.  
