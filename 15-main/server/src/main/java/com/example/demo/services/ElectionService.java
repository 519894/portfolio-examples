package com.example.demo.services;

import com.example.demo.model.database.Election;
import com.example.demo.repositories.ElectionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ElectionService {
    private final ElectionRepository repository;

    public ElectionService(ElectionRepository repository) {
        this.repository = repository;
    }

    public boolean electionExists(Long id) {
        return this.repository.existsById(id);
    }

    public List<Election> getAllElections() {
        return (List<Election>) this.repository.findAll();
    }

    public Election getElectionById(Long id) {
        Optional<Election> specificElection = this.repository.findById(id);
        if (specificElection.isEmpty()){
            throw new IllegalArgumentException("Verkiezing kon niet gevonden worden");
        }
        return specificElection.get();
    }

    public Election saveElection(Election election) {
        this.repository.save(election);
        return election;
    }

    public void deleteElection(Election election) {
        this.repository.delete(election);
    }
}
