package com.example.demo.repositories;

import com.example.demo.model.database.ElectableUser;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ElectableUserRepository extends CrudRepository<ElectableUser,Long> {
    /**
     * find all electable users.
     * @return all elactbale users.
     */
    @Override
    List<ElectableUser> findAll();

    /**
     * finds all the electable users based ont the election id.
     * @param id the election id.
     * @return list of electable users.
     */
    List<ElectableUser> findElectableUsersByElectionId(long id);

    /**
     * Checks if a username already exists
     * @param username The username to check if it exists
     * @return true if the username exists
     */
    boolean existsByUsernameAndElectionId(String username, Long electionId);

    /**
     * Finds all electableUsers with the username in the election
     * @param username The username to check if it exists
     * @return List of electable users
     */
    List<ElectableUser> findAllByUsernameAndElectionId(String username, Long electionId);

}
