package com.example.demo.repositories;

import org.springframework.data.jpa.repository.Query;
import com.example.demo.model.database.Voter;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VoterRepository extends CrudRepository<Voter, Long> {
    @Override
    List<Voter> findAll();

    @Query("SELECT voter FROM Voter voter WHERE voter.election.id = ?1 AND voter.user.id = ?2")
    Voter getVoter(long electionId, long userId);

    List<Voter> findVotersByUser_Id(long userId);
}
