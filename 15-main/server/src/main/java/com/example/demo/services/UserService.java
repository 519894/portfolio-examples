package com.example.demo.services;

import com.example.demo.model.database.User;
import com.example.demo.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public boolean userExists(Long id) {
        return this.repository.existsById(id);
    }

    public List<User> getAllUsers() {
        return (List<User>) this.repository.findAll();
    }

    public User getUserById(Long id) {
        Optional<User> specificUser = this.repository.findById(id);

        if(specificUser.isEmpty()){
            return null;
        }

        return specificUser.get();
    }

    public User saveUser(User user) {
        this.repository.save(user);
        return user;
    }
    public void updateUser(User newUserData) {
        this.repository.save(newUserData);
    }

    public boolean deleteUser(Long id) {
        this.repository.delete(getUserById(id));
        return !userExists(id);
    }

    public boolean isEmailAlreadyUsed(String email){
        return repository.existsUserByEmail(email);
    }
}
