package com.example.demo.model.custom;

import com.example.demo.model.database.Election;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class ElectionData {
    private String name;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer numberOfVotes;
    private String description;
    private Boolean canVoteOnSelf;
    private List<String> voters;

    public String getName() {
        return name;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Integer getNumberOfVotes() {
        return numberOfVotes;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getCanVoteOnSelf() {
        return canVoteOnSelf;
    }

    public List<String> getVoters() {
        return voters;
    }

    public Election toElection(){
        Election election = new Election();
        election.setName(name);
        election.setStartDate(startDate);
        election.setEndDate(endDate);
        election.setNumberOfVotes(numberOfVotes);
        election.setDescription(description);
        election.setCanVoteOnSelf(canVoteOnSelf);
        election.setVoters(new LinkedList<>());
        election.setElectableUsers(new LinkedList<>());
        return election;
    }

    /**
     * Updates the voters of an election
     *
     * @throws ResponseStatusException election data was invalid
     * @throws IllegalArgumentException election data was null
     */
    public void validate(){

        if(name == null || name.isEmpty() || name.isBlank()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verkiezing heeft een ongeldige naam");
        }

        if(startDate == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verkiezing heeft geen start datum");
        }

        if(endDate == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verkiezing heeft geen eind datum");
        }

        LocalDate currentDate = LocalDate.now();
        if(startDate.isBefore(currentDate)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verkiezing heeft geen een begin datum na vandaag");
        }
        if(!endDate.isAfter(currentDate)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verkiezing heeft geen eind datum na vandaag");
        }
        if(!endDate.isAfter(startDate)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verkiezing heeft een eind datum voor de start datum");
        }

        if(getNumberOfVotes() < 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "De hoeveelheid stemmen is lager dan 1");
        }

        if(getVoters() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verkiezing heeft geen stemmers");
        }
    }

}
