package com.example.demo.model.database;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.*;

@JsonIdentityInfo(
        scope = ElectableUser.class,
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Entity
@Table(name = "electableUsers", uniqueConstraints={
        @UniqueConstraint(columnNames = {"user_id", "election_id"}),
        @UniqueConstraint(columnNames = {"username", "election_id"})
})
public class ElectableUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    private User user;

    @ManyToOne(optional = false)
    private Election election;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "file_id", referencedColumnName = "id")
    private File file;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String standpoints;

    @Column(nullable = false)
    private String slogan;

    @Column(nullable = false)
    private Integer votes = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Election getElection() {
        return election;
    }

    public void setElection(Election election) {
        this.election = election;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStandpoints() {
        return standpoints;
    }

    public void setStandpoints(String standpoints) {
        this.standpoints = standpoints;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static void validateElectionData(ElectableUser electableUser){
        if(electableUser == null){
            throw new IllegalArgumentException("Electable data cannot be null");
        }

        String description = electableUser.getDescription();
        if(description == null || description.isEmpty() || description.isBlank()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verkiesbare heeft geen geldige beschrijving.");
        }

        String slogan = electableUser.getSlogan();
        if(slogan == null || slogan.isEmpty() || slogan.isBlank()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verkiesbare heeft geen geldige slogan");
        }

        String standpoints = electableUser.getStandpoints();
        if(standpoints == null || standpoints.isEmpty() || standpoints.isBlank()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "verkiesbare heeft geen geldige standpunten");
        }
    }

    public void validate(){
        validateElectionData(this);
    }

}
