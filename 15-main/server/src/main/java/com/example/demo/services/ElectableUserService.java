package com.example.demo.services;

import com.example.demo.model.database.ElectableUser;
import com.example.demo.repositories.ElectableUserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class ElectableUserService {
    private final ElectableUserRepository repository;

    public ElectableUserService(ElectableUserRepository repository) {
        this.repository = repository;
    }

    public ElectableUser save(ElectableUser electableUser) {
        return repository.save(electableUser);
    }

    public boolean userExists(Long id) {
        return this.repository.existsById(id);
    }

    public boolean deleteElectableUser(Long id) {
        Optional<ElectableUser> electableUser = repository.findById(id);

        if(!electableUser.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Kandidaat kon niet gevonden worden");
        }

        this.repository.delete(electableUser.get());
        return !userExists(id);
    }
    /**
     * Tries to delete an electable user with the given id.
     *
     * @param id the id of the deleted user to search for.
     * @return if deleting the electable was successful.
     */
    public boolean deleteElectableUsers(Long id) {
        if(getElectableUsersById(id).isPresent()){
            this.repository.delete(getElectableUsersById(id).get());
        }
        return !userExists(id);
    }

    /**
     * Tries to get an electable user with the given id
     *
     * @param id the id to search for
     * @return the found electable user
     */
    public Optional<ElectableUser> getElectableUsersById(Long id) {
        return this.repository.findById(id);
    }

    public void updateUser(ElectableUser newElectableData) {
        this.repository.save(newElectableData);
    }

    public boolean userNameExists(String username, Long electionId) {
        return repository.existsByUsernameAndElectionId(username, electionId);
    }

    public List<ElectableUser> findAllElectableUsers(String username, Long electionId) {
        return repository.findAllByUsernameAndElectionId(username, electionId);
    }
}
