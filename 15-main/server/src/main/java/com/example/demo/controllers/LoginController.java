package com.example.demo.controllers;

import com.example.demo.model.custom.HashedPasswordAndSalt;
import com.example.demo.model.custom.LoginCredentials;
import com.example.demo.model.database.User;
import com.example.demo.services.UserService;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/login")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LoginController {

    public static final int MIN_PASSWORD_LENGTH = 7;
    public static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }


    /**
     * Allows the user to log in and get a token.
     *
     * @param credentials the login credentials containing an email and a password
     * @return A token containing userid and role in the payload.
     */
    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public String login(@RequestBody LoginCredentials credentials) {
        //get email and password from credentials
        String email = credentials.getEmail();
        String password = credentials.getPassword();

        //check if email and password are filled in
        if (email.length() == 0 || !UserController.validateEmail(email)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ongeldige email");
        if (password.length() == 0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "password must not be blank");


        for (User user : userService.getAllUsers()) {

            //check if there is an email matching the given email
            String userEmail = user.getEmail();

            if (userEmail.equals(email)) {

                //check passwords if they match and if the user has been initialized
                if (!user.getSaltedPasswordHash().isEmpty() && BCrypt.checkpw(password, user.getSaltedPasswordHash())) {

                    // generates and returns a token
                    return generateToken(user);
                }
            }
        }

        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "incorrect login credentials");
    }

    /**
     * Parses a jwt
     * @return The parsed token or null if the token was invalid
     */
    public static Claims parseJWT(String token){
        if(token == null){
            return null;
        }

        // Tries to parse the token
        try{
            Jws<Claims> jwt = Jwts.parserBuilder()
                    .setSigningKey(LoginController.key)
                    .build()
                    .parseClaimsJws(token);

            Claims body = jwt.getBody();

            // Checks if the body contains the expected values
            if(!body.containsKey("userId") || !body.containsKey("role")){
                return null;
            }

            // Returns the parsed token
            return body;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * Generates a jwt
     * @param user the user for which a token needs to be generated
     * @return The generated jwt
     * @throws IllegalArgumentException user was null
     */
    public static String generateToken(User user){
        if(user == null){
            throw new IllegalArgumentException("User cannot be null");
        }

        //create a payload string containing userId and role in json format
        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", user.getId().toString());
        payload.put("role", user.getRole());

        //in case passwords match, give user a token
        JwtBuilder builder = Jwts.builder()
                .addClaims(payload)
                .signWith(key);

        //return the token
        return String.format("{\"token\":\"%s\"}", builder.compact());
    }

    /**
     * Hashes a password with a randomly generated salt
     * @return HashedPasswordAndSalt the hashed password and the salt used for it
     * @throws IllegalArgumentException User is null
     * @throws IllegalArgumentException Password is null, empty or blank
     */
    public static HashedPasswordAndSalt hashPassword(User user, String password){
        if(user == null){
            throw new IllegalArgumentException("User cannot be null");
        }
        if(password == null){
            throw new IllegalArgumentException("Password cannot be null");
        }
        if(password.isEmpty() || password.isBlank()){
            throw new IllegalArgumentException("Password cannot be blank or empty");
        }

        String salt = BCrypt.gensalt();

        return new HashedPasswordAndSalt(BCrypt.hashpw(password, salt), salt);
    }

    public static Claims checkLoggedIn(String token){
        Claims parsedToken = LoginController.parseJWT(token);

        // Checks if there is a user token or external token
        if(parsedToken == null){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Gebruiker heeft geen toegang");
        }

        return parsedToken;
    }

    public static Claims checkLoggedInAsAdmin(String token){
        Claims parsedToken = LoginController.parseJWT(token);

        // Checks if the token was valid
        if (parsedToken == null || !parsedToken.get("role", String.class).equals("admin")) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Incorrecte token");
        }

        return parsedToken;
    }
}
