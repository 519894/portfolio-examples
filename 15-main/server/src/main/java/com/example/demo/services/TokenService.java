package com.example.demo.services;

import com.example.demo.model.database.Token;
import com.example.demo.repositories.TokenRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class TokenService {
    private final TokenRepository repository;

    public TokenService(TokenRepository repository) {
        this.repository = repository;
    }

    public List<Token> getTokensByInviteToken(String token){
        return this.repository.findTokensByInviteToken(token);
    }

    public List<Token> getTokensByInviteTokenAndUserId(String token, long userId){
        return this.repository.findTokensByInviteTokenAndUser_Id(token, userId);
    }

    public List<Token> getAllTokens() {
        return (List<Token>) this.repository.findAll();
    }

    public Optional<Token> getTokenById(Long id) {
        return this.repository.findById(id);
    }

    public boolean tokenExists(Long id) {
        return this.repository.existsById(id);
    }

    public Token saveToken(Token token) {
        this.repository.save(token);
        return token;
    }

    public boolean deleteToken(Token token){
        if(token != null){
            repository.delete(token);
        }

        return true;
    }

    public void deleteAllByValidUntilBefore(LocalDate date){
        repository.removeAllByValidUntilBefore(date);
    }
}
