package com.example.demo.model.custom;

public class HashedPasswordAndSalt {
    private final String hashedPassword, salt;

    public HashedPasswordAndSalt(String hashedPassword, String salt){
        if(hashedPassword == null){
            throw new IllegalArgumentException("Hashed password cannot be null");
        }
        if(hashedPassword.isEmpty() || hashedPassword.isBlank()){
            throw new IllegalArgumentException("Hashed password cannot be empty or blank");
        }
        if(salt == null){
            throw new IllegalArgumentException("Salt cannot be null");
        }
        if(salt.isEmpty() || salt.isBlank()){
            throw new IllegalArgumentException("Salt cannot be empty or blank");
        }

        this.hashedPassword = hashedPassword;
        this.salt = salt;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public String getSalt() {
        return salt;
    }
}
