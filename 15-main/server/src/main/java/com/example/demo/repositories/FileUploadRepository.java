package com.example.demo.repositories;

import com.example.demo.model.database.File;
import org.springframework.data.repository.CrudRepository;

public interface FileUploadRepository extends CrudRepository<File, Long> {
}
