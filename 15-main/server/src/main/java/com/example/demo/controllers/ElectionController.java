package com.example.demo.controllers;

import com.example.demo.model.custom.ElectionData;
import com.example.demo.model.database.*;
import com.example.demo.services.*;
import io.jsonwebtoken.Claims;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping ("/elections")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ElectionController {
    private final ElectionService electionService;
    private final EmailService emailService;
    private final ElectableUserService electableUserService;
    private final VoterService voterService;
    private final UserService userService;
    private final TokenService tokenService;

    // Queries require the @RequestParam parameter (if optional use: required = false)
    // Path parameters are notated like this {pathparameter}
    // @GetMapping, @PostMapping, @PutMapping @DeleteMapping for different types of calls
    public ElectionController(EmailService emailService, ElectionService electionService, ElectableUserService electableUserService, VoterService voterService, UserService userService, TokenService tokenService) {
        this.emailService = emailService;
        this.electionService = electionService;
        this.electableUserService = electableUserService;
        this.voterService = voterService;
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @PostMapping("/{id}/notify")
    public void sendReminder(@RequestHeader(HttpHeaders.AUTHORIZATION) String token, @PathVariable Long id, @RequestHeader("Port") String port){
        // Validates the token
        Claims parsedToken = LoginController.checkLoggedInAsAdmin(token);

        Election election = electionService.getElectionById(id);

        if (election == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Verkiezing met id " + id + " bestaat niet");
        }

        emailService.sendReminder(election, port);
    }
    /**
     * returns all elections
     * @author Erik Markvoort
     * @return all the elections from the election table.
     */
    @GetMapping("/")
    @ResponseBody
    public List<Election> getElections(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        // Validates the token
        Claims parsedToken = LoginController.checkLoggedIn(token);

        List<Election> electionsForUser = new LinkedList<>();

        List<Election> elections = electionService.getAllElections();

        // Checks if the user is an admin
        if(parsedToken.get("role", String.class).equals("admin")){
            // Returns all elections
            return elections;
        }

        // Filters elections based on where the user is assigned as voter
        for(Election election : elections){

            // Checks if the election hasn't ended yet
            if(election.getEndDate().isAfter(LocalDate.now())){

                // Checks if the user is a voter
                for(Voter voter : election.getVoters()){
                    if(voter.getUser().getId() == Long.parseLong(parsedToken.get("userId", String.class))){
                        electionsForUser.add(election);
                        break;
                    }
                }
            }
        }

        // Returns the filtered elections

        return electionsForUser;
    }

    /**
     * Tries to get an election with the given id
     *
     * @param id the id to search for
     * @return the found election
     * @throws ResponseStatusException election with the given id could not be found
     */
    @GetMapping("/{id}")
    public Election getElectionById(@PathVariable Long id, @RequestHeader(value = HttpHeaders.AUTHORIZATION) String token) {
        LoginController.checkLoggedIn(token);

        Election election = electionService.getElectionById(id);

        if (election == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Verkiezing met id " + id + " kon niet gevonden worden");
        }
        return election;
    }

    /**
     * Gets all electable users from an election
     *
     * @param id the id to search for
     * @return a list of the electable users of this election
     * @throws ResponseStatusException election with the given id could not be found
     */
    @GetMapping("/electionId/{id}/electableUsers")
    @ResponseBody
    public List<ElectableUser> getAllElectableUsersOfElectionWithId(@PathVariable Long id, @RequestHeader(value = HttpHeaders.AUTHORIZATION) String token){
        LoginController.checkLoggedIn(token);
        Election targetElection = getElectionById(id, token);
        return targetElection.getElectableUsers();
    }

    /**
     *
     * @param electionId The election that will be voted in
     * @param token The token of the user that wants to vote
     * @param candidates The ids of the candidates that have been voted on
     */
    @Transactional
    @PostMapping("/{electionId}/votes")
    public void setVotes(@PathVariable long electionId, @RequestHeader(HttpHeaders.AUTHORIZATION) String token, @RequestBody Set<Long> candidates) {

        // Parses the token
        Claims parsedToken = LoginController.checkLoggedIn(token);

        // Gets the target election
        Election election = electionService.getElectionById(electionId);

        if(election == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Verkiezing kon niet gevonden worden");
        }

        // Checks if the user exists
        long userId = Long.parseLong(parsedToken.get("userId", String.class));
        User user = userService.getUserById(userId);

        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gebruiker bestaat niet");
        }

        // Checks if the voter was found or if the voter had already voted
        Voter voter = voterService.getVoter(electionId, userId);

        if (voter == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Stemmer kon niet gevonden worden");
        } else if (voter.isHasVoted()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Stemmer heeft al gestemd");
        }

        // Uploads that the voter now has voted
        voter.setHasVoted(true);
        voterService.saveVoter(voter);

        // Applies votes for all the candidates the user has voted for
        for (Long candidateId : candidates) {
            // Tries to find the candidate
            Optional<ElectableUser> optional = electableUserService.getElectableUsersById(candidateId);
            if (optional.isEmpty()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Kandidaad met id:" + candidateId + " bestaat niet");
            }

            ElectableUser electableUser = optional.get();

            // Checks if the vote is valid
            if(!election.getCanVoteOnSelf() && userId == electableUser.getUser().getId()){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Gebruiker kan niet op zichzelf stemmen");
            }

            // Checks if the candidate is for this election
            if (!electableUser.getElection().getId().equals(electionId)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Kandidaad met id:" + candidateId + " is kandidaad in een andere verkiezing");
            }

            // Saves the new votes
            electableUser.setVotes(electableUser.getVotes() + 1);
            electableUserService.save(electableUser);
        }
    }


    /**
     * Tries to post a new election
     *
     * @param electionData the election that will be posted
     * @return the posted election
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Election createElection(@RequestBody ElectionData electionData, @RequestHeader(HttpHeaders.AUTHORIZATION) String token, @RequestHeader("Port") String port) {
        LoginController.checkLoggedInAsAdmin(token);

        // Validates the election data
        electionData.validate();

        // Creates the new base election
        Election newElection = electionData.toElection();
        // Updates the voters of the election
        updateElectionVoters(newElection, electionData.getVoters(), port);

        electionService.saveElection(newElection);
        return electionService.getElectionById(newElection.getId());
    }

    /**
     * Updates the voters of an election
     *
     * @param election the election that will be updated
     * @param voters the emails of the voters
     * @throws IllegalArgumentException election or voters was null
     */
    private void updateElectionVoters(Election election, List<String> voters, String port){
        if(election == null){
            throw new IllegalArgumentException("Election cannot be null");
        }
        if(voters == null){
            throw new IllegalArgumentException("Voters cannot be null");
        }

        // Gets the emails of all the users in the database
        List<String> currentUserEmails = userService.getAllUsers().stream().map(user -> user.getEmail()).collect(Collectors.toList());

        // Gets the email of the current voters of this election
        List<String> currentVoterEmails = election.getVoters().stream().map(voter -> voter.getUser().getEmail()).collect(Collectors.toList());
        // Gets the emails of the voters that still need to be added to the election
        List<String> votersToAdd = voters.stream().filter(voter -> !currentVoterEmails.contains(voter)).collect(Collectors.toList());

        // Gets the voters that need to be removed from the election
        List<Voter> votersToRemove = election.getVoters().stream().filter(voter -> !voters.contains(voter.getUser().getEmail())).collect(Collectors.toList());

        // Gets the electable users that need to be removed from the election
        List<ElectableUser> electableUsersToRemove = election.getElectableUsers().stream().filter(electableUser -> !voters.contains(electableUser.getUser().getEmail())).collect(Collectors.toList());

        // Removes electable users from the election if they were removed as voter
        for(ElectableUser electableUser : electableUsersToRemove){
            election.getElectableUsers().remove(electableUser);
        }

        // Removes voters from the election if they were removed as voter
        for(Voter voter : votersToRemove){
            election.getVoters().remove(voter);
        }

        // Adds the new voters to the election
        for(String voterEmail : votersToAdd){
            if(!UserController.validateEmail(voterEmail)){
                continue;
            }

            User targetUser = null;
            // Checks if the email already exists as user
            if(!currentUserEmails.contains(voterEmail)){

                // Creates a new user
                User newUser = new User();
                newUser.setEmail(voterEmail);
                newUser.setRole("user");
                userService.saveUser(newUser);

                targetUser = newUser;

                // Makes a new token for the user
                Token token = new Token();
                token.setUser(targetUser);
                token.setInviteToken(Token.generateToken());
                tokenService.saveToken(token);

                // Sends the user an email with the token
                emailService.sendInvite(targetUser, token, port);
            }else{
                // Gets the user that matches with the email
                for(User user : userService.getAllUsers()){
                    if(user.getEmail().equals(voterEmail)){
                        targetUser = user;
                        break;
                    }
                }
            }

            if(targetUser == null){
                throw new IllegalStateException("Target user cannot be null");
            }

            // Creates a new voter and adds it to the election
            Voter newVoter = new Voter();
            newVoter.setElection(election);
            newVoter.setUser(targetUser);
            //voterService.saveVoter(newVoter);
            election.getVoters().add(newVoter);
        }
    }

    /**
     * deletes an election based on the election id.
     * @param id election id.
     * @param token user token that contains the role of the user.
     * @returns response with the message that it successfully deleted an election.
     */
    @DeleteMapping("/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteElection(@PathVariable Long id, @RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        LoginController.checkLoggedInAsAdmin(token);

        // Find the election with the specified ID
        Election election = electionService.getElectionById(id);

        if (election == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Verkiezing met id " + id + " kon niet gevonden worden");
        }

        electionService.deleteElection(election);
    }


    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Election updateElection(@PathVariable Long id, @RequestBody ElectionData electionData, @RequestHeader(HttpHeaders.AUTHORIZATION) String token, @RequestHeader("Port") String port ){
        LoginController.checkLoggedInAsAdmin(token);

        electionData.validate();
        if(this.electionService.electionExists(id)){
            //get the election from the backend
            Election electionToUpdate = this.electionService.getElectionById(id);

            //create an election from the electiondata
            Election election = electionData.toElection();
            election.setVoters(electionToUpdate.getVoters());
            election.setElectableUsers(electionToUpdate.getElectableUsers());

            //set the data
            election.setId(electionToUpdate.getId());

            //set the voters
            updateElectionVoters(election,electionData.getVoters(), port);
            return this.electionService.saveElection(election);
        }else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Verkiezing kon niet gevonden worden");
        }
    }

    /**
     * Tries to post a new election
     *
     * @return the posted election
     */
    @PostMapping("/{electionId}/electableUsers")
    @ResponseStatus(HttpStatus.CREATED)
    public ElectableUser addElectableUserToElection(@RequestBody ElectableUser electableUser, @RequestHeader(value = HttpHeaders.AUTHORIZATION) String token) {
        LoginController.checkLoggedIn(token);

        electableUser.validate();
        if (electableUserService.userNameExists(electableUser.getUsername(), electableUser.getElection().getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Gebruiker met naam " + electableUser.getUsername() + " bestaat al");
        }
        return electableUserService.save(electableUser);
    }

    @PutMapping(value = "/{electionId}/electableUsers/{electableId}", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ElectableUser putElectableUser(@PathVariable Long electableId, @RequestBody ElectableUser newElectableData, @RequestHeader(HttpHeaders.AUTHORIZATION) String token){
        // Validates the token
        Claims parsedToken = LoginController.checkLoggedIn(token);

        Optional<ElectableUser> electableUser = electableUserService.getElectableUsersById(electableId);

        //validates if the user is also the elected user.
        if(!electableUser.isEmpty()){
            if(!parsedToken.get("userId", String.class).equals(electableUser.get().getUser().getId().toString())){
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
        }

        // Checks if the electable user was found
        if(electableUser.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Verkiesbare met ID %d kon niet gevonden worden", electableId));
        }

        ElectableUser.validateElectionData(newElectableData);

        ElectableUser foundElectable = electableUser.get();
        newElectableData.setId(foundElectable.getId());

        foundElectable.validate();

        if (electableUserService.findAllElectableUsers(newElectableData.getUsername(), newElectableData.getElection().getId()).stream().anyMatch(user -> user.getId() != newElectableData.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Gebruiker met naam " + newElectableData.getUsername() + " bestaat al");
        }

        electableUserService.updateUser(newElectableData);

        return newElectableData;
    }
}
