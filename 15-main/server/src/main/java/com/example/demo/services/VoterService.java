package com.example.demo.services;

import com.example.demo.model.database.Voter;
import com.example.demo.repositories.VoterRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class VoterService {
    private final VoterRepository repository;

    public VoterService(VoterRepository repository) {
        this.repository = repository;
    }

    public boolean voterExists(Long id) {
        return this.repository.existsById(id);
    }

    public List<Voter> getAllVoters() {
        return (List<Voter>) this.repository.findAll();
    }

    public boolean deleteVoter(Long id) {
        Optional<Voter> voter = repository.findById(id);

        if(!voter.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Stemmer kon niet gevonden worden");
        }

        this.repository.delete(voter.get());
        return !voterExists(id);
    }

    public Voter saveVoter(Voter voter) {
        this.repository.save(voter);
        return voter;
    }

    /**
     * gets a voter by election and userId
     * @param electionid id of the election
     * @param userId id of the user
     * @return the voter found in the query
     */
    public Voter getVoter(long electionid, long userId) {
        return repository.getVoter(electionid, userId);
    }

    public List<Voter> getVotersByUserId(long userId){
        return repository.findVotersByUser_Id(userId);
    }
}
