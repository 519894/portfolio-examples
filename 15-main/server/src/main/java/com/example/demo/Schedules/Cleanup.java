package com.example.demo.Schedules;

import com.example.demo.services.TokenService;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Component
@EnableScheduling
public class Cleanup {
    private final static int checkTokenCleanupDelayInHours = 24;
    private final TokenService tokenService;

    public Cleanup(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Transactional
    @Scheduled(fixedDelay = checkTokenCleanupDelayInHours * 60 * 60 * 1000)
    public void cleanupTokens(){
        // Removes old tokens
        tokenService.deleteAllByValidUntilBefore(LocalDate.now());
    }
}
