package com.example.demo.model.database;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Locale;
import java.util.Random;

@JsonIdentityInfo(
        scope = Token.class,
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Entity
@Table(name = "tokens", uniqueConstraints={
        @UniqueConstraint(columnNames = {"user_id"})
})
public class Token {
    private static int REMOVE_AFTER_DAYS = 7;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String inviteToken;

    @Column(nullable = false)
    private LocalDate validUntil = LocalDate.now().plusDays(REMOVE_AFTER_DAYS - 1);

    @JsonIgnore
    @ManyToOne(optional = false)
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInviteToken() {
        return inviteToken;
    }

    public void setInviteToken(String inviteToken) {
        this.inviteToken = inviteToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static int getRemoveAfterDays() {
        return REMOVE_AFTER_DAYS;
    }

    public static void setRemoveAfterDays(int removeAfterDays) {
        REMOVE_AFTER_DAYS = removeAfterDays;
    }

    public LocalDate getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(LocalDate validUntil) {
        this.validUntil = validUntil;
    }

    public static String generateToken(){
        int minTokenLength = 15;
        int maxTokenLength = 20;
        String letters = "abcdefhijklmnop";
        String numbers = "0123456789";

        char[] validCharacters = String.format("%s%s%s", letters, letters.toUpperCase(Locale.ROOT), numbers).toCharArray();

        if(validCharacters.length < 2){
            throw new IllegalStateException("There are not enough unique characters for a random string");
        }

        Random random = new Random();
        int[] selectedCharacters = random.ints(0, validCharacters.length)
                .limit(random.nextInt(minTokenLength, maxTokenLength + 1))
                        .toArray();

        StringBuilder token = new StringBuilder();

        for(int characterIndex : selectedCharacters){
            token.append(validCharacters[characterIndex]);
        }

        return token.toString();
    }
}
