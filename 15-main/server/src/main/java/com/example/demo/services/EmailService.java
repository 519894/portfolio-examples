package com.example.demo.services;

import com.example.demo.model.database.Election;
import com.example.demo.model.database.Token;
import com.example.demo.model.database.User;
import com.example.demo.model.database.Voter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    private void sendSimpleMessage(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("VotingCarthago@gmail.com");
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);
            emailSender.send(message);
        } catch (MailException e) {
            e.printStackTrace();
        }
    }

    public void sendReminder(Election election, String port) {
        for (Voter voter : election.getVoters()) {
            if (!voter.isHasVoted()) {
                sendSimpleMessage(voter.getUser().getEmail(), "Vergeet niet te stemmen!",
                        "U bent uitgenodigd om te stemmen in een verkiezing.\n" +
                                "Deze verkiezing loopt tot " + election.getEndDate() +
                                "\nU kunt stemmen op de volgende website: http://localhost:" + port);
            }
        }
    }

    public void sendInvite(User user, Token token, String port) {
        if(user == null){
            throw new IllegalArgumentException("User cannot be null");
        }
        if(token == null){
            throw new IllegalArgumentException("Token cannot be null");
        }

        String subject = "Maak een account aan om te stemmen";
        String text = String.format("Maak een account aan via de volgende link: http://localhost:%s/users/%d/change-password/%s", port, user.getId(), token.getInviteToken());

        sendSimpleMessage(user.getEmail(), subject, text);
    }

}
