package com.example.demo.repositories;

import com.example.demo.model.database.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    @Override
    List<User> findAll();

    boolean existsUserByEmail(String email);
}
