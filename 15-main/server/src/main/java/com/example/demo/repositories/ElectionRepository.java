package com.example.demo.repositories;

import com.example.demo.model.database.Election;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ElectionRepository extends CrudRepository<Election, Long> {
    @Override
    List<Election> findAll();

    Election getElectionById(Long id);
}
