package com.example.demo.controllers;

import com.example.demo.model.database.ElectableUser;
import io.jsonwebtoken.Claims;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import com.example.demo.services.ElectableUserService;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/electableUsers")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ElectableUserController {
    private final ElectableUserService electableUserService;

    public ElectableUserController(ElectableUserService userService) {
        this.electableUserService = userService;
    }

    // Queries require the @RequestParam parameter (if optional use: required = false)
    // Path parameters are notated like this {pathparameter}
    // @GetMapping, @PostMapping, @PutMapping @DeleteMapping for different types of calls

     /**
     * Tries to get an electable user with the given id
     *
     * @param id the id to search for
     * @return the found electable user
     * @throws ResponseStatusException electable user with the given id could not be found
     */
    @GetMapping("/{id}")
    public ElectableUser getElectableUserById(@PathVariable Long id, @RequestHeader(value = HttpHeaders.AUTHORIZATION) String token){
        Claims parsedToken = LoginController.checkLoggedIn(token);

        // Gets the electable user with the given id
        Optional<ElectableUser> electableUser = electableUserService.getElectableUsersById(id);

        // Checks if the electable user was found
        if(electableUser.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Verkiesbare met ID %d kon niet gevonden worden", id));
        }

        return electableUser.get();
    }

    /**
     * Tries to post a new electable user
     *
     * @param e the electable user that will be posted
     * @return the posted electable user
     * @throws ResponseStatusException do not provide (new) id
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ElectableUser createElectableUser(@RequestBody ElectableUser e, @RequestHeader(value = HttpHeaders.AUTHORIZATION) String token) {
        Claims parsedToken = LoginController.checkLoggedIn(token);

        e.validate();
        if (electableUserService.userNameExists(e.getUsername(), e.getElection().getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Gebruiker met naam " + e.getUsername() + " bestaat al");
        }
        return electableUserService.save(e);
    }

    /**
     * Tries to delete an electable user with the given id.
     *
     * @param id the id of the deleted user to search for.
     * @return if deleting the electable was successful.
     * @throws ResponseStatusException electable user with the given id could not be found or not deleted.
     */
    @DeleteMapping("/{id}")
    public Boolean deleteElectableUserById(@PathVariable Long id, @RequestHeader(HttpHeaders.AUTHORIZATION) String token){
        // Validates the token
        Claims parsedToken = LoginController.checkLoggedIn(token);

        Optional<ElectableUser> electableUser = electableUserService.getElectableUsersById(id);

        //validates if the user is also the elected user.
        if(!electableUser.isEmpty()){
            if(parsedToken == null || !parsedToken.get("userId", String.class).equals(electableUser.get().getUser().getId().toString())){
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
        }

        // Checks if the electable user was found
        if(electableUser.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Verkiesbare met ID %d kon niet gevonden worden", id));
        }

        //deletes the selected electable user
        Boolean deleted = electableUserService.deleteElectableUsers(id);

        //checks if the delete was successful
        if(!deleted){
            throw new ResponseStatusException(HttpStatus.NOT_MODIFIED, String.format("Verkiesbare met ID %d kon niet verwijderd worden", id));
        }

        return deleted;
    }
}
