package com.example.demo.services;

import com.example.demo.model.database.File;
import com.example.demo.repositories.FileUploadRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Service
public class FileUploadService {

    private final FileUploadRepository repository;

    public FileUploadService(FileUploadRepository repository) {
        this.repository = repository;
    }

    public Optional<File> getFileById(Long id) {
        return repository.findById(id);
    }

    public File store(MultipartFile multipartFile) throws IOException {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        File file = new File(fileName, multipartFile.getContentType(), multipartFile.getBytes());

        return repository.save(file);
    }
}

