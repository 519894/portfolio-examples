package com.example.demo.repositories;

import com.example.demo.model.database.Token;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface TokenRepository extends CrudRepository<Token,Long> {
    @Override
    List<Token> findAll();

    List<Token> findTokensByInviteToken(String inviteToken);

    List<Token> findTokensByInviteTokenAndUser_Id(String inviteToken, long id);

    void removeAllByValidUntilBefore(LocalDate date);
}
