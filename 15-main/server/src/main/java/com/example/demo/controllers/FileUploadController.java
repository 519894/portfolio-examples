package com.example.demo.controllers;

import com.example.demo.model.database.ElectableUser;
import com.example.demo.model.database.File;
import com.example.demo.services.ElectableUserService;
import com.example.demo.services.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/files")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FileUploadController {

    private final FileUploadService fileUploadService;
    private final ElectableUserService electableUserService;

    @Autowired
    public FileUploadController(FileUploadService fileUploadService, ElectableUserService electableUserService) {
        this.fileUploadService = fileUploadService;
        this.electableUserService = electableUserService;
    }

    /**
     * Tries to get a file with the given id
     *
     * @param id the id to search for
     * @return the found file data in a ResponseEntity
     * @throws ResponseStatusException file with the given id could not be found
     */
    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable Long id) {
        // Gets the file as an optional with the given id
        Optional<File> fileOptional = fileUploadService.getFileById(id);
        // Checks if the file was found
        if(fileOptional.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Bestand met ID %d kon niet gevonden worden", id));
        }
        File file = fileOptional.get();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                .body(file.getData());
    }

    /**
     * Tries to post a new file
     *
     * @param multipartFile the file that will be posted
     * @param id the id of the electable user
     * @return the status of the upload in a ResponseEntity
     */
    @PostMapping()
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile multipartFile, @RequestParam("user_id") Long id) {
        String message;
        try {
            // Gets the electable user with the given id
            Optional<ElectableUser> electableUser = electableUserService.getElectableUsersById(id);
            // Checks if the electable user was found
            if (electableUser.isEmpty()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Verkiesbare met ID %d kon niet gevonden worden", id));
            }
            ElectableUser user = electableUser.get();
            // Stores the file in the database
            File file = fileUploadService.store(multipartFile);
            message = "U heeft " + file.getName() + " geüpload";
            // Add the file to the electable user
            user.setFile(file);
            // Update the electable user
            electableUserService.updateUser(user);
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "Kon het bestand " + multipartFile.getOriginalFilename() + " niet uploaden!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

}