package com.example.demo.controllers;

import com.example.demo.model.custom.HashedPasswordAndSalt;
import com.example.demo.model.database.User;
import com.example.demo.model.database.Token;
import com.example.demo.services.TokenService;
import com.example.demo.services.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.regex.Pattern;

@RestController
@RequestMapping ("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    private final UserService userService;
    private final TokenService tokenService;

    public UserController(UserService userService, TokenService tokenService) {
        this.userService = userService;
        this.tokenService = tokenService;
    }

    /**
     * @return all the users from the user table.
     */
    @GetMapping("/")
    @ResponseBody
    public List<User> getUsers(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token) {
        LoginController.checkLoggedInAsAdmin(token);

        return userService.getAllUsers();
    }

    /**
     * @return the user with the given id.
     */
    @GetMapping("/{id}")
    @ResponseBody
    public User getUserById(@PathVariable Long id, @RequestHeader(value = HttpHeaders.AUTHORIZATION) String token) {

        // Checks if the user is an admin
        LoginController.checkLoggedInAsAdmin(token);

        User userToReturn =  userService.getUserById(id);

        if (userToReturn == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return userToReturn;
    }


    /**
     * Updates a user with a specific id
     * @param userId the id of the user to change
     * @param emailOrPassword the new email or password
     * @param token the token of the user
     * @param authenticationTokenString the authentication token of the user
     *
     * @return a new token with the updated user data
     *
     * @throws ResponseStatusException User is unauthorized
     * @throws ResponseStatusException User could not be found
     */
    @PutMapping(value = "/{userId}", consumes = "application/json")
    @ResponseBody
    public String UpdateUserById(@PathVariable Long userId, @RequestBody String emailOrPassword, @RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String token, @RequestHeader(value = "Authentication-Token", required = false) String authenticationTokenString) {
        // Checks if there is a user token or external token
        if(token == null && authenticationTokenString == null){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Gebruiker heeft geen toegang");
        }

        User targetUser =  userService.getUserById(userId);

        if (targetUser == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Gebruiker kon niet gevonden worden");
        }

        // Checks if the user wants to change the email or password
        if(token != null){
            // Updates the user email
            updateUserEmail(targetUser, token, emailOrPassword);
        }else{
            // Updates the user password
            updateUserPassword(targetUser, authenticationTokenString, emailOrPassword);
        }

        // Updates the user in the database
        userService.updateUser(targetUser);

        return LoginController.generateToken(targetUser);
    }

    /**
     * Updates a users email
     * @param targetUser the user to change
     * @param token the token of the user
     * @param email the new email of the user
     *
     * @throws ResponseStatusException User is unauthorized
     * @throws ResponseStatusException The email is invalid
     */
    private void updateUserEmail(User targetUser, String token, String email){
        // Checks if the user is an admin
        LoginController.checkLoggedInAsAdmin(token);

        // Validates the new email
        if(!validateEmail(email)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Ongeldige email");
        }

        if(userService.isEmailAlreadyUsed(email)){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Email adres is al in gebruik");
        }

        // Sets the email of the user
        targetUser.setEmail(email);
    }

    /**
     * Updates a users password
     * @param targetUser the user to change
     * @param token the token of the user
     * @param password the new password of the user
     *
     * @throws ResponseStatusException User is unauthorized
     * @throws ResponseStatusException The password is invalid
     */
    private void updateUserPassword(User targetUser, String token, String password){
        // Gets the invite token
        List<Token> authenticationTokens = tokenService.getTokensByInviteTokenAndUserId(token, targetUser.getId());

        // Checks if the token exists
        if(authenticationTokens.size() == 0){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Gebruiker heeft geen toegang");
        }

        Token tokenForUser = authenticationTokens.get(0);

        // Updates the user password to the new password
        if(password.length() < LoginController.MIN_PASSWORD_LENGTH){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Wachtwoord moet minimaal %d karakters bevatten", LoginController.MIN_PASSWORD_LENGTH));
        }

        if(password.isBlank()){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Wachtwoord mag niet leeg zijn");
        }

        // Generates a new hashed password and a random salt
        HashedPasswordAndSalt newPasswordAndSalt = LoginController.hashPassword(targetUser, password);
        targetUser.setSaltedPasswordHash(newPasswordAndSalt.getHashedPassword());
        targetUser.setSalt(newPasswordAndSalt.getSalt());

        // Removes the token
        tokenService.deleteToken(tokenForUser);
    }

    /**
     * Deletes a user from the database
     * @throws ResponseStatusException token is invalid or no user was found
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id, @RequestHeader(HttpHeaders.AUTHORIZATION) String token){
        // Validates the token and role
        LoginController.checkLoggedInAsAdmin(token);

        // Checks if the user exists
        if(!this.userService.userExists(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        // Removes the user from the database
        this.userService.deleteUser(id);
    }

    /**
     * Checks if an email is valid
     * @return boolean if the email was valid
     * @throws IllegalArgumentException email was null
     */
    public static boolean validateEmail(String email){
        if(email == null){
            throw new IllegalArgumentException("Email cannot be null");
        }

        // Regex for checking an email
        String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

        // Checks if the email matches with the regex
        return Pattern.compile(regexPattern)
                .matcher(email)
                .matches();
    }
}
