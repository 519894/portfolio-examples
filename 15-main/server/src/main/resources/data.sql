INSERT INTO elections (name, can_vote_on_self, description, start_date, number_of_votes, end_date)
VALUES ('Aankomende verkiezing 1', false, 'Beschrijving van aankomende verkiezing 1', TO_DATE('17/12/2024', 'DD/MM/YYYY'), 5, TO_DATE('17/12/2025', 'DD/MM/YYYY'));
INSERT INTO elections (name, can_vote_on_self, description, start_date, number_of_votes, end_date)
VALUES ('Aankomende verkiezing 2', true, 'Beschrijving van aankomende verkiezing 2', TO_DATE('17/12/2024', 'DD/MM/YYYY'), 3, TO_DATE('17/12/2025', 'DD/MM/YYYY'));

INSERT INTO elections (name, can_vote_on_self, description, start_date, number_of_votes, end_date)
VALUES ('Begonnen verkiezing 1', false, 'Beschrijving van begonnen verkiezing 1', TO_DATE('17/12/2021', 'DD/MM/YYYY'), 5, TO_DATE('17/12/2025', 'DD/MM/YYYY'));
INSERT INTO elections (name, can_vote_on_self, description, start_date, number_of_votes, end_date)
VALUES ('Begonnen verkiezing 2', true, 'Beschrijving van begonnen verkiezing 2', TO_DATE('17/12/2021', 'DD/MM/YYYY'), 3, TO_DATE('17/12/2025', 'DD/MM/YYYY'));

INSERT INTO elections (name, can_vote_on_self, description, start_date, number_of_votes, end_date)
VALUES ('Afgelopen verkiezing 1', false, 'Beschrijving van afgelopen verkiezing 1', TO_DATE('17/12/2020', 'DD/MM/YYYY'), 5, TO_DATE('17/12/2021', 'DD/MM/YYYY'));
INSERT INTO elections (name, can_vote_on_self, description, start_date, number_of_votes, end_date)
VALUES ('Afgelopen verkiezing 2', true, 'Beschrijving van afgelopen verkiezing 2', TO_DATE('17/12/2020', 'DD/MM/YYYY'), 3, TO_DATE('17/12/2021', 'DD/MM/YYYY'));

INSERT INTO users (email, salt, salted_password_hash, role)
VALUES ('test@gmail.com', '$2a$10$4XV8/mj5z/ZHCigaiukBF.', '$2a$10$Y9XKj1dOP2QCgAC2StDAkO3BhSP0fDGvtK3V8KlXAVUieqJbTZ1Ce', 'user');
INSERT INTO users (email, salt, salted_password_hash, role)
VALUES ('password@gmail.com', '$2a$10$4XV8/mj5z/ZHCigaiukBF.', '$2a$10$5mAubRlXESJJo4oDMMrOPuu87wxBGjyK5Vfwp5p.gAUS74qzqosF6', 'user');
INSERT INTO users (email, salt, salted_password_hash, role)
VALUES ('user@gmail.com', '$2a$10$4XV8/mj5z/ZHCigaiukBF.', '$2a$10$QnLStzauLUFTC/6F64sW3uh.LGceMs6Y8BDVD2WzMuAgvxd/dzXN2', 'user');
INSERT INTO users (email, salt, salted_password_hash, role)
VALUES ('admin@gmail.com', '$2a$10$4XV8/mj5z/ZHCigaiukBF.', '$2a$10$UeMWwiOqvGDIyRF8YvAvAud99K1jez9zfSMfPVAAMlazkvifxlJVa', 'admin');
INSERT INTO users (email, salt, salted_password_hash, role)
VALUES ('token@gmail.com', '$2a$10$4XV8/mj5z/ZHCigaiukBF.', '', 'user');


INSERT INTO tokens (invite_token, user_id, valid_until)
VALUES ('token1', 1, TO_DATE('17/12/2020', 'DD/MM/YYYY'));
INSERT INTO tokens (invite_token, user_id, valid_until)
VALUES ('token2', 2, TO_DATE('17/12/2020', 'DD/MM/YYYY'));

INSERT INTO tokens (invite_token, user_id, valid_until)
VALUES ('token3', 3, TO_DATE('24/01/2023', 'DD/MM/YYYY'));
INSERT INTO tokens (invite_token, user_id, valid_until)
VALUES ('token4', 4, TO_DATE('25/01/2023', 'DD/MM/YYYY'));

INSERT INTO tokens (invite_token, user_id, valid_until)
VALUES ('token5', 5, TO_DATE('17/12/2025', 'DD/MM/YYYY'));


INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 1', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 0, 1, 1);
INSERT INTO electable_users (username, description,  slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 2', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 0, 1, 2);

INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 1', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 0, 2, 3);
INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 2', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 0, 2, 4);

INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 1', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 0, 3, 1);
INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 2', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 0, 3, 2);

INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 1', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 0, 4, 3);
INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 2', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 0, 4, 4);

INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 1', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 0, 5, 1);
INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 2', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 2, 5, 2);

INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 1', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 5, 6, 3);
INSERT INTO electable_users (username, description, slogan, standpoints, votes, election_id, user_id)
VALUES ('Kandidaat 2', 'beschrijving van verkiesbare', 'slogan', 'standpunten', 2, 6, 4);

INSERT INTO voters (has_voted, election_id, user_id)
VALUES(false, 1, 1);
INSERT INTO voters (has_voted, election_id, user_id)
VALUES(true , 1, 2);

INSERT INTO voters (has_voted, election_id, user_id)
VALUES(false, 2, 3);
INSERT INTO voters (has_voted, election_id, user_id)
VALUES(true , 2, 4);

INSERT INTO voters (has_voted, election_id, user_id)
VALUES(false, 3, 1);

INSERT INTO voters (has_voted, election_id, user_id)
VALUES(false, 4, 3);
INSERT INTO voters (has_voted, election_id, user_id)
VALUES(true , 4, 4);

INSERT INTO voters (has_voted, election_id, user_id)
VALUES(false, 5, 1);
INSERT INTO voters (has_voted, election_id, user_id)
VALUES(true , 5, 2);

INSERT INTO voters (has_voted, election_id, user_id)
VALUES(false, 6, 3);
INSERT INTO voters (has_voted, election_id, user_id)
VALUES(true , 6, 4);