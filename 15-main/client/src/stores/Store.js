import {writable} from "svelte/store";

export const tokenStore = writable("");
export const loggedIn = writable(false);
export const userId = writable(-1);
export const userRole = writable("");
export const currentElectionIdStore = writable();
export const currentCandidateIdStore = writable();
export const currentUserIdStore = writable();
export const createdElectableUserIdStore = writable();

tokenStore.subscribe(token => {
    if (token) {
        const payload = JSON.parse(atob(token.token.split('.')[1]));
        loggedIn.set(true)
        userId.set(parseInt(payload.userId));
        userRole.set(payload.role);
    } else {
        loggedIn.set(false);
        userId.set(-1);
        userRole.set("");
    }
})